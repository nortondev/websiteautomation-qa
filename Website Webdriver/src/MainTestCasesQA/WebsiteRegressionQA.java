package MainTestCasesQA;

import org.testng.annotations.Test;

import TestBase.SetupQA;
import pageObjectsPROD.SocialMediaSitesPROD;
import pageObjectsQA.CreateNewAccountQA;
import pageObjectsQA.HomePageQA;
import pageObjectsQA.SocialMediaSitesQA;

public class WebsiteRegressionQA extends SetupQA {
	
	  @Test(description = "Verify Header and Footer links", priority = 1) public
	  void home_page_links() throws Exception {
	  
	  HomePageQA QAwebsiteHomePage = new HomePageQA(driver);
	  
	  QAwebsiteHomePage.QAclickOnReader(); 
	  QAwebsiteHomePage.QAclickOnStudent();
	  QAwebsiteHomePage.QAclickOnEducator(); 
	  QAwebsiteHomePage.QAclickOnWhoWeAre();
	  QAwebsiteHomePage.QAclickOnCareers(); 
	  QAwebsiteHomePage.QAclickOnHelp();
	  QAwebsiteHomePage.QAclickOnAccessibility();
	  QAwebsiteHomePage.QAclickOnPrivacyPolicy();
	  QAwebsiteHomePage.QAclickOnTermsOfUse();
	  QAwebsiteHomePage.QAclickOnContactUs(); 
	  QAwebsiteHomePage.QAclickOnSeagull();
	  
	 }
	  
	  @Test(description = "Verify Signup for News Letter", priority = 2) public
	  void News_Letter_Signup() throws Exception {
	  
	  HomePageQA QAnewsLetterSignup = new HomePageQA(driver);
	  
	  QAnewsLetterSignup.QAsendEmailForNewsLetter();
	  QAnewsLetterSignup.QAclickOnSendForNewsLetter();
	  driver.navigate().refresh();
	  QAnewsLetterSignup.QAsendIncorrectEmailForNewsLetter();
	 
	  }
	  
	  @Test(description = "Click on social media sites", priority = 3) public void
	  Click_On_Social_Media() throws Exception {
	 
	  SocialMediaSitesQA QAsocialmedia = new SocialMediaSitesQA(driver);
	  Thread.sleep(2000); 
	  QAsocialmedia.QAclickOnFacebook();
	  QAsocialmedia.QAclickOnTwitter(); 
	  QAsocialmedia.QAclickOnInstagram();
	  
	  } 
	 


	@Test(description = "Create new user account , verify existing email , Forgot Password , login with new login", priority = 4)
	public void Create_New_Account() throws Exception {

		CreateNewAccountQA createAccountQA = new CreateNewAccountQA(driver);

		createAccountQA.QAclickOnLogin();
		createAccountQA.QAcreateAccountEmail();
		createAccountQA.QAclickOnCreateAccountButton();
		createAccountQA.QAfirstName();
		createAccountQA.QAlastName();
		createAccountQA.QAAccountEmail();
		createAccountQA.QARetypeAccountEmail();
		createAccountQA.QAclickOnShowPassword();
		createAccountQA.QAAccountPassword();
		createAccountQA.QAfinalCreateNewAccount();
		createAccountQA.QAContinueButtonConfirmationMessage();
		createAccountQA.QAcreateAccountWithExistingEmail();
		
		ForgotPasswordQA forgotPasswordQA = new ForgotPasswordQA(driver);
		forgotPasswordQA.QAclickOnForgotYourPassword();
		
		LoginPageQA loginPageQA = new LoginPageQA(driver);
		loginPageQA.QAloginWithNewAccount();

	} 


}
