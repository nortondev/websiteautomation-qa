package pageObjectsQA;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.formula.functions.Replace;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

//import com.relevantcodes.extentreports.LogStatus;

import TestBase.SetupQA;
import pageObjectsQA.CreateNewAccountQA;

public class ForgotPasswordQA extends SetupQA {

	WebDriver driver;

	@FindBy(linkText = "Forgot your password?")
	WebElement forgotpassword;

	@FindBy(id = "accountEmail")
	WebElement accountEmailForgotPassword;

	@FindBy(className = "SubmitButton")
	WebElement forgotPasswordSubmitButton;
	
	@FindBy(id = "profileLogin")
	WebElement profileLogin;
	
	@FindBy(xpath = "//a[text()='Log Out']")
	public static WebElement logoutElement;

	public ForgotPasswordQA(WebDriver driver) {
		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);

	}

	public void QAclickOnForgotYourPassword() throws IOException, InterruptedException {
		driver.navigate().refresh();
		CreateNewAccountQA QAWebsiteLoginLink = new CreateNewAccountQA(driver);
		QAWebsiteLoginLink.QAclickOnLogin();

		forgotpassword.click();

		accountEmailForgotPassword.sendKeys(CreateNewAccountQA.newUserEmailID);
		forgotPasswordSubmitButton.click();
		driver.get("https://www.mailinator.com/v3/#/#inboxpane");
		String forgotPasswordEmailID = CreateNewAccountQA.newUserEmailID;
		String forgotPasswordEmailID1 = forgotPasswordEmailID.replace("@", "_");
		driver.findElement(By.id("inbox_field")).sendKeys(forgotPasswordEmailID1);
		driver.findElement(By.id("go_inbox")).click();
		driver.findElement(By.linkText("Reset Password for W. W. Norton Account")).click();
		driver.switchTo().frame(0);
		driver.findElement(By.linkText("wwnorton.com/resetpassword")).click();
		Thread.sleep(5000);
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.findElement(By.id("accountPassword")).sendKeys("Test@123");
		driver.findElement(By.id("accountRetypePassword")).sendKeys("Test@123");
		driver.findElement(By.id("LoginSubmitButton")).sendKeys(Keys.ENTER);
		driver.findElement(By.id("loginEmail")).sendKeys(CreateNewAccountQA.newUserEmailID);
		driver.findElement(By.id("loginPassword")).sendKeys("Test@123");
		driver.findElement(By.id("LoginSubmitButton")).sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		profileLogin.click();
		Thread.sleep(1000);
		logoutElement.click();
		//test.log(LogStatus.PASS, "Forgot Password - Successfully reset the password");
	}
}