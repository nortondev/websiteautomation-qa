package pageObjectsQA;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

//import com.relevantcodes.extentreports.LogStatus;

import TestBase.SetupQA;

public class LoginPageQA extends SetupQA {

	WebDriver driver;

	@FindBy(id = "loginEmail")
	WebElement LoginEmailLink;

	@FindBy(id = "loginPassword")
	WebElement Password;

	@FindBy(id = "LoginSubmitButton")
	WebElement LoginSubmitButton;

	public LoginPageQA(WebDriver driver) {
		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);

	}

	public void QAloginWithNewAccount() throws IOException {
		driver.navigate().refresh();
		CreateNewAccountQA QAWebsiteLoginLink = new CreateNewAccountQA(driver);
		QAWebsiteLoginLink.QAclickOnLogin();
		LoginEmailLink.sendKeys(CreateNewAccountQA.newUserEmailID);
		Password.sendKeys("Test@123");
		LoginSubmitButton.click();

		if (ForgotPasswordQA.logoutElement.isEnabled()) {
			ForgotPasswordQA.logoutElement.click();
			// test.log(LogStatus.PASS, "Create New User Account - Click on Login link");

		} else {
			System.out.println("Login unsuccessfull");
			// test.log(LogStatus.FAIL, "Login link is not working");

		}

	}
}
