package pageObjectsQA;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class HomePageQA {

	WebDriver driver;
	// JavascriptExecutor js = (JavascriptExecutor) driver;

	@FindBy(linkText = "READER")
	WebElement readerElement;

	@FindBy(linkText = "STUDENT")
	WebElement studentElement;

	@FindBy(linkText = "EDUCATOR")
	WebElement educatorElement;

	@FindBy(linkText = "WHO WE ARE")
	WebElement whoweareElement;

	@FindBy(linkText = "CAREERS")
	WebElement careersElement;

	@FindBy(linkText = "HELP")
	WebElement helpElement;

	@FindBy(linkText = "ACCESSIBILITY")
	WebElement accessibilityElement;

	@FindBy(linkText = "PRIVACY POLICY")
	WebElement privacypolicyElement;

	@FindBy(linkText = "TERMS OF USE")
	WebElement termsofuseElement;

	@FindBy(linkText = "CONTACT US")
	WebElement contactusElement;

	@FindBy(id = "TopCenterImage")
	WebElement seagullicon;

	@FindBy(name = "email")
	WebElement newsletter;

	@FindBy(id = "newsletterSend")
	WebElement sendnewsletter;
	
	@FindBy(className = "errorLabel")
	WebElement errormessageforincorrectEmail;

	public HomePageQA(WebDriver driver) {
		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);

	}

	public void QAclickOnReader() {
		readerElement.click();

		Assert.assertEquals("https://qa.wwnorton.net/reader", driver.getCurrentUrl());

	}

	public void QAclickOnStudent() {
		studentElement.click();
		Assert.assertEquals("https://qa.wwnorton.net/student", driver.getCurrentUrl());
	}

	public void QAclickOnEducator() {
		educatorElement.click();
		Assert.assertEquals("https://qa.wwnorton.net/educator", driver.getCurrentUrl());
	}

	public void QAclickOnWhoWeAre() {
		// js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		whoweareElement.click();
		Assert.assertEquals("https://qa.wwnorton.net/who-we-are", driver.getCurrentUrl());
	}

	public void QAclickOnCareers() {
		careersElement.click();
		Assert.assertEquals("https://qa.wwnorton.net/careers", driver.getCurrentUrl());
	}

	public void QAclickOnHelp() {
		helpElement.click();
		Assert.assertEquals("https://qa.wwnorton.net/help", driver.getCurrentUrl());
	}

	public void QAclickOnAccessibility() {
		accessibilityElement.click();
		Assert.assertEquals("https://qa.wwnorton.net/accessibility", driver.getCurrentUrl());
	}

	public void QAclickOnPrivacyPolicy() {
		privacypolicyElement.click();
		Assert.assertEquals("https://qa.wwnorton.net/privacy-policy", driver.getCurrentUrl());
	}

	public void QAclickOnTermsOfUse() {
		termsofuseElement.click();
		Assert.assertEquals("https://qa.wwnorton.net/terms-of-use", driver.getCurrentUrl());
	}

	public void QAclickOnContactUs() {
		contactusElement.click();
		Assert.assertEquals("https://qa.wwnorton.net/contact-us", driver.getCurrentUrl());
	}

	public void QAclickOnSeagull() {
		seagullicon.click();
		Assert.assertEquals("https://qa.wwnorton.net/", driver.getCurrentUrl());
	}

	public void QAsendEmailForNewsLetter() {
		newsletter.sendKeys("smaheshwari@wwnorton.com");

	}

	public void QAclickOnSendForNewsLetter() {
		sendnewsletter.click();

	}

	public void QAsendIncorrectEmailForNewsLetter() {
		newsletter.sendKeys("abcd");
		errormessageforincorrectEmail.getAttribute("value");
		boolean SendButtonDisabled = sendnewsletter.isEnabled();
		System.out.println("Send button is enabled?: "+SendButtonDisabled);
		
		

	}

}
