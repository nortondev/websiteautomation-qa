package pageObjectsQA;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CreateNewAccountQA {

	WebDriver driver;
	Random randomGenerator = new Random();
	int randomInt = randomGenerator.nextInt(10000);
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
	String date = sdf.format(new Date());

	String newUserEmailID = "QA" + date + randomInt + "@mailinator.com";

	@FindBy(id = "profileLogin")
	WebElement loginElement;

	@FindBy(id = "creatAccountEmail")
	WebElement createAccountEmail;

	@FindBy(id = "CreateAccountSubmitButton")
	WebElement createAccountButton;

	@FindBy(id = "accountFirstName")
	WebElement firstName;

	@FindBy(id = "accountLastName")
	WebElement lastName;

	@FindBy(xpath = "//*[@id='accountEmail']")
	WebElement accountEmail;

	@FindBy(xpath = "//*[@id='accountReTypeEmail']")
	WebElement reTypeAccountEmail;

	@FindBy(linkText = "Show Password")
	WebElement showPasswordLink;

	@FindBy(xpath = "//*[@id='accountPassword']")
	WebElement accountPassword;

	@FindBy(xpath = "//*[@id='LoginSubmitButton']")
	WebElement finalCreateAccountButton;

	@FindBy(id = "profileLogin")
	WebElement profileloginElement;

	@FindBy(xpath = "//a[text()='Log Out']")
	WebElement logoutElement;

	@FindBy(xpath = "//button[text()='Continue']")
	WebElement continueButton;
	
	@FindBy(id = "CreateAccountEmailId")
	WebElement errorMessage1;

	public CreateNewAccountQA(WebDriver driver) {
		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);

	}

	public void QAclickOnLogin() {
		loginElement.click();

	}

	public void QAcreateAccountEmail() {

		createAccountEmail.sendKeys(newUserEmailID);
		System.out.println("PROVIDED EMAIL ID IS: " + newUserEmailID);

	}

	public void QAclickOnCreateAccountButton() {
		createAccountButton.click();

	}

	public void QAfirstName() {
		firstName.sendKeys("WWNQA");

	}

	public void QAlastName() {
		lastName.sendKeys("Testing");

	}

	public void QAAccountEmail() {

		System.out.println("COMPARED WITH: " + accountEmail.getAttribute("value"));

		Assert.assertEquals(newUserEmailID, accountEmail.getAttribute("value"));

	}

	public void QARetypeAccountEmail() {
		reTypeAccountEmail.sendKeys(newUserEmailID);
		

	}

	public void QAclickOnShowPassword() {
		showPasswordLink.click();

	}

	public void QAAccountPassword() {
		accountPassword.sendKeys("Password@123");

	}

	public void QAfinalCreateNewAccount() {
		finalCreateAccountButton.sendKeys(Keys.TAB);
		finalCreateAccountButton.sendKeys(Keys.ENTER);

	}

	public void QAContinueButtonConfirmationMessage() {
		continueButton.click();

	}

	public void QAcreateAccountWithExistingEmail() throws InterruptedException {

		logoutElement.click();
		Thread.sleep(2000);
		loginElement.click();
		createAccountEmail.sendKeys(newUserEmailID);
		createAccountButton.click();
		System.out.println(errorMessage1.getText());
		

	}

}
