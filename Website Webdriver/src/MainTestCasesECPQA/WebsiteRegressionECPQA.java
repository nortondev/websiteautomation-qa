package MainTestCasesECPQA;

import org.testng.annotations.Test;

import TestBase.SetupECPQA;
import TestBase.SetupQA;
import pageObjectsECPQA.CreateNewAccountECPQA;
import pageObjectsECPQA.HomePageECPQA;
import pageObjectsECPQA.SocialMediaSitesECPQA;
import pageObjectsPROD.CreateNewAccountPROD;
import pageObjectsQA.HomePageQA;

public class WebsiteRegressionECPQA extends SetupECPQA {

	@Test(description = "Verify Header and Footer links", priority = 1)

	public void home_page_links() throws Exception {
		HomePageECPQA ECPQAwebsiteHomePage = new HomePageECPQA(driver1);

		ECPQAwebsiteHomePage.ECPQAclickOnReader();

		ECPQAwebsiteHomePage.ECPQAclickOnStudent();

		ECPQAwebsiteHomePage.ECPQAclickOnEducator();

		ECPQAwebsiteHomePage.ECPQAclickOnWhoWeAre();
		Thread.sleep(1000);
		ECPQAwebsiteHomePage.ECPQAclickOnCareers();
		Thread.sleep(1000);
		ECPQAwebsiteHomePage.ECPQAclickOnHelp();
		Thread.sleep(1000);
		ECPQAwebsiteHomePage.ECPQAclickOnAccessibility();
		Thread.sleep(1000);
		ECPQAwebsiteHomePage.ECPQAclickOnPrivacyPolicy();
		Thread.sleep(1000);
		ECPQAwebsiteHomePage.ECPQAclickOnTermsOfUse();
		Thread.sleep(1000);
		ECPQAwebsiteHomePage.ECPQAclickOnContactUs();
		Thread.sleep(1000);
		ECPQAwebsiteHomePage.ECPQAclickOnSeagull();
		Thread.sleep(1000);

	}

	@Test(description = "Verify news letter subscription", priority = 2)
	public void home_page_news_letter() throws Exception {

		HomePageECPQA ECPQAwebsiteHomePage = new HomePageECPQA(driver1);

		ECPQAwebsiteHomePage.ECPQAsendEmailForNewsLetter();
		Thread.sleep(1000);
		ECPQAwebsiteHomePage.ECPQAclickOnSendForNewsLetter();

	}

	@Test(description = "Click on social media sites", priority = 3)
	public void Click_On_Social_Media() throws Exception {

		SocialMediaSitesECPQA ECPQAsocialmedia = new SocialMediaSitesECPQA(driver1);
		Thread.sleep(2000);
		ECPQAsocialmedia.ECPQAclickOnFacebook();
		ECPQAsocialmedia.ECPQAclickOnTwitter();
		ECPQAsocialmedia.ECPQAclickOnInstagram();

	} 

	@Test(description = "Create new user account , verify existing email , Forgot Password , login with new login", priority = 4)
	public void Create_New_Account() throws Exception {

		CreateNewAccountECPQA createAccountECPQA = new CreateNewAccountECPQA(driver1);
		createAccountECPQA.ECPQAclickOnLogin();
		createAccountECPQA.ECPQAcreateAccountEmail();
		createAccountECPQA.ECPQAclickOnCreateAccountButton();
		createAccountECPQA.ECPQAfirstName();
		createAccountECPQA.ECPQAlastName();
		createAccountECPQA.ECPQAAccountEmail();
		createAccountECPQA.ECPQARetypeAccountEmail();
		createAccountECPQA.ECPQAclickOnShowPassword();
		createAccountECPQA.ECPQAAccountPassword();
		createAccountECPQA.ECPQAfinalCreateNewAccount();
		createAccountECPQA.ECPQAContinueButtonConfirmationMessage();
		createAccountECPQA.ECPQAcreateAccountWithExistingEmail();

		
		ForgotPasswordECPQA forgotPasswordECPQA = new ForgotPasswordECPQA(driver1);
		forgotPasswordECPQA.ECPQAclickOnForgotYourPassword();
		
		LoginPageECPQA loginPageECPQA = new LoginPageECPQA(driver1);
		loginPageECPQA.ECPQAloginWithNewAccount();
	}

}
