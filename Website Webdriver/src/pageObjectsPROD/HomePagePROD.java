package pageObjectsPROD;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class HomePagePROD {

	WebDriver driver;
	//JavascriptExecutor js = (JavascriptExecutor) driver;
	
	@FindBy(linkText = "READER")
	WebElement readerElement;

	@FindBy(linkText = "STUDENT")
	WebElement studentElement;

	@FindBy(linkText = "EDUCATOR")
	WebElement educatorElement;
	
	@FindBy(linkText = "WHO WE ARE")
	WebElement whoweareElement; 

	@FindBy(linkText = "CAREERS")
	WebElement careersElement; 
	
	@FindBy(linkText = "HELP")
	WebElement helpElement; 
	
	@FindBy(linkText = "ACCESSIBILITY")
	WebElement accessibilityElement; 
	
	@FindBy(linkText = "PRIVACY POLICY")
	WebElement privacypolicyElement; 
	
	@FindBy(linkText = "TERMS OF USE")
	WebElement termsofuseElement; 
	

	@FindBy(linkText = "CONTACT US")
	WebElement contactusElement; 
	
	@FindBy(id = "TopCenterImage")
	WebElement seagullicon;
	
	@FindBy(name = "email")
	WebElement newsletter;
	
	@FindBy(id = "newsletterSend")
	WebElement sendnewsletter;
	
	public HomePagePROD(WebDriver driver) {
		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);

	} 

	public void PRODclickOnReader() {
		readerElement.click();

		Assert.assertEquals("https://wwnorton.com/reader", driver.getCurrentUrl());

	} 

	public void PRODclickOnStudent() {
		studentElement.click();
		Assert.assertEquals("https://wwnorton.com/student", driver.getCurrentUrl());
	}

	public void PRODclickOnEducator() {
		educatorElement.click();
		Assert.assertEquals("https://wwnorton.com/educator", driver.getCurrentUrl());
	}
	
	public void PRODclickOnWhoWeAre() {
		//js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		whoweareElement.click();
		Assert.assertEquals("https://wwnorton.com/who-we-are", driver.getCurrentUrl());
	} 
	
	public void PRODclickOnCareers() {
		careersElement.click();
		Assert.assertEquals("https://wwnorton.com/careers", driver.getCurrentUrl());
	} 
	public void PRODclickOnHelp() {
		helpElement.click();
		Assert.assertEquals("https://wwnorton.com/help", driver.getCurrentUrl());
	} 
	
	public void PRODclickOnAccessibility() {
		accessibilityElement.click();
		Assert.assertEquals("https://wwnorton.com/accessibility", driver.getCurrentUrl());
	} 
	
	public void PRODclickOnPrivacyPolicy() {
		privacypolicyElement.click();
		Assert.assertEquals("https://wwnorton.com/privacy-policy", driver.getCurrentUrl());
	} 
	
	public void PRODclickOnTermsOfUse() {
		termsofuseElement.click();
		Assert.assertEquals("https://wwnorton.com/terms-of-use", driver.getCurrentUrl());
	} 
	public void PRODclickOnContactUs() {
		contactusElement.click();
		Assert.assertEquals("https://wwnorton.com/contact-us", driver.getCurrentUrl());
	} 
	
	public void PRODclickOnSeagull() {
		seagullicon.click();
		Assert.assertEquals("https://wwnorton.com/", driver.getCurrentUrl());
	} 
	
	public void PRODsendEmailForNewsLetter() {
		newsletter.sendKeys("smaheshwari@wwnorton.com");
		
	} 
	
	public void PRODclickOnSendForNewsLetter() {
		sendnewsletter.click();
		
	} 


}
