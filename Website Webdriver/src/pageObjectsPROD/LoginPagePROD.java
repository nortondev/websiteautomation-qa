package pageObjectsPROD;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

//import com.relevantcodes.extentreports.LogStatus;

import TestBase.SetupPROD;
import TestBase.SetupQA;

public class LoginPagePROD extends SetupPROD{
	
	WebDriver driver;

	@FindBy(id = "loginEmail")
	WebElement LoginEmailLink;
	
	@FindBy(id = "loginPassword")
	WebElement Password;
	
	@FindBy(id = "LoginSubmitButton")
	WebElement LoginSubmitButton;
	
	

	public LoginPagePROD(WebDriver driver) {
		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);

	}
	
	public void QAloginWithNewAccount() throws IOException {
		driver.navigate().refresh();
		CreateNewAccountPROD PRODWebsiteLoginLink = new CreateNewAccountPROD(driver);
		PRODWebsiteLoginLink.PRODclickOnLogin();
		LoginEmailLink.sendKeys(CreateNewAccountPROD.newUserEmailID);
		Password.sendKeys("Test@123");
		LoginSubmitButton.click();
		
		if (ForgotPasswordPROD.logoutElement.isEnabled()) {
			ForgotPasswordPROD.logoutElement.click();
			//test.log(LogStatus.PASS, "Login with new user account is successfull");

		} else {
			System.out.println("Login unsuccessfull");
			//test.log(LogStatus.FAIL, "Login unsuccessfull");

		}
		
		

		}
	}



