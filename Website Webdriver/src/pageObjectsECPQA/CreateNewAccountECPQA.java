package pageObjectsECPQA;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CreateNewAccountECPQA {

	WebDriver driver;
	Random randomGenerator = new Random();
	int randomInt = randomGenerator.nextInt(10000);
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
	String date = sdf.format(new Date());
	
	String newUserEmailID = "QA"+date+randomInt+"@mailinator.com";

	@FindBy(id = "profileLogin")
	WebElement loginElement;

	@FindBy(id = "creatAccountEmail")
	WebElement createAccountEmail;

	@FindBy(id = "CreateAccountSubmitButton")
	WebElement createAccountButton;

	@FindBy(id = "accountFirstName")
	WebElement firstName;

	@FindBy(id = "accountLastName")
	WebElement lastName;

	@FindBy(xpath = "//*[@id='accountEmail']")
	WebElement accountEmail;

	@FindBy(xpath = "//*[@id='accountReTypeEmail']")
	WebElement reTypeAccountEmail;

	@FindBy(linkText = "Show Password")
	WebElement showPasswordLink;

	@FindBy(xpath = "//*[@id='accountPassword']")
	WebElement accountPassword;

	@FindBy(xpath = "//*[@id='LoginSubmitButton']")
	WebElement finalCreateAccountButton;
	
	@FindBy(id = "profileLogin")
	WebElement profileloginElement;

	@FindBy(xpath = "//a[text()='Log Out']")
	WebElement logoutElement;

	@FindBy(xpath = "//button[text()='Continue']")
	WebElement continueButton;
	
	@FindBy(id = "CreateAccountEmailId")
	WebElement errorMessage1;

	public CreateNewAccountECPQA(WebDriver driver) {
		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);

	}

	public void ECPQAclickOnLogin() {
		loginElement.click();

	}

	public void ECPQAcreateAccountEmail() {

		createAccountEmail.sendKeys(newUserEmailID);
		System.out.println("PROVIDED EMAIL ID IS: " + newUserEmailID);

	}

	public void ECPQAclickOnCreateAccountButton() {
		createAccountButton.click();

	}

	public void ECPQAfirstName() {
		firstName.sendKeys("WWNQA");

	}

	public void ECPQAlastName() {
		lastName.sendKeys("Testing");

	}

	public void ECPQAAccountEmail() {

		System.out.println("COMPARED WITH: " + accountEmail.getAttribute("value"));

		Assert.assertEquals(newUserEmailID, accountEmail.getAttribute("value"));

	}

	public void ECPQARetypeAccountEmail() {
		reTypeAccountEmail.sendKeys(newUserEmailID);

	}

	public void ECPQAclickOnShowPassword() {
		showPasswordLink.click();

	}

	public void ECPQAAccountPassword() {
		accountPassword.sendKeys("Password@123");

	}

	public void ECPQAfinalCreateNewAccount() {
		finalCreateAccountButton.sendKeys(Keys.TAB);
		finalCreateAccountButton.sendKeys(Keys.ENTER);

	}
	
	public void ECPQAContinueButtonConfirmationMessage() {
		continueButton.click();

	}

	public void ECPQAcreateAccountWithExistingEmail() throws InterruptedException {

		logoutElement.click();
		Thread.sleep(2000);
		loginElement.click();
		createAccountEmail.sendKeys(newUserEmailID);
		createAccountButton.click();
		System.out.println(errorMessage1.getText());
		

	}

}
