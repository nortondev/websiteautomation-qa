package pageObjectsECPQA;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

//import com.relevantcodes.extentreports.LogStatus;

import TestBase.SetupECPQA;
import TestBase.SetupQA;

public class LoginPageECPQA extends SetupECPQA{
	
	WebDriver driver;

	@FindBy(id = "loginEmail")
	WebElement LoginEmailLink;
	
	@FindBy(id = "loginPassword")
	WebElement Password;
	
	@FindBy(id = "LoginSubmitButton")
	WebElement LoginSubmitButton;
	
	

	public LoginPageECPQA(WebDriver driver) {
		this.driver = driver;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver, this);

	}
	
	public void ECPQAloginWithNewAccount() throws IOException {
		driver.navigate().refresh();
		CreateNewAccountECPQA ECPQAWebsiteLoginLink = new CreateNewAccountECPQA(driver);
		ECPQAWebsiteLoginLink.ECPQAclickOnLogin();
		LoginEmailLink.sendKeys(CreateNewAccountECPQA.newUserEmailID);
		Password.sendKeys("Test@123");
		LoginSubmitButton.click();
		
		if (ForgotPasswordECPQA.logoutElement.isEnabled()) {
			ForgotPasswordECPQA.logoutElement.click();
			//test.log(LogStatus.PASS, "Login with new user account is successfull");

		} else {
			System.out.println("Login unsuccessfull");
			//test.log(LogStatus.FAIL, "Login unsuccessfull");

		}
		
		

		}
	}



