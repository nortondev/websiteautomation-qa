package pageObjectsECPQA;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class HomePageECPQA {

	WebDriver driver1;
	//JavascriptExecutor js = (JavascriptExecutor) driver;
	
	@FindBy(linkText = "READER")
	WebElement readerElement;

	@FindBy(linkText = "STUDENT")
	WebElement studentElement;

	@FindBy(linkText = "EDUCATOR")
	WebElement educatorElement;
	
	@FindBy(linkText = "WHO WE ARE")
	WebElement whoweareElement; 

	@FindBy(linkText = "CAREERS")
	WebElement careersElement; 
	
	@FindBy(linkText = "HELP")
	WebElement helpElement; 
	
	@FindBy(linkText = "ACCESSIBILITY")
	WebElement accessibilityElement; 
	
	@FindBy(linkText = "PRIVACY POLICY")
	WebElement privacypolicyElement; 
	
	@FindBy(linkText = "TERMS OF USE")
	WebElement termsofuseElement; 
	

	@FindBy(linkText = "CONTACT US")
	WebElement contactusElement; 
	
	@FindBy(id = "TopCenterImage")
	WebElement seagullicon;
	
	@FindBy(name = "email")
	WebElement newsletter;
	
	@FindBy(id = "newsletterSend")
	WebElement sendnewsletter;
	
	
	public  HomePageECPQA(WebDriver driver1) 
		
	 {
		this.driver1 = driver1;
		// This initElements method will create all WebElements
		PageFactory.initElements(driver1, this);

	} 

	public void ECPQAclickOnReader() {
		readerElement.click();

		Assert.assertEquals("https://ecp-qa.wwnorton.net/reader", driver1.getCurrentUrl());

	} 

	public void ECPQAclickOnStudent() {
		studentElement.click();
		Assert.assertEquals("https://ecp-qa.wwnorton.net/student", driver1.getCurrentUrl());
	}

	public void ECPQAclickOnEducator() {
		educatorElement.click();
		Assert.assertEquals("https://ecp-qa.wwnorton.net/educator", driver1.getCurrentUrl());
	}
	
	public void ECPQAclickOnWhoWeAre() {
		//js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		whoweareElement.click();
		Assert.assertEquals("https://ecp-qa.wwnorton.net/who-we-are", driver1.getCurrentUrl());
	} 
	
	public void ECPQAclickOnCareers() {
		careersElement.click();
		Assert.assertEquals("https://ecp-qa.wwnorton.net/careers", driver1.getCurrentUrl());
	} 
	public void ECPQAclickOnHelp() {
		helpElement.click();
		Assert.assertEquals("https://ecp-qa.wwnorton.net/help", driver1.getCurrentUrl());
	} 
	
	public void ECPQAclickOnAccessibility() {
		accessibilityElement.click();
		Assert.assertEquals("https://ecp-qa.wwnorton.net/accessibility", driver1.getCurrentUrl());
	} 
	
	public void ECPQAclickOnPrivacyPolicy() {
		privacypolicyElement.click();
		Assert.assertEquals("https://ecp-qa.wwnorton.net/privacy-policy", driver1.getCurrentUrl());
	} 
	
	public void ECPQAclickOnTermsOfUse() {
		termsofuseElement.click();
		Assert.assertEquals("https://ecp-qa.wwnorton.net/terms-of-use", driver1.getCurrentUrl());
	} 
	public void ECPQAclickOnContactUs() {
		contactusElement.click();
		Assert.assertEquals("https://ecp-qa.wwnorton.net/contact-us", driver1.getCurrentUrl());
	} 
	
	public void ECPQAclickOnSeagull() {
		seagullicon.click();
		Assert.assertEquals("https://ecp-qa.wwnorton.net/", driver1.getCurrentUrl());
	} 
	public void ECPQAsendEmailForNewsLetter() {
		newsletter.sendKeys("smaheshwari@wwnorton.com");
		
	} 
	
	public void ECPQAclickOnSendForNewsLetter() {
		sendnewsletter.click();
		
	} 
}
