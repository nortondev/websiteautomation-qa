package pageObjectsECPQA;

import java.util.Set;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class SocialMediaSitesECPQA {

	WebDriver driver1;

	@FindBy(xpath = "//a[@href='https://www.facebook.com/wwnorton/']")
	WebElement facebookElement;

	@FindBy(xpath = "//a[@href='https://twitter.com/wwnorton']")
	WebElement twitterElement;

	@FindBy(xpath = "//a[@href='https://www.instagram.com/w.w.norton/']")
	WebElement instagramElement;

	public SocialMediaSitesECPQA(WebDriver driver1)

	{
		this.driver1 = driver1;

		PageFactory.initElements(driver1, this);

	}

	public void ECPQAclickOnFacebook() {
		facebookElement.click();

		String parentWindow = driver1.getWindowHandle();
		Set<String> handles = driver1.getWindowHandles();
		for (String windowHandle : handles) {
			if (!windowHandle.equals(parentWindow)) {
				driver1.switchTo().window(windowHandle);
				Assert.assertEquals("https://www.facebook.com/wwnorton/", driver1.getCurrentUrl());
				driver1.close();
			}
		}

		driver1.switchTo().window(parentWindow);

	}

	public void ECPQAclickOnTwitter() {
		twitterElement.click();

		String parentWindow = driver1.getWindowHandle();
		Set<String> handles = driver1.getWindowHandles();
		for (String windowHandle : handles) {
			if (!windowHandle.equals(parentWindow)) {
				driver1.switchTo().window(windowHandle);
				Assert.assertEquals("https://twitter.com/wwnorton", driver1.getCurrentUrl());
				driver1.close();

			}
		}

		driver1.switchTo().window(parentWindow);

	}

	public void ECPQAclickOnInstagram() {
		instagramElement.click();

		String parentWindow = driver1.getWindowHandle();
		Set<String> handles = driver1.getWindowHandles();
		for (String windowHandle : handles) {
			if (!windowHandle.equals(parentWindow)) {
				driver1.switchTo().window(windowHandle);
				Assert.assertEquals("https://www.instagram.com/w.w.norton/", driver1.getCurrentUrl());
				driver1.close();
			}

		}
		driver1.switchTo().window(parentWindow);

	}

}