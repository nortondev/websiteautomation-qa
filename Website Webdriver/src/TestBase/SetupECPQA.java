package TestBase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class SetupECPQA {

	public static WebDriver driver1;

	@DataProvider(name = "data-provider")
	public Object[][] urldataProviderMethod() {

		return new Object[][] { { "https://ecp-qa.wwnorton.net/" } };
	}

	@BeforeClass

	@Parameters("browser")

	public void setup(String browser) throws Exception {

		if (browser.equalsIgnoreCase("firefox")) {

			System.setProperty("webdriver.gecko.driver", "C:\\Users\\surabhim\\Documents\\Drivers\\geckodriver.exe");
			driver1 = new FirefoxDriver();
		}

		else if (browser.equalsIgnoreCase("chrome")) {
			// set path to chromedriver.exe
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\surabhim\\Documents\\Drivers\\chromedriver.exe");
			driver1 = new ChromeDriver();

		}

		/*
		 * else if(browser.equalsIgnoreCase("Edge")){ //set path to Edge.exe
		 * System.setProperty("webdriver.edge.driver",
		 * "C:\\Users\\surabhim\\Documents\\Drivers\\msedgedriver.exe"); driver1 = new
		 * EdgeDriver(); }
		 */

		else {
			// If no browser passed throw exception
			throw new Exception("Browser is not correct");
		}

		driver1.manage().window().maximize();
		driver1.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	}

	@Test(dataProvider = "data-provider")

	public void ECPQAurl(String url1) {
		// To open norton site

		System.out.println("CURRENT URL IS:" + url1);
		driver1.get(url1);
	}

	@AfterClass
	public void quitDriver() {
		driver1.quit();

	}

}
