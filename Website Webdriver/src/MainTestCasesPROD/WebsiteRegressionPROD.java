package MainTestCasesPROD;

import org.testng.annotations.Test;

import TestBase.SetupPROD;
import TestBase.SetupQA;
import pageObjectsECPQA.SocialMediaSitesECPQA;
import pageObjectsPROD.CreateNewAccountPROD;
import pageObjectsPROD.HomePagePROD;
import pageObjectsPROD.SocialMediaSitesPROD;
import pageObjectsQA.CreateNewAccountQA;

public class WebsiteRegressionPROD extends SetupPROD {

	@Test(description = "Verify Header and Footer links", priority = 1)
	public void home_page_links() throws Exception {

		HomePagePROD PRODwebsiteHomePage = new HomePagePROD(driver);

		PRODwebsiteHomePage.PRODclickOnReader();
		Thread.sleep(1000);
		PRODwebsiteHomePage.PRODclickOnStudent();
		Thread.sleep(1000);
		PRODwebsiteHomePage.PRODclickOnEducator();
		Thread.sleep(1000);
		PRODwebsiteHomePage.PRODclickOnWhoWeAre();
		Thread.sleep(1000);
		PRODwebsiteHomePage.PRODclickOnCareers();
		Thread.sleep(1000);
		PRODwebsiteHomePage.PRODclickOnHelp();
		Thread.sleep(1000);
		PRODwebsiteHomePage.PRODclickOnAccessibility();
		Thread.sleep(1000);
		PRODwebsiteHomePage.PRODclickOnPrivacyPolicy();
		Thread.sleep(1000);
		PRODwebsiteHomePage.PRODclickOnTermsOfUse();
		Thread.sleep(1000);
		PRODwebsiteHomePage.PRODclickOnContactUs();
		Thread.sleep(1000);
		PRODwebsiteHomePage.PRODclickOnSeagull();
		Thread.sleep(1000);
		PRODwebsiteHomePage.PRODsendEmailForNewsLetter();
		Thread.sleep(1000);
		PRODwebsiteHomePage.PRODclickOnSendForNewsLetter();
		Thread.sleep(1000);

	} 

	@Test(description = "Verify news letter subscription", priority = 2)
	public void home_page_news_letter() throws Exception {

		HomePagePROD PRODwebsiteHomePage = new HomePagePROD(driver);

		PRODwebsiteHomePage.PRODsendEmailForNewsLetter();
		Thread.sleep(1000);

		PRODwebsiteHomePage.PRODclickOnSendForNewsLetter();

	}

	@Test(description = "Click on social media sites", priority = 3)
	public void Click_On_Social_Media() throws Exception {

		SocialMediaSitesPROD PRODsocialmedia = new SocialMediaSitesPROD(driver);
		Thread.sleep(2000);
		PRODsocialmedia.PRODclickOnFacebook();
		PRODsocialmedia.PRODclickOnTwitter();
		PRODsocialmedia.PRODclickOnInstagram();

	} 

	@Test(description = "Create new user account , verify existing email , Forgot Password , login with new login", priority = 4)
	public void Create_New_Account() throws Exception {

		CreateNewAccountPROD createAccountPROD = new CreateNewAccountPROD(driver);
		createAccountPROD.PRODclickOnLogin();
		createAccountPROD.PRODcreateAccountEmail();
		createAccountPROD.PRODclickOnCreateAccountButton();
		createAccountPROD.PRODfirstName();
		createAccountPROD.PRODlastName();
		createAccountPROD.PRODAccountEmail();
		createAccountPROD.PRODRetypeAccountEmail();
		createAccountPROD.PRODclickOnShowPassword();
		createAccountPROD.PRODAccountPassword();
		createAccountPROD.PRODfinalCreateNewAccount();
		createAccountPROD.PRODContinueButtonConfirmationMessage();
		createAccountPROD.PRODcreateAccountWithExistingEmail();
		
		ForgotPasswordPROD forgotPasswordPROD = new ForgotPasswordPROD(driver);
		forgotPasswordPROD.PRODclickOnForgotYourPassword();
		
		LoginPagePROD loginPagePROD = new LoginPagePROD(driver);
		loginPagePROD.QAloginWithNewAccount();
		

	} 

}
