package com.wwnorton.WebsiteAutomation.utilities;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.WebDriverWait;




public class PropertiesFile extends BaseDriver {

	
	public static WebDriverWait wait;
	public static String UserDir = System.getProperty("user.dir");
	public static String url;
	public static String DriverPath;

		
	// Read Browser name, Test url and Driver file path from config.properties.

	public static void readPropertiesFile() throws Exception {

		Properties prop = new Properties();

		try {

			InputStream input = new FileInputStream(UserDir +
					"/src/test/resources/config.properties");
			prop.load(input);

			//Browser = prop.getProperty("browsername");
			url = prop.getProperty("testurl");
			DriverPath = UserDir +"/Drivers/";
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	// Set Browser configurations by comparing Browser name and Diver file path.
	// @Parameters({"browser"})
	public static void setBrowserConfig(String browser)
			throws IllegalAccessException {
		init_driver(browser);

	}

	// Set Test URL based on config.properties file.

	public static void setURL() {
		
		getDriver().manage().deleteAllCookies();
		getDriver().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		getDriver().manage().window().maximize();
		getDriver().get(url);
	}


	// Close the driver after running test script.
	public static void tearDownTest() throws InterruptedException {
	
		if(getDriver() != null){
			getDriver().manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
			getDriver().close();
			getDriver().quit();
		}
	}

}