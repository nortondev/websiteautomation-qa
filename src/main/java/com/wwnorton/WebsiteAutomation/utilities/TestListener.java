package com.wwnorton.WebsiteAutomation.utilities;

import java.util.List;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import ru.yandex.qatools.allure.annotations.Attachment;

public class TestListener extends BaseDriver implements ITestListener {

	private static String getTestMethodName(ITestResult iTestResult) {
		return iTestResult.getMethod().getConstructorOrMethod().getName();
	}

	// Capture Image Attachment for Allure
	@Attachment(value = "Screenshot of {0}", type = "image/png")
	public byte[] captureScreenshot(WebDriver driver) {

		return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);

	}

	// Save Image Attachment for Allure
	@Attachment(value = "Page Screenshot", type = "image/png")
	public byte[] saveScreenshotPNG(WebDriver driver) {
		return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
	}

	// Text attachments for Allure
	@Attachment(value = "{0}", type = "text/plain")
	public static String saveTextLog(String message) {
		return message;
	}

	// HTML attachments for Allure
	@Attachment(value = "{0}", type = "text/html")
	public static String attachHtml(String html) {
		return html;
	}

	// All test methods invoked after the test class is instantiated and before
	// any configuration method is called.
	@Override
	public void onStart(ITestContext iTestContext) {

		System.out.println("I am in onStart method " + iTestContext.getName());
		iTestContext.setAttribute("WebDriver", BaseDriver.getDriver());

	}

	@Override
	public void onFinish(ITestContext iTestContext) {

		System.out.println("I am in onFinish method " + iTestContext.getName());

	}

	@Override
	public void onTestStart(ITestResult iTestResult) {

		System.out.println("I am in onTestStart method " + getTestMethodName(iTestResult) + " start");

	}

	@Override
	public void onTestSuccess(ITestResult iTestResult) {
		// logOutput(Reporter.getOutput(iTestResult));
		System.out.println("I am in onTestSuccess method " + getTestMethodName(iTestResult) + " succeed");

	}

	@Override
	public void onTestSkipped(ITestResult iTestResult) {

		System.out.println("I am in onTestSkipped method " + getTestMethodName(iTestResult) + " skipped");

	}

	@Override
	public void onTestFailure(ITestResult iTestResult) {
		System.out.println("I am onTestFailure method " + getTestMethodName(iTestResult) + " failed");

		// Get driver from PropertiesFile and assign to local webdriver
		// variable.
		//Object testClass = iTestResult.getInstance();
		//WebDriver driver = ((PropertiesFile) testClass).getDriver();
		@SuppressWarnings("static-access")
		WebDriver driver = BaseDriver.getDriver();
		
		// Allure ScreenShotRobot and SaveTestLog
		if (driver instanceof WebDriver) {
			System.out.println("Screenshot captured for Test script:" + getTestMethodName(iTestResult));
			saveScreenshotPNG(driver);
		}

		// Save a testlog on Allure.
		saveTextLog(getTestMethodName(iTestResult) + " Failed and screenshot taken!");

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

		System.out.println("Test failed but it is in defined success ratio " + getTestMethodName(iTestResult));
	}

	@Attachment
	public String logOutput(List<String> outputList) {
		String output = "";
		for (String o : outputList)
			output += o + "<br/>";
		return output;
	}
}
