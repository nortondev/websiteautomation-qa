package com.wwnorton.WebsiteAutomation.utilities;

import java.util.Properties;
import org.openqa.selenium.WebDriver;


public class BaseDriver {
	
	public WebDriver driver = null;
	Properties prop;
	//public static ThreadLocal<WebDriver> tlDriver = new ThreadLocal<WebDriver>(); 
	public static final ThreadLocal<WebDriver> tlDriver = new ThreadLocal<WebDriver>();
	// thread local driver object for webdriver
	
	public static void init_driver(String browser) throws IllegalAccessException{
		WebDriver driver = new WebDriverFactory().init(browser);
		setWebdriver(driver);
		getDriver();
		
	}
	  //set Driver
		public static void setWebdriver(WebDriver driver) {
		// TODO Auto-generated method stub
			tlDriver.set(driver);
		
	}
	 //* getDriver using ThreadLocal
	 
	public synchronized static WebDriver getDriver() {
		return tlDriver.get();
	}
	public Properties init_prop() {
		prop = new Properties();
		return prop;
	}
	
}
