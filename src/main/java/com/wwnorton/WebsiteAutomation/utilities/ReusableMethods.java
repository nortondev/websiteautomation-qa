package com.wwnorton.WebsiteAutomation.utilities;




import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class ReusableMethods {
	
	public static WebDriver driver;

	public static void checkPageIsReady(WebDriver driver) {

		boolean isPageReady = false;
		JavascriptExecutor js = (JavascriptExecutor) driver;

		while (!isPageReady) {

			isPageReady = js.executeScript("return document.readyState")
					.toString().equals("complete");

			try {

				Thread.sleep(1000);

			} catch (InterruptedException e) {

				e.printStackTrace();

			}
		}
	}

	public static boolean elementExist(WebDriver driver, String xpath) {

		try {

			driver.findElement(By.xpath(xpath));

		} catch (NoSuchElementException e) {
			return false;
		}

		return true;
	}

	public static void scrollToBottom(WebDriver driver) {
		try {
			((JavascriptExecutor) driver)
					.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	
	public static void scrollToTop(WebDriver driver) {
		try {
			((JavascriptExecutor) driver)
		    .executeScript("window.scrollTo(0, -document.body.scrollHeight)");
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollToElement(WebDriver driver, By by) {
		try {
			WebElement element = driver.findElement(by);
			Actions actions = new Actions(driver);
			actions.moveToElement(element);
			actions.perform();
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}
	
	
	public static void moveToElement(WebDriver driver, WebElement webElement) {
		try {
				Actions actions = new Actions(driver);
				actions.moveToElement(webElement);
				actions.perform();
				Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}

	public static void scrollIntoView(WebDriver driver, WebElement webElement) {
		try {
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].scrollIntoView(true);", webElement);
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("Exception Handlled: " + e.getMessage());
		}
	}


	public static void waitForPageLoad(WebDriver driver) {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 150);
		wait.until(pageLoadCondition);
	}


	public static void waitVisibilityOfElement(WebDriver driver, By by) {

		WebDriverWait Wait = new WebDriverWait(driver, 50);
		Wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	public static void waitElementClickable(WebDriver driver, By by) {

		WebDriverWait Wait = new WebDriverWait(driver, 50);
		Wait.until(ExpectedConditions.elementToBeClickable(by));
	}

	public static void loadingWaitDisapper(WebDriver driver, WebElement loader) {
		WebDriverWait wait = new WebDriverWait(driver, 5000L);
		wait.until(ExpectedConditions.visibilityOf(loader)); // wait for loader
																// to appear
		wait.until(ExpectedConditions.invisibilityOf(loader)); // wait for
																// loader to
																// disappear
	}

	
	public static void clickWebElement(WebDriver driver, WebElement element){
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", element);
	}
	
	public static String generateEmailID()	{
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(10000);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		String date = sdf.format(new Date());

		String newUserEmailID = "Test" + date + randomInt + "@maildrop.cc";
		
		return newUserEmailID;
	}
	
	
	public static String getNewWindowTab(String  parentWin) {
		driver = BaseDriver.getDriver();
		String currentURL = null;
		Set<String> handles = driver.getWindowHandles();
		for (String windowHandle : handles) {
			if (!windowHandle.equals(parentWin)) {
				driver.switchTo().window(windowHandle);
				currentURL = driver.getCurrentUrl();
				driver.close();
			}
		}

		driver.switchTo().window(parentWin);
		return currentURL;

	}
	
	public static String getNewWindow(String  parentWin) {
		driver = BaseDriver.getDriver();
		Set<String> handles = driver.getWindowHandles();
		for (String windowHandle : handles) {
			if (!windowHandle.equals(parentWin)) {
				driver.switchTo().window(windowHandle);
				
			}
		}
		
		return parentWin;

	}
	
	public static String getNewWindowNewTab(String  parentWin) {
		driver = BaseDriver.getDriver();
		String currentURL = null;
		parentWin = driver.getWindowHandle();
		Set<String> handles = driver.getWindowHandles();
		for (String windowHandle : handles) {
			if (!windowHandle.equals(parentWin)) {
				driver.switchTo().window(windowHandle);
				currentURL = driver.getCurrentUrl();
				driver.close();
			}
		}

		driver.switchTo().window(parentWin);
		return currentURL;

	}
	
	public static String getCurrentDay() {

		Date currentDate = new Date();
		String day = new SimpleDateFormat("d").format(currentDate);
		return day;
	}

	public static void switchToFrame(WebDriver driver, String string) {
		driver.switchTo().frame(string);	
	}


}