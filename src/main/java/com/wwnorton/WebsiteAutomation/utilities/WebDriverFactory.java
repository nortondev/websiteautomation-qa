package com.wwnorton.WebsiteAutomation.utilities;

import io.github.bonigarcia.wdm.WebDriverManager;

import java.util.HashMap;
import java.util.logging.Level;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;


public class WebDriverFactory {
	public static String UserDir = System.getProperty("user.dir");
	//static String DriverPath = UserDir + "\\Drivers\\";
	
	WebDriver init(String type) throws IllegalAccessException {
		WebDriver driver;
		switch (type) {
		case "Firefox":
			//System.setProperty("webdriver.gecko.driver", DriverPath + "/geckodriver.exe");

			FirefoxProfile profile = new FirefoxProfile();
			profile.setPreference("network.proxy.no_proxies_on", "localhost");
			profile.setPreference("javascript.enabled", true);
			FirefoxOptions options = new FirefoxOptions();

			options.setProfile(profile);
			options.addPreference("browser.link.open_newwindow", 2);
			options.addPreference("browser.link.open_newwindow.restriction", 0);
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver(options);
		    driver.manage().window().maximize();
			break;
		case "Chrome":
			System.setProperty("webdriver.chrome.silentOutput","true");
			//System.setProperty("webdriver.chrome.driver", DriverPath + "/chromedriver.exe");
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put(
					"profile.default_content_setting_values.notifications", 1);
			chromePrefs.put("download.prompt_for_download", false);
			chromePrefs.put("plugins.plugins_disabled", "Chrome PDF Viewer");
			chromePrefs.put("download.default_directory",
					System.getProperty("user.dir") + "\\Downloads");
			chromePrefs.put("savefile.default_directory",
					System.getProperty("user.dir") + "\\Downloads");
		   // DesiredCapabilities desiredCapabilities = DesiredCapabilities.
			ChromeOptions chromeOptions = new ChromeOptions();
			//chromeOptions.setHeadless(true);
			chromeOptions.setPageLoadStrategy(PageLoadStrategy.EAGER);
			chromeOptions.addArguments("--test-type");
			chromeOptions.addArguments("--enable-javascript");
			chromeOptions.addArguments("disable-features=NetworkService");
			chromeOptions.addArguments("--start-maximized");
			//chromeOptions.addArguments("--disable-infobars");
			chromeOptions.addArguments("--disable-extensions");
			chromeOptions.addArguments("--disable-notifications");
			//chromeOptions.addArguments("--disable-gpu");
			chromeOptions.addArguments("--no-default-browser-check");
			chromeOptions.addArguments("--ignore-certificate-errors");
			chromeOptions.addArguments("--proxy-server='direct://'");
			chromeOptions.addArguments("--proxy-bypass-list=*");
			chromeOptions.addArguments("--disable-features=VizDisplayCompositor");
			chromeOptions.addArguments("--kiosk-printing");
			chromeOptions.addArguments("--print-to-pdf");
			chromeOptions.setExperimentalOption("prefs", chromePrefs);
		   // chromeOptions.addArguments("--headless");
		 //   chromeOptions.addArguments("--window-size=1920,1080");
			LoggingPreferences logPrefs = new LoggingPreferences();
			logPrefs.enable( LogType.PERFORMANCE, Level.ALL );
			chromeOptions.setCapability( "goog:loggingPrefs", logPrefs );
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver(chromeOptions);
			break;
		case "Edge":
			//System.setProperty("webdriver.edge.driver", DriverPath + "/msedgedriver.exe");
			HashMap<String, Object> edgePrefs = new HashMap<String, Object>();
			edgePrefs.put("profile.default_content_settings.popups", 0);
			EdgeOptions edgeoptions = new EdgeOptions();
			edgeoptions.setCapability("prefs", edgePrefs);
			edgeoptions.setCapability("useAutomationExtension", false);
			WebDriverManager.edgedriver().setup();
			driver = new EdgeDriver(edgeoptions);
			driver.manage().window().maximize();
			break;
		default:
			throw new IllegalAccessException();
		}
		
		return driver;

	}

}
