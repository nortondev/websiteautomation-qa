package com.wwnorton.WebsiteAutomation.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

//import Utilities.ReadUIJsonFile;

public class ReadJsonFile {

	public static String UserDir = System.getProperty("user.dir");

	// Read Json test data from testData.json file with the parameters and values.
	Properties prop = new Properties();
	

	public JsonObject readJason() {

		JsonObject rootObject = null;
		InputStream input = null;
		try {
			input = new FileInputStream(UserDir +"/src/test/resources/config.properties");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			prop.load(input);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		String url = prop.getProperty("testurl");

		try {
			
			JsonParser parser = new JsonParser();
			if(url.equalsIgnoreCase("https://auth-qa.wwnorton.net/")){
			JsonReader jReader = new JsonReader(new FileReader(UserDir + "/src/test/resources/testData_QA.json"));
			jReader.setLenient(true);
			JsonElement rootElement = parser.parse(jReader);
			rootObject = rootElement.getAsJsonObject();
			}else if(url.equalsIgnoreCase("https://ecp-stg.wwnorton.net/")) {
			JsonReader jReader = new JsonReader(new FileReader(UserDir + "/src/test/resources/testData_ECPQA.json"));
			jReader.setLenient(true);
			JsonElement rootElement = parser.parse(jReader);
			rootObject = rootElement.getAsJsonObject();
			}else if (url.equalsIgnoreCase("https://wwnorton.com")) {
				JsonReader jReader = new JsonReader(new FileReader(UserDir
						+ "/src/test/resources/testData_Prod.json"));

				jReader.setLenient(true);
				JsonElement rootElement = parser.parse(jReader);
				rootObject = rootElement.getAsJsonObject();
			}

		}

		catch (Exception e)

		{
			e.printStackTrace();
		}

		return rootObject;

	}
	
	//SS
	public static Map<String, String> readDataJson(String jsonObjectName) throws IOException {
		ReadJsonFile urlJosnFilereader = new ReadJsonFile();
		urlJosnFilereader.readJason();
		String jsonFileName = readConfigData("JsonFileName");
		String key = null,value = null;
		HashMap<String, String> hash_map = new HashMap<String,String>();
		String contents = new String(Files.readAllBytes(Paths.get(System.getProperty("user.dir")
				+ "//resources//"+jsonFileName+".json")));
		JSONObject jsonFile = new JSONObject(contents);
		@SuppressWarnings("unused")
		JSONArray deepC = jsonFile.names();
		/*for(int k=0; k<deepC.length(); k++){
			key = deepC.getString(k);
			
		}*/
		/*String roleName = readConfigData("UserRole3");
		if(roleName.equals("Student")){	
			key=jsonObjectName;
		}*/
	//	JSONObject parent1 = (JSONObject) jsonFile.get("eBookInstructorLoginCredentials");
		JSONObject variableList = jsonFile.getJSONObject(jsonObjectName); // <-- use getJSONObject
	    JSONArray keys = variableList.names ();
	    for (int i = 0; i < keys.length (); i++) {
	        key = keys.getString(i);
	        value = variableList.getString(key);
	        //System.out.println("key: " + key + " value: " + value);
	        
		    hash_map.put(key, value);
		   
	    }
	    hash_map.get("accountEmail");
	    hash_map.get("accountpw");
	   return hash_map;
	}

	private static String readConfigData(String string) {
		// TODO Auto-generated method stub
		return null;
	}

}
