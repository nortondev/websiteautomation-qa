package com.wwnorton.WebsiteAutomation.objectFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class CartItemDetails {
	
	WebDriver driver;
	
	
	// Finding Web Elements on Book Details page with respect to Add to Cart using PageFactory.
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Remove')]")
	WebElement removeButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='empty-text']")
	WebElement emptyCart;
	
	@FindBy(how = How.XPATH, using = "//div[@class='dropdown btn-group btn-group-default']")
	WebElement quantityDropDown;
	
	@FindBy(how = How.XPATH, using = "//body//li[6]")
	WebElement selectQuantityValue;
	
	@FindBy(how = How.XPATH, using = "//body//li[2]")
	WebElement selectShippingQuantity;
	
	@FindBy(how = How.XPATH, using = "//h4[@class='modal-title']")
	WebElement modleTitle;
	
	@FindBy(how = How.XPATH, using = "//button[@id='QTYMessageCloseButton']")
	WebElement qtyCloseButton;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"CommonModelPop\"]/div/div/div[2]/div[1]/div[1]/span")
	WebElement commercialText;        
	
	@FindBy(how = How.XPATH, using = "//div[@class='qtyMessageDetail']//div[2]")
	WebElement contactPhoneNumber; //div[@class='modal-body']//div[3]   
	
	@FindBy(how = How.XPATH, using = "//div[@class='modal-body']//div[3]")
	WebElement contactEmail; //div[@role='dialog']//div[4]
	
	@FindBy(how = How.XPATH, using = "//div[@class='displayOrderItems']")
	public WebElement yourCartItemsEle;
	
	@FindBy(how = How.XPATH, using = "//span[@class='itemCount topMargin']")
	public WebElement totalCartAmtEle;
	
	@FindBy(how = How.XPATH, using = "//div[@class='cartBookPrice']")
	public WebElement cartBookPrice;
	
	@FindBy(how = How.XPATH, using = "//span[@class='itemCount']")
	public List<WebElement> cartItems;
	
	@FindBy(how = How.XPATH, using = "//div[@class='checkOutLogin']/span[2]/span")
	public WebElement logOut_CheckOut;
	
	
	// Initializing Web Driver and PageFactory.
	
	public CartItemDetails() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}
	
	
	// Validate Cart Item Details
	
	@Step("Remove the Promo item from the Cart,  Method: {method} ")
	public void clickRemove_PromoBook(String bookName) throws Exception {
		
		WebElement promoRemoveEle;
		try {
				promoRemoveEle = driver.findElement(By.xpath(
						"//div[contains(text(),'" + bookName + "')]/following-sibling::div/button[@type='button']"));
				promoRemoveEle.click();
		} catch (NoSuchElementException e) {
			
			throw new Exception ("Promo Remove button does not exist");	
		}
		
	}
	
	
	@Step("Get Total Cart Amount, Method: {method} ")
	public String getTotalCartAmount() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(totalCartAmtEle));
		return totalCartAmtEle.getText();
		
	}
	
	@Step("Get Book Price in the Cart, Method: {method} ")
	public String getBookPrice_Cart() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(cartBookPrice));
		return cartBookPrice.getText();
		
	}
	
	@Step("Get Estimated Tax in the Cart, Method: {method} ")
	public String getEstimatedTax_Cart() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfAllElements(cartItems));
		return cartItems.get(2).getText();
		
	}
	
	
	@Step("Capture Empty Cart text, Method: {method} ")
	public String getEmptyCartText() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(emptyCart));
		
		return emptyCart.getText();
		
	}
	
	
	@Step("Select Quantity of Books,  Method: {method} ")
	public void selectQuantity() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(quantityDropDown));
		quantityDropDown.click();
		
		wait.until(ExpectedConditions.visibilityOf(selectQuantityValue));
		selectQuantityValue.click();
		
	}
	
	@Step("Select Shipping Quantity of Books,  Method: {method} ")
	public void selectShippingQuantity() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(quantityDropDown));
		quantityDropDown.click();
		
		wait.until(ExpectedConditions.visibilityOf(selectShippingQuantity));
		selectShippingQuantity.click();
		
	}
	
	
	@Step("Verify the language on PopUp,  Method: {method} ")
	public String[] qtyPopUpMessage() throws Exception {
		
		String[] strArray = new String[4];
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(modleTitle));
		strArray[0] = modleTitle.getText();
		
		wait.until(ExpectedConditions.visibilityOf(commercialText));
		strArray[1] = commercialText.getText();
		
		wait.until(ExpectedConditions.visibilityOf(contactPhoneNumber));
		strArray[2] = contactPhoneNumber.getText();
		
		wait.until(ExpectedConditions.visibilityOf(contactEmail));
		strArray[3] = contactEmail.getText();
		
		
		return strArray;
		
	}
	
	@Step("Checkout PopUp Close,  Method: {method} ")
	public boolean checkoutPopUpDisplay() throws Exception {
		
		boolean closeButtonExist;
		
		try 
		{
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(quantityDropDown));
			closeButtonExist = qtyCloseButton.isDisplayed();
			
		}
		
		catch (NoSuchElementException e) {
			
			throw new Exception ("Close button does not exist");
			
		}
			
		if(closeButtonExist)
		{
			qtyCloseButton.click();
		}
		
		return closeButtonExist;
		
	}
	
	@Step("Click LogOut in Checkout page, Method: {method} ")
	public void clickLogOut_CheckOut() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(logOut_CheckOut));
		logOut_CheckOut.click();

	}
	
	
	
	
}

