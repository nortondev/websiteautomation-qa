package com.wwnorton.WebsiteAutomation.objectFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class NRLToolPage {
	
	WebDriver driver;

	
	@FindBy(how = How.XPATH, using = "//input[@id='txtEmail']")
	public WebElement Email;
	
	@FindBy(how = How.XPATH, using = "//input[@id='txtIsbn']")
	public WebElement ISBN;
	
	
	@FindBy(how = How.XPATH, using = "//input[@label='Discipline']")
	public WebElement Discipline;
	
	@FindBy(how = How.XPATH, using = "//input[@id='schoolType0']//following-sibling::label/span[@data-style='radio']")
	public WebElement schoolInformation;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'rs__placeholder') and text()='Please select a School State...']")
	public WebElement schoolState;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'rs__placeholder') and text()='Please select a School Name...']")
	public WebElement schoolName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='department']")
	public WebElement Department;
	
	@FindBy(how = How.XPATH, using = "//input[@id='courseName']")
	public WebElement courseName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='termEnrollment']")
	public WebElement EnrollmentTerm;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'rs__placeholder') and text()='Please select a Term...']")
	public WebElement TermDropDown;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'rs__placeholder') and text()='Please select a Year...']")
	public WebElement YearDropDown;
	
	@FindBy(how = How.XPATH, using = "//textarea[@id='comments']")
	public WebElement Comments;
	
	@FindBy(how = How.XPATH, using = "//input[@id='AssignedBook0']//following-sibling::label/span[@data-style='radio']")
	public WebElement assignedBook;
	
	@FindBy(how = How.XPATH, using = "//div[@class='element-container TermCheckbox row']/div/div/input")
	public WebElement TnC_Checkbox;
	
	@FindBy(how = How.XPATH, using = "//button[@class='SubmitButton']")
	public WebElement btnSubmit;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'rs__indicators')]")
	public List<WebElement> dropDownList;
	
	@FindBy(how = How.XPATH, using = "//div[@class='alert alert-success alert-dismissible']")
	public WebElement successMsg;
	
	
	
	public NRLToolPage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}
	

	@Step("Submit the NRL Request form ,   Method: {method}")
	public void SubmitNRLRequestForm(String email, String isbn, String StateName, String SchoolName, String dept) throws InterruptedException {
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Email.sendKeys(email);
		ISBN.sendKeys(isbn);
		schoolInformation.click();
		Thread.sleep(2000);
		//schoolState.click();
		dropDownList.get(0).click();
		//((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropDownList.get(0));
		WebElement stateOption;
		String stateName;
		for(int i=0; i<51; i++)
		{
			stateOption = driver.findElement(By.id("react-select-2-option-" + i));
			stateName = stateOption.getText();
			if(stateName.equals(StateName))
			{
				stateOption.click();
				break;
			}
		}		
		
		//schoolName.click();
		Thread.sleep(2000);
		dropDownList.get(1).click();
		//((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropDownList.get(1));
		WebElement schoolOption;
		String schoolName;
		for(int i=0; i<7; i++)
		{
			schoolOption = driver.findElement(By.id("react-select-3-option-" + i));
			schoolName = schoolOption.getText();
			if(schoolName.equals(SchoolName))
			{
				schoolOption.click();
				break;
			}
		}
		
		Department.sendKeys(dept);
		courseName.sendKeys("Fall Course");
		EnrollmentTerm.sendKeys("2");
		//TermDropDown.click();
		Thread.sleep(2000);
		dropDownList.get(2).click();
		//((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropDownList.get(2));
		WebElement termOption;
		String termName;
		for(int i=0; i<4; i++)
		{
			termOption = driver.findElement(By.id("react-select-4-option-" + i));
			termName = termOption.getText();
			if(termName.equals("Fall"))
			{
				termOption.click();
				break;
			}
		}
		
		//YearDropDown.click();
		Thread.sleep(2000);
		dropDownList.get(3).click();
		//((JavascriptExecutor) driver).executeScript("arguments[0].click();", dropDownList.get(3));
		WebElement yearOption;
		String year;
		for(int i=0; i<5; i++)
		{
			yearOption = driver.findElement(By.id("react-select-5-option-" + i));
			year = yearOption.getText();
			if(year.equals("2025"))
			{
				yearOption.click();
				break;
			}
		}
		
		Comments.sendKeys("Test, please disregard");
		assignedBook.click();
		TnC_Checkbox.click();
		btnSubmit.click();
	}
	
	
	@Step("Check Success message for NRL Admin request submission,  Method: {method} ")
	public Boolean checkSuccessMsg() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(successMsg));

		try {
				successMsg.isDisplayed();
			
			} catch (Exception e) {
				return false; }

		return true;
	}
}
