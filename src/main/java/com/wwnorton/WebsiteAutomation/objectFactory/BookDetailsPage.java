package com.wwnorton.WebsiteAutomation.objectFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class BookDetailsPage {

	WebDriver driver;
	String parentWindow;

	// Finding Web Elements on Book Details page using PageFactory.

	@FindBy(how = How.XPATH, using = "//a[@class='MenuTitle' and text()='Overview']")
	WebElement bookDetailsOverviewMenu;

	@FindBy(how = How.XPATH, using = "//h1[@class='title']")
	WebElement bookTitle;

	@FindBy(how = How.XPATH, using = "//a[@class='ContributorfirstNameURL']")
	WebElement AuthorName;

	@FindBy(how = How.XPATH, using = "//div[@class='AuthorTitle']/h1")
	WebElement authorTitle;

	@FindBy(how = How.XPATH, using = "//div[@class='description']")
	public WebElement readMoreLink;

	@FindBy(how = How.XPATH, using = "//div[@class='description']")
	WebElement bookDescription;
	
	@FindBy(how = How.XPATH, using = "//div[@data-module='gallery']/div/div")
	public WebElement recommendedBooksSection;

	@FindBy(how = How.XPATH, using = "//div[@class='AuthorImage']/a/img")
	List<WebElement> recommendedBooks;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'About the Book')]")
	WebElement AboutTheBook;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'About the Author')]")
	WebElement AboutTheAuthor;

	@FindBy(how = How.XPATH, using = "//div[@data-module='header']/div")
	WebElement authorName_AboutTheAuthor;

	@FindBy(how = How.XPATH, using = "//div[@data-module='list']/div[@class='row']/div/div/div/button")
	public WebElement button_AboutTheAuthor;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Product Details')]")
	WebElement ProductDetails;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Connect')]")
	WebElement connectLink;

	@FindBy(how = How.XPATH, using = "//li[text()='ISBN']/following-sibling:: li")
	WebElement ISBN;

	@FindBy(how = How.XPATH, using = "//*[@id=\"avaliableFormatPanelGrp-heading-2\"]/div/a/div[1]/div[1]")
	WebElement bookBinder;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'bookInfo')]/div[@class='headerTitle']")
	public WebElement bookBinderTitle;

	@FindBy(how = How.XPATH, using = "//div[@data-module='retailers']")
	WebElement retailersSection;

	@FindBy(how = How.XPATH, using = "//div[@class='headerTitle' and text()='Paperback']")
	public WebElement paperbackBookBinding;
	
	@FindBy(how = How.XPATH, using = "//div[@data-module='button']/button[text()='Add to cart']")
	public WebElement addtoCart;

	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[1]/div[1]/div[3]/div[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/main[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]/button[1]")
	WebElement addtoCart1; // *[@id="avaliableFormatPanelGrp-body-1"]/div/div[2]/div/button

	@FindBy(how = How.XPATH, using = "//*[@id=\\\"avaliableFormatPanelGrp-body-2\\\"]/div/div[2]/div/button")
	WebElement addtoCart2;

	@FindBy(how = How.XPATH, using = "//*[@id=\\\"avaliableFormatPanelGrp-body-3\\\"]/div/div/div[1]/div/button")
	WebElement addtoCart3;
	
	@FindBy(how = How.XPATH, using = "//div[@data-module='button']/button[contains(@class,'right')]")
	List<WebElement> addtoCartbuttons;

	@FindBy(how = How.XPATH, using = "//*[@id=\"avaliableFormatPanelGrp-heading-1\"]/div/a/div[1]/div[2]")
	WebElement NYPText; // div[@class='header-subcontent']

	@FindBy(how = How.XPATH, using = "//*[@id=\"avaliableFormatPanelGrp-heading-1\"]/div/a/div[1]/div[1]")
	WebElement eBookText; // *[@id=\"avaliableFormatPanelGrp-heading-1\"]/div/a/div[1]/div[1]

	@FindBy(how = How.XPATH, using = "//*[@id=\"avaliableFormatPanelGrp-body-1\"]/div/div[2]/div/button")
	WebElement buttonText; // *[@id=\"avaliableFormatPanelGrp-body-1\"]/div/div/div[1]/div/button

	@FindBy(how = How.XPATH, using = "//div[@class='bookAvailableFormat']")
	public WebElement bookFormatLayout;

	@FindBy(how = How.XPATH, using = "//ul/li[@id='tab0']")
	public WebElement defaultSelectedTabDescription;

	@FindBy(how = How.XPATH, using = "//li[@id='tab0']/a/span/a[text()='Description']")
	public WebElement tabDescription;

	@FindBy(how = How.XPATH, using = "//li[@id='tab1']/a/span/a[text()='Reviews & Endorsements']")
	public WebElement tabReview;

	@FindBy(how = How.XPATH, using = "//li[@id='tab2']/a/span/a[text()='Product Details']")
	public WebElement tabProductDetails;

	@FindBy(how = How.XPATH, using = "//div[@data-module='rich-text']/div/span")
	public WebElement description_AboutTheeBook;

	@FindBy(how = How.XPATH, using = "//div[@data-module='list']")
	public List<WebElement> reviewQuoteList;

	@FindBy(how = How.XPATH, using = "//span[@data-id='connect' and text()='CONNECT']")
	public WebElement connectSection;

	@FindBy(how = How.XPATH, using = "//span[@class='iconLabel' and text()='Facebook']")
	public WebElement connect_FaceBook;

	@FindBy(how = How.XPATH, using = "//span[@class='iconLabel' and text()='Twitter']")
	public WebElement connect_Twitter;

	@FindBy(how = How.XPATH, using = "//span[@class='iconLabel' and text()='Instagram']")
	public WebElement connect_Instagram;

	// --> Added By Surabhi

	@FindBy(how = How.XPATH, using = "//input[@name='email']")
	public WebElement newsletter;

	@FindBy(id = "newsletterSend")
	public WebElement sendnewsletter;

	@FindBy(className = "successLabel")
	public WebElement successLabel;

	@FindBy(how = How.XPATH, using = "//button[@type='button' and text()='Amazon']")
	public WebElement connect_Amazon;

	@FindBy(how = How.XPATH, using = "//button[@type='button' and text()='Apple Books']")
	public WebElement connect_Apple_Books;

	@FindBy(how = How.XPATH, using = "//button[@type='button' and text()='Barnes & Noble']")
	public WebElement connect_Barnes_Noble;

	@FindBy(how = How.XPATH, using = "//div[@class='book-badge__caption' and text()='W. W. Norton & Company']")
	public WebElement publisherName_AppleBook;

	@FindBy(how = How.XPATH, using = "//span[@itemprop='publisher' and contains(text(),'W. W. & Company')]")
	public WebElement publisherName_Barnes;

	@FindBy(how = How.XPATH, using = "//*[text()=': W. W. Norton & Company']")
	public WebElement publisherName_BooksAMillion;

	@FindBy(how = How.XPATH, using = "//button[@class='ltkpopup-close']")
	public WebElement closeButton_BooksAMillion;

	@FindBy(how = How.XPATH, using = "//*[text()='W. W. Norton & Company']")
	public WebElement publisherName_BookShop;

	@FindBy(how = How.XPATH, using = "//*[text()='W. W. Norton & Company']")
	public WebElement publisherName_Hudson;

	@FindBy(how = How.XPATH, using = "//div[@id='book-data']/p[6]")
	public WebElement publisherName_Indiebound;

	@FindBy(how = How.XPATH, using = "//button[@type='button' and text()='Books-A-Million']")
	public WebElement connect_Books_A_Million;

	@FindBy(how = How.XPATH, using = "//button[@type='button' and text()='Bookshop']")
	public WebElement connect_Book_Shop;

	@FindBy(how = How.XPATH, using = "//button[@type='button' and text()='Hudson']")
	public WebElement connect_Hudson;

	@FindBy(how = How.XPATH, using = "//button[@type='button' and text()='IndieBound']")
	public WebElement connect_IndieBound;

	@FindBy(how = How.XPATH, using = "//button[@type='button' and text()='Target']")
	public WebElement connect_Target;

	@FindBy(how = How.XPATH, using = "//button[@type='button' and text()='Walmart']")
	public WebElement connect_Walmart;

	@FindBy(how = How.XPATH, using = "//span[text()='W. W. Norton & Company']")
	public WebElement publisherName_Amazon;

	// Initializing Web Driver and PageFactory.

	public BookDetailsPage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}

	// Capture and return ISBN Number as input from Json.

	@Step("Return and Check ISBN Number,  Method: {method} ")
	public String getISBNNumber() throws InterruptedException {
		String ISBNText = "null";

		return ISBNText;

	}

	// --> Added By Surabhi

	@Step("Get a new window for book retailer,  Method: {method}")

	public String openBookRetailerWindow(String parentWindow) throws InterruptedException {

		return ReusableMethods.getNewWindow(parentWindow);

	}

	// --> Added By Surabhi

	@Step("Get WWNorton Publisher Name in Amazon Retailler ,  Method: {method} ")

	public String getPublisherName_AmazonRetailer() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(publisherName_Amazon));
		return publisherName_Amazon.getText();

	}

	// --> Added By Surabhi

	@Step("Get WWNorton Publisher Name in Apple Books Retailler ,  Method: {method} ")

	public String getPublisherName_AppleBookRetailer() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(publisherName_AppleBook));
		return publisherName_AppleBook.getText();

	}

	// --> Added By Surabhi

	@Step("Get WWNorton Publisher Name in Barnes Books Retailler ,  Method: {method} ")

	public String getPublisherName_BarnesBookRetailer() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(publisherName_Barnes));

		return publisherName_Barnes.getText();

	}

	// --> Added By Surabhi bdp.clickOnWalmart();

	@Step("Click on Books-A-Million Retailers,  Method: {method} ")

	public String clickOnBooksAMillion() throws InterruptedException {

		connect_Books_A_Million.click();

		return ReusableMethods.getNewWindowNewTab(parentWindow);

	}

	// --> Added By Surabhi

	@Step("Click on Book shop Retailers,  Method: {method} ")

	public String clickOnBookShop() throws InterruptedException {

		connect_Book_Shop.click();

		return ReusableMethods.getNewWindowNewTab(parentWindow);

	}

	// --> Added By Surabhi

	@Step("Click on Hudson Retailers,  Method: {method} ")

	public String clickOnHudson() throws InterruptedException {

		connect_Hudson.click();

		return ReusableMethods.getNewWindowNewTab(parentWindow);

	}

	// --> Added By Surabhi

	@Step("Click on Indiebound Retailers,  Method: {method} ")

	public String clickOnndieBound() throws InterruptedException {

		connect_IndieBound.click();

		return ReusableMethods.getNewWindowNewTab(parentWindow);

	}

	// --> Added By Surabhi

	@Step("Click on Target Retailers,  Method: {method} ")

	public String clickOnTarget() throws InterruptedException {

		connect_Target.click();

		return ReusableMethods.getNewWindowNewTab(parentWindow);

	}

	// --> Added By Surabhi

	@Step("Click on Walmart Retailers,  Method: {method} ")

	public String clickOnWalmart() throws InterruptedException {

		connect_Walmart.click();

		return ReusableMethods.getNewWindowNewTab(parentWindow);

	}

	@Step("Check Book Details Overview,  Method: {method} ")
	public boolean checkBookDetailsOverview() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(bookDetailsOverviewMenu));
		return bookDetailsOverviewMenu.isDisplayed();

	}

	// Capture Book Heading in Book Details page.

	@Step("Get Book Title from Book Details,  Method: {method} ")
	public String getBookHeading() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);

		wait.until(ExpectedConditions.visibilityOf(bookTitle));
		String Title = bookTitle.getText();
		return Title;

	}

	// Capture Book Details in Book Details page.

	@Step("Check Book Details from Book Details - Overview,  Method: {method} ")
	public Boolean checkBookDetails() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(bookTitle));

		try {
			bookTitle.isDisplayed();
			AuthorName.isDisplayed();
			bookDescription.isDisplayed();
		} catch (Exception e) {
			return false;
		}

		return true;

	}

	// Click on About the Book tab in Book Details page.

	@Step("Navigate to About The Book section,  Method: {method} ")
	public void clickAboutTheBook() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(AboutTheBook));
		AboutTheBook.click();

	}

	@Step("Check About The Book Tabs selections,  Method: {method} ")
	public Boolean checkAboutTheBook_Tabs() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(defaultSelectedTabDescription));

		try {
			if (defaultSelectedTabDescription.isDisplayed() == true) {
				tabDescription.isDisplayed();
				tabReview.isDisplayed();
				tabProductDetails.isDisplayed();
				description_AboutTheeBook.isDisplayed();
			}
		} catch (Exception e) {
			return false;
		}

		return true;

	}

	// Click on About the Author tab in Book Details page.

	@Step("Navigage to About The Author section and get Author Name,  Method: {method} ")
	public String clickAboutTheAuthor() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(AboutTheAuthor));
		AboutTheAuthor.click();
		return authorName_AboutTheAuthor.getText();

	}

	@Step("Get Author Name on Author's page,  Method: {method} ")
	public String getAuthorName_BookDetaillsOverview() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(AuthorName));
		String authorName = AuthorName.getText();
		return authorName;

	}

	@Step("Get Author Name on Author's page,  Method: {method} ")
	public String getAuthorName() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(AuthorName));
		String authorName = AuthorName.getText();
		AuthorName.click();
		return authorName;

	}

	@Step("Get Author Title in Author Page,  Method: {method} ")
	public String getAuthorTitle() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(authorTitle));
		return authorTitle.getText();

	}

	@Step("Click On Recommended Book in Authors Page,  Method: {method} ")
	public void clickOnRecommendedBook() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		try {
			wait.until(ExpectedConditions.visibilityOf(recommendedBooksSection));
			if (recommendedBooksSection.isDisplayed() == true) {
				recommendedBooks.get(0).click();
			}
		} catch (Exception e) {
			e.getMessage();

		}
	}

	// Click on Product Details tab in About the Book section.

	@Step("Navigate to Product Details section,  Method: {method} ")
	public void clickProductDetails() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(ProductDetails));
		ProductDetails.click();

	}

	// Click on Connect tab in About the Book section.

	@Step("Navigate to Product Details section,  Method: {method} ")
	public void clickConnectLink() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(connectLink));
		connectLink.click();

	}

	@Step("Check Social Media Links in Connect section,  Method: {method} ")
	public Boolean checkSocialMediaLinks() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(connectSection));

		try {
			connect_FaceBook.isDisplayed();
			connect_Twitter.isDisplayed();
			connect_Instagram.isDisplayed();
		} catch (Exception e) {
			return false;
		}

		return true;

	}

	// Capture and return ISBN Number associated to Book from Product Details
	// section.

	@Step("Get ISBN Number from Product Details section,  Method: {method} ")
	public String getISBN() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(ISBN));
		return ISBN.getText();

	}

	// Verify Book Information on Book Details page.

	@Step("Verify Book Characterisitcs, Add to Cart and Available Retailers,  Method: {method} ")
	public boolean verifyBookInformation(String bookBinding) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(bookBinderTitle));

		String headerTitle = bookBinderTitle.getText();

		try {
			if (headerTitle.equalsIgnoreCase(bookBinding)) {
				addtoCart.isDisplayed();
				retailersSection.isDisplayed();
			}

		}

		catch (NoSuchElementException e) {
			return false;
		}

		catch (Exception e) {

			e.printStackTrace();
		}

		return true;

	}

	// Verify Pre Order button is displayed for NYP books.

	@Step("Validate Pre Order button for NYP,  Method: {method} ")
	public String NYPBookExist() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(NYPText));
		wait.until(ExpectedConditions.visibilityOf(eBookText));

		boolean NYPBookExist = true;
		String NYPBookTitle = null;
		String eBookTitle = null;
		String buttonTitle = null;

		try {
			NYPBookExist = NYPText.isDisplayed();
			System.out.println(NYPBookExist);

			NYPBookTitle = NYPText.getText();
			System.out.println(NYPBookTitle);

			eBookTitle = eBookText.getText();
			System.out.println(eBookTitle);

		}

		catch (NoSuchElementException e) {
			NYPBookExist = false;
		}

		catch (Exception e) {

			e.printStackTrace();
		}

		if ((NYPBookExist && NYPBookTitle.equalsIgnoreCase("Not Yet Published"))
				&& eBookTitle != ("Ebook & Learning Tools")) {

			buttonTitle = buttonText.getText();
			System.out.println(buttonTitle);

		}

		else {
			throw new NoSuchElementException("Error: Pre Order button is NOT displayed. ");

		}

		return buttonTitle;

	}

	// Verify Pre Order button is NOT displayed for NYP + eBooks.

	@Step("Validate Pre Order button for NYP + eBooks,  Method: {method} ")
	public boolean NYPeBookExist(String locator) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 3);
		TimeUnit.SECONDS.sleep(3);
		wait.until(ExpectedConditions.visibilityOf(NYPText));
		wait.until(ExpectedConditions.visibilityOf(eBookText));

		boolean NYPeBookExist = true;
		String NYPeBookTitle = null;
		String eBookTitle = null;
		boolean buttonExist = false;

		try {
			NYPeBookExist = NYPText.isDisplayed();
			System.out.println(NYPeBookExist);

			NYPeBookTitle = NYPText.getText();
			System.out.println(NYPeBookTitle);

			eBookTitle = eBookText.getText();
			System.out.println(eBookTitle);

		}

		catch (NoSuchElementException e) {
			NYPeBookExist = false;
		}

		catch (Exception e) {

			e.printStackTrace();
		}

		if ((NYPeBookExist && NYPeBookTitle.equalsIgnoreCase("Not Yet Published"))
				&& eBookTitle.equalsIgnoreCase("Ebook & Learning Tools")) {
			try {
				buttonExist = buttonText.isDisplayed();

				if (buttonExist) {
					buttonExist = true;
				}

			} catch (NullPointerException | NoSuchElementException e) {
				System.err.println("Unable to locate Any Button '" + locator + "'");

			} catch (Exception e) {
				System.err.println("Unable to check display status of Button '" + locator + "'");
				e.printStackTrace();
			}

		}

		return buttonExist;

	}

	// Verify View All Options button is displayed for E-Item Book.

	@Step("Validate View All Options button for NYP + eBooks,  Method: {method} ")
	public String eItemBookExist() throws Exception {

		// WebDriverWait wait = new WebDriverWait(driver,3);
		// TimeUnit.SECONDS.sleep(3);
		// wait.until(ExpectedConditions.visibilityOf(NYPText));
		// wait.until(ExpectedConditions.visibilityOf(eBookText));

		Thread.sleep(3000);

		boolean eItemBookExist = true;
		String eItemBookTitle = null;
		String eBookTitle = null;
		String buttonTitle = null;

		try {
			eItemBookExist = NYPText.isDisplayed();
			System.out.println(eItemBookExist);

			eItemBookTitle = NYPText.getText();
			System.out.println(eItemBookTitle);

			eBookTitle = eBookText.getText();
			System.out.println(eBookTitle);

		}

		catch (NoSuchElementException e) {
			eItemBookExist = false;
		}

		catch (Exception e) {

			e.printStackTrace();
		}

		if ((eItemBookExist && eItemBookTitle.contains("E-Item"))
				&& eBookTitle.equalsIgnoreCase("Ebook & Learning Tools")) {

			buttonTitle = buttonText.getText();
			System.out.println(buttonTitle);

		}

		else {
			throw new NoSuchElementException("Error: View All Options button is NOT displayed. ");

		}

		return buttonTitle;

	}

	// Click on Book binder options to make Add to Cart visible in Book Details
	// page.

	@Step("Click on Book Binding options to see Add to Cart button,  Method: {method} ")
	public void clickBookBinding(String headerTitleOption) throws Exception

	{

		WebElement headerEle;

		try {
			headerEle = driver.findElement(
					By.xpath("//div[@class='headerTitle' and text()='" + headerTitleOption + "']"));

			if (headerEle.getText().equals(headerTitleOption)) {
				headerEle.click();
			}

		} catch (NoSuchElementException e) {

			e.getMessage();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Step("Click on Book Binding options to see Add to Cart button,  Method: {method} ")
	public void addToCart() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		boolean cartButtonExist = true;
		String buttonLabel = null;
		String label= null;
		List<WebElement> cartButtons = null;
		//List<WebElement> buttons = null;

		try {
			cartButtons = driver.findElements(
					By.xpath("//div[@data-module='button']/button"));
			Thread.sleep(2000);
			cartButtonExist = cartButtons.get(0).isDisplayed();
			buttonLabel = cartButtons.get(0).getText();

		} catch (NoSuchElementException e) {

			cartButtonExist = false;
		} catch (Exception e) {

			e.printStackTrace();
		}

		if (cartButtonExist && (buttonLabel != null)
				&& (buttonLabel.equalsIgnoreCase("Add To Cart") || buttonLabel.equalsIgnoreCase("Preorder"))) {
			wait.until(ExpectedConditions.visibilityOf(cartButtons.get(0)));
			//cartButtons.get(0).click();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", cartButtons.get(0));
		}

		else {
				ReusableMethods.scrollIntoView(driver, bookFormatLayout);
				WebElement bindingCheckOutOption;
				List<WebElement> bindingOptions;
				bindingOptions = driver.findElements(By.className("headerTitle"));
				String[] elementArray = new String[bindingOptions.size()];

				for (int i = 1; i <= bindingOptions.size(); i++) {
				elementArray[i - 1] = bindingOptions.get(i).getText();
				bindingCheckOutOption = driver
						.findElement(By.xpath("//div[contains(text()," + "'" + elementArray[0] + "')]"));
				bindingCheckOutOption.click();
				wait.until(ExpectedConditions.visibilityOf(addtoCartbuttons.get(i)));
				label = addtoCartbuttons.get(i).getText();
				if (addtoCartbuttons.isEmpty() != true && (label != null)
						&& (label.equalsIgnoreCase("Add To Cart") || label.equalsIgnoreCase("Preorder"))) {
					addtoCartbuttons.get(i).click();
					break;
				}

			}
		}
	}
	
	@Step("Click on Book Binding options to see Preorder button,  Method: {method} ")
	public void addBookToCart() throws Exception {

		boolean cartButtonExist = false;
		String buttonLabel = null;
		List<WebElement> cartButtons = null;

		try {
				cartButtons = driver.findElements(
					By.xpath("//div[@data-module='button']/button"));
				cartButtonExist = cartButtons.get(0).isDisplayed();
				buttonLabel = cartButtons.get(0).getText();

			} catch (NoSuchElementException e) {

				cartButtonExist = false;
			} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (cartButtonExist && (buttonLabel != null) 
				&& (buttonLabel.equalsIgnoreCase("Add To Cart") 
				|| buttonLabel.equalsIgnoreCase("Preorder"))) {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", cartButtons.get(0));
			//cartButtons.get(0).click();
		}
		
	}

}
