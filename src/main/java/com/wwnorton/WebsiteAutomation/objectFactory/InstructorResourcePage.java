package com.wwnorton.WebsiteAutomation.objectFactory;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class InstructorResourcePage {

	WebDriver driver;

	@FindBy(how = How.XPATH, using = "//button[text()='View Instructor Resources']")
	public WebElement viewInstructorResourcesButton;

	@FindBy(how = How.XPATH, using = "//button[@class='center' and text()='Request Access']")
	public WebElement requestAccessButton;

	@FindBy(how = How.XPATH, using = "//button[@class='secondaryButton' and text()='Request Exam Copy']")
	public WebElement requestExamCopyButton;

	@FindBy(how = How.XPATH, using = "//div[@class='headerDefault ']/h1[text()='Instructor Resources']")
	public WebElement headerText_InstructorResources;

	@FindBy(how = How.XPATH, using = "//div[@class='ResultInstructor']/a")
	public List<WebElement> InstructorResourcesButtonsList;

	@FindBy(how = How.XPATH, using = "//h4[@class='modal-title' and text()='Verify Instructor Status']")
	public WebElement verifyInstructorResourceOverlay;

	@FindBy(how = How.XPATH, using = "//button[@class='SubmitButton CloseButton' and contains(text(),'Instructor')]")
	public WebElement InstrctorButton_InstructorResourceOverlay;

	@FindBy(how = How.XPATH, using = "//button[@class='SubmitButton' and contains(text(),'Close')]")
	public WebElement closeButton_SubmittedRequestOverlay;
	
	@FindBy(how = How.XPATH, using = "//h4[@class='modal-title' and text()='School Information']")
	public WebElement verifySchoolInformationOverlay;

	@FindBy(how = How.XPATH, using = "//h4[@class='modal-title' and text()='Request Submitted']")
	public WebElement verifySubmittedRequestOverlay;

	@FindBy(xpath = "//div[@id='refCollegeCityName']/div/div[1]")
	WebElement SchoolStateDropdown_College;

	@FindBy(xpath = "//div[@id='refTxtDepartment']")
	WebElement DepartmentDropdown;
	
	@FindBy(xpath = "(//div[@id='refTxtDepartment']/div/div)[3]")
	WebElement DepartmentDropdown_Highschool;

	@FindBy(xpath = "(//div[@id='refSchoolCityName']/div/div)[1]")
	WebElement SchoolStateDropdown_Highschool;
	
	@FindBy(xpath = "(//div[@id='refSchoolRegion']/div/div)[1]")
	WebElement RegionStateDropdown_International;
	
	@FindBy(xpath = "(//div[@id='refRegionCollegeCityName']/div/div)[1]")
	WebElement ProvinceDropdown_International;
	
	@FindBy(xpath = "(//div[@id='txtRegionCollegeName']/div/div)[1]")
	WebElement SchoolStateDropdown_International;
	
	@FindBy(xpath = "(//div[@id='txtDeptCollegeName']/div/div)[1]")
	WebElement DeptDropdown_International;

	@FindBy(xpath = "//input[contains(@id,'react-select-3')]")
	WebElement SchoolNameDropdown_College;

	@FindBy(xpath = "(//div[@id='txtCollegeName']/div/div)[3]")
	List<WebElement> SchoolNameDropdown_Highschool;

	@FindBy(how = How.XPATH, using = "//button[text()='Continue']")
	public WebElement continueButton;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'ErrorMessage')]")
	public WebElement selectEditionError;
	
	@FindBy(how = How.XPATH, using = "//button[@class='selectbookedition']")
	public WebElement selectEdition;
	
	@FindBy(how = How.XPATH, using = "//span[@class='TabContentTitle' and text()='Desk Copy']")
	public WebElement deskCopySection;

	@FindBy(how = How.XPATH, using = "//button[text()='Submit']")
	public WebElement submitButton;

	@FindBy(how = How.XPATH, using = "//input[@id='txtExamCourseName']")
	public WebElement courseName;

	@FindBy(how = How.XPATH, using = "//input[@id='txtExamCurrentBook']")
	public WebElement currentBookName;

	@FindBy(how = How.XPATH, using = "//input[@id='txtExamPerTerm']")
	public WebElement perTermEnrollment;

	@FindBy(how = How.XPATH, using = "//button[@id='dropdown-basic-default']/span")
	public WebElement selectTermDropDown;

	@FindBy(how = How.XPATH, using = "//label[text()='Term']/following-sibling::div/div/button[@id='dropdown-basic-default']/span")
	public WebElement termDropDown;

	@FindBy(how = How.XPATH, using = "//label[text()='Year']/following-sibling::div/div/button[@id='dropdown-basic-default']/span")
	public WebElement yearDropDown;

	@FindBy(how = How.XPATH, using = "//textarea[@id='examcomment']")
	public WebElement commentsTextBox;

	@FindBy(how = How.XPATH, using = "//input[@name='bookAssigned' and @value='No']")
	public WebElement radioOption_No;

	@FindBy(how = How.XPATH, using = "//span[text()='I accept the ']/preceding::input[@type='checkbox']")
	public WebElement TCCheckbox;

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[2]")
	public WebElement NotificationCheckbox;

	@FindBy(how = How.XPATH, using = "//span[@class='TabContentTitle' and text()='High School']")
	public WebElement HighschoolSection;

	@FindBy(how = How.XPATH, using = "(//input[@id='txtDepartment'])[2]")
	public WebElement departmentName;

	@FindBy(how = How.XPATH, using = "//button[text()='Request Pending' and @class='center']")
	public WebElement requestPendingButton;

	@FindBy(how = How.XPATH, using = "//*[@id='noanim-tab-example-pane-1']/div/div[4]/div[2]/div/div/div/div/input")
	public WebElement decisionDateCalendar;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Add to cart']")
	public WebElement addToCartButton_Examcopy;
	
	@FindBy(how = How.XPATH, using = "//button[@class='SubmitButton CloseButton' and contains(text(),'Student')]")
	public WebElement StudentButton_InstructorResourceOverlay;

	@FindBy(how = How.XPATH, using = "//button[@id='AddEmailSubmitButton' and contains(text(),'Close')]")
	public WebElement closeButton_InstructorStatusOverlay;

	@FindBy(how = How.XPATH, using = "//h4[@class='modal-title' and text()='Verify Instructor Status']")
	public WebElement verifyInstructorStatusOverlay;
	
	@FindBy(how = How.XPATH, using = "//p[@class='notice']/strong")
	public WebElement verifyNotice_InstructorStatusOverlay;
	
	@FindBy(how = How.XPATH, using = "//span[@class='TabContentTitle' and text()='High School']")
	public WebElement HighSchoolTab_SchoolInfoOverlay;
	
	@FindBy(how = How.XPATH, using = "//span[@class='TabContentTitle' and text()='International']")
	public WebElement InternationalTab_SchoolInfoOverlay;
	
	@FindBy(xpath = "//button[@class='ViewButton']")
	List<WebElement> Assets;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Demo']")
	public WebElement IIGDemoButton;
	
	@FindBy(how = How.XPATH, using = "//button[text()='View Full Site']")
	public WebElement IIGViewFullSiteButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='loginText']/a")
	public WebElement loginText;
	
	@FindBy(how = How.XPATH, using = "//div[@class='loginText']")
	public WebElement SignedInText;
	
	@FindBy(how = How.XPATH, using = "//button/i[@class='glyphicon glyphicon-cogwheels']")
	public WebElement gearMenuIcon_IIGSite;
	
	@FindBy(how = How.XPATH, using = "//a[@id='lnkLogout']")
	public WebElement signOutLink;
	
	@FindBy(how = How.XPATH, using = "//span[@id='profileLogin']")
	public WebElement profileLogIn;
	
	//SS
	@FindBy(how = How.XPATH, using = "//div[@class=\"mainInstructorResource\"]//ul[@class=\"instructorTypesUL\"]/li[@class=\"instructorTypesLI  \"]")
	public WebElement allAssets;
	
	@FindBy(how = How.XPATH, using = "//li[@data-keyvalue='Test Bank'][1]")
	public WebElement testBank;

	// SS
	@FindBy(how = How.XPATH, using = "//li[contains(@data-keyvalue,\"Interactive Instructor\")]")
	public WebElement interactiveInstGuide;

	//SS
	@FindBy(how = How.XPATH, using = "//li[@data-keyvalue=\"Ebook\"]")
	public WebElement eBook;
	
	//SS
		@FindBy(how = How.XPATH, using = "//li[@data-keyvalue=\"High-Impact Practices: A Teaching Guide for Psychology (HIP Guide)\"]")
		public WebElement highImpactPractices;
		
		@FindBy(how = How.XPATH, using = "//li[@data-keyvalue=\"Image Files\"]")
		public WebElement imageFiles;
		
		@FindBy(how = How.XPATH, using = "//li[@data-keyvalue=\"InQuizitive\"]")
		public WebElement inquisitives;
		
		@FindBy(how = How.XPATH, using = "//li[@data-keyvalue=\"PowerPoints: Active Learning\"]")
		public WebElement pptActiveLearning;
		
		@FindBy(how = How.XPATH, using = "//li[@data-keyvalue=\"PowerPoints: Interactive Lecture\"]")
		public WebElement pptInteractiveLecture;
		
		@FindBy(how = How.XPATH, using = "//li[@data-keyvalue=\"PowerPoints: Lecture Outlines\"]")
		public WebElement pptLectureOutlines;
		
		@FindBy(how = How.XPATH, using = "//li[@data-keyvalue=\"Test Bank\"][2]")
		public WebElement testBank2;
		
		@FindBy(how = How.XPATH, using = "//li[@data-keyvalue=\"3D Brain\"]")
		public WebElement threeDbrain;
		
		@FindBy(how = How.XPATH, using = "//section[@name=\"interactive-instructors-guide\"]//div[contains(@class,\"ItemTitle\")]")
		public WebElement iigs_title;
		
		@FindBy(how = How.XPATH, using = "//section[@name=\"test-bank\"][1]//div[contains(@class,\"ItemTitle\")]")
		public WebElement testbank1_title;
		
		@FindBy(how = How.XPATH, using = "//section[@name=\"ebook\"]//div[contains(@class,\"ItemTitle\")]")
		public WebElement eBook_title;
		
		@FindBy(how = How.XPATH, using = "//section[@name=\"high-impact-practices-a-teaching-guide-for-psychology-hip-guide\"]//div[contains(@class,\"ItemTitle\")]")
		public WebElement HIP_title;
		
		@FindBy(how = How.XPATH, using = "//section[@name=\"image-files\"]//div[contains(@class,\"ItemTitle\")]")
		public WebElement imageFiles_title;
		
		@FindBy(how = How.XPATH, using = "//section[@name=\"inquizitive\"]//div[contains(@class,\"ItemTitle\")]")
		public WebElement inquisitive_title;
		
		@FindBy(how = How.XPATH, using = "//section[@name=\"powerpoints-active-learning\"]//div[contains(@class,\"ItemTitle\")]")
		public WebElement pptAL_title;
	
		@FindBy(how = How.XPATH, using = "//section[@name=\"powerpoints-interactive-lecture\"]//div[contains(@class,\"ItemTitle\")]")
		public WebElement pptIL_title;
		
		@FindBy(how = How.XPATH, using = "//section[@name=\"powerpoints-lecture-outlines\"]//div[contains(@class,\"ItemTitle\")]")
		public WebElement pptLO_title;
		
		@FindBy(how = How.XPATH, using = "//section[@name='test-bank'][1]//div[contains(@class,'ItemTitle')]")
		public WebElement testbank2_title;
		
		@FindBy(how = How.XPATH, using = "//section[@name=\"3d-brain\"]//div[contains(@class,\"ItemTitle\")]")
		public WebElement threeDbrain_title;
		
		@Step("Click Link Interactive,   Method: {method}")
		public void clickInteractiveGuide() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(interactiveInstGuide));
			interactiveInstGuide.click();
			}
		
		public void clickTestBank1() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(interactiveInstGuide));
			testBank.click();
			}
		
		public void clickhighImpactPractices() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(interactiveInstGuide));
			highImpactPractices.click();
			}
		
		public void clickimageFiles() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(interactiveInstGuide));
			imageFiles.click();
			}
		
		public void clickinquisitives() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(interactiveInstGuide));
			inquisitives.click();
			}
		
		public void clickpptActiveLearning() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(interactiveInstGuide));
			pptActiveLearning.click();
			}
		
		public void clickpptInteractiveLecture() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(interactiveInstGuide));
			pptInteractiveLecture.click();
			}
		public void clickpptLectureOutlines() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(interactiveInstGuide));
			pptLectureOutlines.click();
			}
		public void clicktestBank2() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(interactiveInstGuide));
			testBank2.click();
			}
		public void clickthreeDbrain() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(interactiveInstGuide));
			threeDbrain.click();
			}
		
		public void clickAllAssets() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.elementToBeClickable(interactiveInstGuide));
			allAssets.click();
			}
		

	public InstructorResourcePage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	@Step("Click View Instructor Resources Button on Educator Page ,   Method: {method}")
	public boolean clickViewInstructorResourcesButton() {

		boolean viewInstructorResourcesButtonExist = false;
		try {
			viewInstructorResourcesButton.isDisplayed();
			viewInstructorResourcesButton.click();

		} catch (Exception e) {
			return false;
		}

		return viewInstructorResourcesButtonExist;
	}

	@Step("Get Instructor Resources page header,   Method: {method}")
	public String getInstrctorResourcePageHeader() {
		return headerText_InstructorResources.getText();
	}

	@Step("Click First Instructor Resources Button on Instructor Resources Page ,   Method: {method}")
	public void clickInstructorResourceButton(int num) {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.elementToBeClickable(InstructorResourcesButtonsList.get(0)));
		
			InstructorResourcesButtonsList.get(num).click();

	}

	// --> Added By Surabhi

	@Step("Verify Request Access button present in Instructor Resource Page ,   Method: {method}")
	public WebElement verifyRequestAccessButton() {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Instructor Resources']")));

		try {
			if (requestAccessButton.isDisplayed() == true) {
				System.out.println("Request access button is present");
			}
		} catch (Exception e) {
			System.out.println("Request access button is not present");
			return requestAccessButton;
		}

		return requestAccessButton;
	}

	@Step("Click RequestAccessButton on Instructor Resources - BDP Page ,   Method: {method}")
	public void clickRequestAccessButton_BDP() {
		requestAccessButton.click();

	}

	@Step("Verify and Click Instructor Button on Instructor Resource Overlay ,   Method: {method}")
	public boolean verifyInstructorResourceOverlay_Instructor() {

		boolean verifyInstructorResourceOverlayExist = false;
		try {
			verifyInstructorResourceOverlayExist = verifyInstructorResourceOverlay.isDisplayed();
			if (verifyInstructorResourceOverlayExist == true) {
				InstrctorButton_InstructorResourceOverlay.click();
			}

		} catch (Exception e) {
			e.getMessage();
		}

		return verifyInstructorResourceOverlayExist;
	}
	
	
	@Step("Verify and Click I'm Student Button on Instructor Resource Overlay ,   Method: {method}")
	public boolean verifyInstructorResourceOverlay_Student() throws InterruptedException {

		boolean verifyInstructorResourceOverlayExist = false;
			try {
					verifyInstructorResourceOverlayExist = verifyInstructorResourceOverlay.isDisplayed();
						if (verifyInstructorResourceOverlayExist == true) {
							StudentButton_InstructorResourceOverlay.click();
						}

				} catch (Exception e) {
						e.getMessage();
						return false;
				}
			
			return true;
	}



	@Step("Verify and Click Close button on Instructor Status Overlay ,   Method: {method}")
	public boolean verifyInstructorStatus_Student() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[@class='modal-title']")));
		boolean verifyInstructorStatusOverlayExist = false;
			try {
					verifyInstructorStatusOverlayExist = verifyInstructorStatusOverlay.isDisplayed();
						if (verifyInstructorStatusOverlayExist == true) {
							verifyNotice_InstructorStatusOverlay.isDisplayed();
							closeButton_InstructorStatusOverlay.click();
						}

				} catch (Exception e) {
					e.getMessage();
					return false;
				}

			return true;
	}

	
	@Step("Verify School Information Overlay ,   Method: {method}")
	public boolean verifySchoolInformationOverlay() {

		boolean verifySchoolInformationOverlayExist = false;
		try {
			verifySchoolInformationOverlayExist = verifySchoolInformationOverlay.isDisplayed();

		} catch (Exception e) {
			e.getMessage();
		}

		return verifySchoolInformationOverlayExist;
	}

	@Step("Verify and Click Close button on Submitted Request Overlay ,   Method: {method}")
	public boolean verifySubmittedRequestOverlay() {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[@class='modal-title']")));
		boolean verifySubmittedRequestOverlayExist = false;
		try {
			verifySubmittedRequestOverlayExist = verifySubmittedRequestOverlay.isDisplayed();
			if (verifySubmittedRequestOverlayExist == true) {
				closeButton_SubmittedRequestOverlay.click();
			}

		} catch (Exception e) {
			e.getMessage();
		}

		return verifySubmittedRequestOverlayExist;
	}

	
	@Step("Select school state and school name for USA College,   Method: {method}")
	public void selectSchoolStateUSACollege(String stateNameUSA, String schNameUSA) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		Thread.sleep(2000);
		Actions act1 = new Actions(driver);
		act1.moveToElement(SchoolStateDropdown_College).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//*[@id='react-select-2-option-0']")));
		for (int i = 0; i <= 58; i++) {
			WebElement drpDownValue = driver.findElement(
					By.xpath("" + "//*[@id='react-select-2-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(stateNameUSA)) {
				act1.click(drpDownValue).build().perform();
				break;
			}

		}

		wait.until(ExpectedConditions.elementToBeClickable(SchoolNameDropdown_College));
		Actions act2 = new Actions(driver);
		act2.moveToElement(SchoolNameDropdown_College).click().build().perform();
		//((JavascriptExecutor) driver).executeScript("arguments[0].click();", SchoolNameDropdown_College);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//*[@id='react-select-3-option-0']")));
		for (int i = 0; i <= 101; i++) {
			WebElement drpDownValue = driver.findElement(
					By.xpath("" + "//*[@id='react-select-3-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(schNameUSA)) {
				act2.click(drpDownValue).build().perform();
				break;

			}
		}
	}

	
	@Step("Select Term,   Method: {method}")
	public void selectTerm(String Term) throws InterruptedException {
		Actions act = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		act.moveToElement(termDropDown).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//label[text()='Term']/following-sibling::div/div/ul[@class='dropdown-menu']/li[1]")));
		for (int i = 1; i <= 4; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath(
					"//label[text()='Term']/following-sibling::div/div/ul[@class='dropdown-menu']/li[" + i + "]"));
			if (drpDownValue.getText().equalsIgnoreCase(Term)) {
				act.click(drpDownValue).build().perform();
				break;

			}
		}
	}

	@Step("Select Year,   Method: {method}")
	public void selectYear(String Year) throws InterruptedException {
		Actions act = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		act.moveToElement(yearDropDown).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//label[text()='Year']/following-sibling::div/div/ul[@class='dropdown-menu']/li[1]")));
		for (int i = 1; i <= 5; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath(
					"//label[text()='Year']/following-sibling::div/div/ul[@class='dropdown-menu']/li[" + i + "]"));
			if (drpDownValue.getText().equalsIgnoreCase(Year)) {
				act.click(drpDownValue).build().perform();
				break;

			}
		}
	}

	@Step("Select school state and school name for USA Highschool,   Method: {method}")
	public void selectSchoolStateUSAHighSchool(String stateNameUSA, String schNameUSA) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		Thread.sleep(1000);
		HighschoolSection.click();
		Actions act3 = new Actions(driver);
		act3.moveToElement(SchoolStateDropdown_Highschool).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-4-option-0']")));
		for (int i = 0; i <= 58; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-4-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(stateNameUSA)) {
				act3.click(drpDownValue).build().perform();
				break;
			}

		}

		Thread.sleep(2000);
		Actions act4 = new Actions(driver);
		act4.moveToElement(SchoolNameDropdown_Highschool.get(0)).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-5-option-0']")));
		for (int i = 0; i <= 2245; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-5-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(schNameUSA)) {
				act4.click(drpDownValue).build().perform();
				break;

			}
		}
	}
	
	@Step("Select school state and school name for International,   Method: {method}")
	public void selectSchoolStateInternational(String rgnName, String prnName, String stateName, String bookName) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		Actions act1 = new Actions(driver);
		act1.moveToElement(RegionStateDropdown_International).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//*[@id='react-select-6-option-0']")));
		for (int i = 0; i <= 58; i++) {
			WebElement drpDownValue = driver.findElement(
					By.xpath("" + "//*[@id='react-select-6-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(rgnName)) {
				act1.click(drpDownValue).build().perform();
				break;
			}

		}
		
		Thread.sleep(2000);
		Actions act2 = new Actions(driver);
		act2.moveToElement(ProvinceDropdown_International).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//*[@id='react-select-7-option-0']")));
		for (int i = 0; i <= 58; i++) {
			WebElement drpDownValue = driver.findElement(
					By.xpath("" + "//*[@id='react-select-7-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(prnName)) {
				act2.click(drpDownValue).build().perform();
				break;
			}

		}

		Thread.sleep(2000);
		Actions act3 = new Actions(driver);
		act3.moveToElement(SchoolStateDropdown_International).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//*[@id='react-select-8-option-0']")));
		for (int i = 0; i <= 2245; i++) {
			WebElement drpDownValue = driver.findElement(
					By.xpath("" + "//*[@id='react-select-8-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(stateName)) {
				act3.click(drpDownValue).build().perform();
				break;

			}
		}
		
		Thread.sleep(2000);
		Actions act4 = new Actions(driver);
		act4.moveToElement(DeptDropdown_International).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//*[@id='react-select-9-option-0']")));
		for (int i = 0; i <= 2245; i++) {
			WebElement drpDownValue = driver.findElement(
					By.xpath("" + "//*[@id='react-select-9-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(bookName)) {
				act4.click(drpDownValue).build().perform();
				break;

			}
		}
	}

	@Step("Verify Request Exam Copy button present in Instructor Resource Page ,   Method: {method}")
	public WebElement verifyRequestExamCopyButton() {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//h1[text()='Instructor Resources']")));

		try {
			if (requestExamCopyButton.isDisplayed() == true) {
				System.out.println("Request exam copy button is present");
			}
		} catch (Exception e) {
			System.out.println("Request access button is not present");
			return requestExamCopyButton;
		}

		return requestExamCopyButton;
	}

	@Step("Click Request Exam Copy button on Instructor Resources - BDP Page ,   Method: {method}")
	public void clickRequestExamCopyButton() {
		requestExamCopyButton.click();

	}

	@Step("Select Department name,   Method: {method}")
	public void selectDepartmentName(String depName) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		Thread.sleep(1000);
		Actions act3 = new Actions(driver);
		act3.moveToElement(DepartmentDropdown).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//*[@id='react-select-4-option-1']")));
		for (int i = 0; i <= 2; i++) {
			WebElement drpDownValue = driver.findElement(
					By.xpath("" + "//*[@id='react-select-4-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(depName)) {
				act3.click(drpDownValue).build().perform();
				break;
			}

		}

	}

	@Step("Select current date for decision date,   Method: {method}")
	public void selectDecisionDate() {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		decisionDateCalendar.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@class='react-datepicker__month']")));
		WebElement calendarDate = driver.findElement(By.xpath
				("//div[@class='react-datepicker__month']/div/div[contains(@class,'today')]"));
			//if(calendarDate.getText().equalsIgnoreCase(currnetDay)) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", calendarDate);

			//}
		}
	
	@Step("Select school state and school name for Exam copy request - Highschool,   Method: {method}")
	public void selectSchoolStateExamCopyHighSchool(String stateNameUSA, String schNameUSA) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		Thread.sleep(1000);
		HighschoolSection.click();
		Actions act3 = new Actions(driver);
		act3.moveToElement(SchoolStateDropdown_Highschool).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-5-option-0']")));
		for (int i = 0; i <= 58; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-5-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(stateNameUSA)) {
				act3.click(drpDownValue).build().perform();
				break;
			}

		}

		Thread.sleep(2000);
		Actions act4 = new Actions(driver);
		act4.moveToElement(SchoolNameDropdown_Highschool.get(0)).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-6-option-0']")));
		for (int i = 0; i <= 2245; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-6-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(schNameUSA)) {
				act4.click(drpDownValue).build().perform();
				break;

			}
		}
	}
	
	@Step("Select Department name for exam copy request - Highschool,   Method: {method}")
	public void selectDepartmentNameExamCopy_Highschool(String depNameHS) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		Thread.sleep(1000);
		Actions act5 = new Actions(driver);
		act5.moveToElement(DepartmentDropdown_Highschool).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-7-option-0']")));
		for (int i = 0; i <= 1; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-7-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(depNameHS)) {
				act5.click(drpDownValue).build().perform();
				break;
			}

		}
	}
	
	@Step("Check all the assets present in Instructor  Page ,   Method: {method}")
	public boolean checkAllAssets(int numberOfAssets) {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//h1[contains(text(),'Instructor Resources')]")));
		boolean assetsExist = false;
		try {
				if(Assets.size() == numberOfAssets) {
					assetsExist = true;
				}

		} catch (Exception e) {
			return false;
		}
		
		return assetsExist;
	}
	
	@Step("Check IIG Demo and View Full Site buttons ,   Method: {method}")
	public boolean checkIIGButtons() {

		boolean IIGButtonsExist = false;
		try {
				IIGDemoButton.isDisplayed();
				IIGViewFullSiteButton.isDisplayed();
				IIGButtonsExist = true;

		} catch (Exception e) {
			return false;
		}

		return IIGButtonsExist;
	}
	
	
	@Step("Check IIG Demo Site Sign In Text,   Method: {method}")
	public String checkIIGDemoSite_NoSignIn(String parentWindow) {
		IIGDemoButton.click();
		ReusableMethods.getNewWindow(parentWindow);
		String loginString = loginText.getText();
		driver.close();
		driver.switchTo().window(parentWindow);
		return loginString;
	}
	
	
	@Step("Check IIG Full Site Signed In Text,   Method: {method}")
	public String checkIIGFullSite_SignedIn(String parentWindow) {
		IIGViewFullSiteButton.click();
		ReusableMethods.getNewWindow(parentWindow);
		String SignedInString = SignedInText.getText();
		return SignedInString;

	}
	
	@Step("SignedOut from IIG Site,   Method: {method}")
	public String signOut_IIGSite(String parentWindow) throws InterruptedException {

		gearMenuIcon_IIGSite.click();
		Thread.sleep(2000);
		signOutLink.click();
		Thread.sleep(2000);
		driver.close();
		driver.switchTo().window(parentWindow);
		driver.navigate().refresh();
		Thread.sleep(2000);
		return profileLogIn.getText();
		
	}
	
	@Step("Check IIG Demo and View Full Site buttons ,   Method: {method}")
	public boolean checkSelectEditionError() {

		boolean selectEditionErrExist = false;
		try {
				selectEditionError.isDisplayed();
				selectEditionErrExist = true;

		} catch (Exception e) {
			return false;
		}

		return selectEditionErrExist;
	}
	
}
