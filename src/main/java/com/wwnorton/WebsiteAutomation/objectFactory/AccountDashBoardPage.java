package com.wwnorton.WebsiteAutomation.objectFactory;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;



public class AccountDashBoardPage {
	
	WebDriver driver;
	
	@FindBy(xpath = "//div[text()='Name']//following-sibling::div//div[@class='account-left-section']")
	public WebElement profileNameElement;
	
	@FindBy(xpath = "//div[text()='Email']//following-sibling::div//div[@class='account-left-section']")
	public WebElement emailIDElement;
	
	@FindBy(xpath = "//a[@class='buttonlabel' and text()='Change Password']")
	public WebElement changePasswordElement;
	
	@FindBy(xpath = "//h4[@class='modal-title' and text()='Change Password']")
	public WebElement changePasswordModal;
	
	@FindBy(how = How.XPATH, using = "//input[@id='currentPassword']")
	public WebElement currentPassword;
	
	@FindBy(how = How.XPATH, using = "//input[@id='accountPassword']")
	public WebElement accountPassword;
	
	@FindBy(how = How.XPATH, using = "//input[@id='accountRetypePassword']")
	public WebElement accountRetypePassword;
	
	@FindBy(how = How.XPATH, using = "//button[@id='LoginSubmitButton']")
	public WebElement changePasswordButton;
	
	@FindBy(xpath = "//h4[@class='modal-title' and text()='Confirmation']")
	public WebElement changePasswordConfirmation;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Close']")
	public WebElement closeButton;
	
	@FindBy(xpath = "//span/a[@class='OrderDetailLink']")
	public List <WebElement> orderDetailLink;
	
	@FindBy(xpath = "//div/div/div[contains(@class,'nopadding OrderNumber')]")
	public WebElement orderNumber;
	
	@FindBy(how = How.XPATH, using = "//a[@class='buttonlabel' and text()='Add Address']")
	public WebElement addAddressLink;
	
	@FindBy(how = How.XPATH, using = "//a[@class='buttonlabel panelEdit' and text()='Edit']")
	public List<WebElement> editAddressLinks;
	
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"profilePanel\"]/div[2]/div/div/div[2]/div[3]")
	public WebElement addressOne;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"profilePanel\"]/div[2]/div/div/div[3]/div[3]")
	public WebElement addressTwo;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"profilePanel\"]/div[2]/div/div/div[3]/div[2]/div")
	public WebElement addressTwoUpdate;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Save')]")
	public WebElement addressTwoSave;
	
	@FindBy(how = How.XPATH, using = "//h4[@class='modal-title' and text()='Add Address']")
	public WebElement verifyAddAddreaaOverlay;
	
	@FindBy(how = How.XPATH, using = "//h4[@class='modal-title' and text()='Edit Address']")
	public WebElement verifyEditAddreaaOverlay;
	
	@FindBy(how = How.XPATH, using = "//input[@id='firstName']")
	public WebElement firstName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='lastName']")
	public WebElement lastName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='address1']")
	public WebElement addressLine1;
	
	@FindBy(how = How.XPATH, using = "//input[@id='city']")
	public WebElement city;
	
	@FindBy(how = How.XPATH, using = "//label[@class='control-label' and text()='State']/following-sibling::div/div/div/div[1]")
	public WebElement stateDropDownList;
	
	@FindBy(how = How.XPATH, using = "//input[@id='zip']")
	public WebElement zipCode;
	
	@FindBy(how = How.XPATH, using = "//input[@id='phone']")
	public WebElement phoneNumber;
	
	@FindBy(how = How.XPATH, using = "//button[@id='ProfileAddressSubmitButton' and text()='Save']")
	public WebElement saveButton;
	
	@FindBy(how = How.XPATH, using = "//button[@id='nobutton' and text()='Remove']")
	public WebElement removeButton;
	
	
	
	public AccountDashBoardPage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}
	
	
	@Step("Get Profile Name in Account Dashboard,   Method: {method}")
	public String getProfileName() throws IOException {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(profileNameElement));
		return profileNameElement.getText();

	}
	
	@Step("Get Profile Email ID in Account Dashboard,   Method: {method}")
	public String getProfileEmailID() throws IOException {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(emailIDElement));
		return emailIDElement.getText();

	}
	
	
	@Step("Check Change Password link in Account Dashboard,   Method: {method}")
	public boolean checkChangePasswordLink() throws IOException {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(changePasswordElement));
		return changePasswordElement.isDisplayed();

	}
	
	@Step("Update Password in Account Dashboard,   Method: {method}")
	public void updateAccountPassword(String CurrentPSWD, String NewPSWD) throws IOException {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(changePasswordElement));
		changePasswordElement.click();
		wait.until(ExpectedConditions.visibilityOf(changePasswordModal));
		currentPassword.sendKeys(CurrentPSWD);
		accountPassword.sendKeys(NewPSWD);
		accountRetypePassword.sendKeys(NewPSWD);
		changePasswordButton.click();
		wait.until(ExpectedConditions.visibilityOf(changePasswordConfirmation));
		closeButton.click();

	}
	
	@Step("Click Order Detail link in Account Dashboard,   Method: {method}")
	public void clickAccountDetailLink() throws IOException {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		int noOfLinks = orderDetailLink.size();
		wait.until(ExpectedConditions.visibilityOf(orderDetailLink.get(0)));
		
		if (noOfLinks >= 1) {
		orderDetailLink.get(0).click();
		} 
		//else {
		//	orderDetailLink.get(noOfLinks-1).click();
		//}
	}
	
	
	@Step("Click View Order Detail link in Account Dashboard,   Method: {method}")
	public String clickViewOrderDetailsLink() throws IOException {
		String  bookName = null;
		int OrdersLength = orderDetailLink.size();
		for (int i=1; i>= OrdersLength; i++) {
			WebElement bookNameElement = driver.findElement(By.xpath("//tbody/tr[" + i + "]/td/div[@class='title']"));
			bookName = bookNameElement.getText();
		}
		
		return bookName;
	}
	
	
	
	@Step("Get Order Number in Order Details Page,   Method: {method}")
	public String getOrderNumber() throws IOException {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(orderNumber));
		return orderNumber.getText();
	}
	
	
	
	@Step("Check Add Address link exist,  Method: {method} ")
	public boolean checkAddAddressLink() throws Exception {
		
		boolean addAddressLinkExist;
		if(editAddressLinks.size() <= 3 ) {
			try 
			{
				addAddressLinkExist = addAddressLink.isDisplayed();
			
					if(addAddressLinkExist)
					{
						return true;
					}	
			} catch (NoSuchElementException e) {
					throw new NoSuchElementException("Error: No Shipping Address exist " 
							+ e.getMessage());	
					}
			}
		
		return false;
	}
	
	@Step("Add Address Information if does not exist,  Method: {method} ")
	public void addShiipingAddress(String fName, String lName, String address, String cityName, String stateName, String zip, int[] phoneIntArray) throws Exception {

		if(editAddressLinks.size() <= 4 ) {
			
			addAddressLink.click();
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.visibilityOf(verifyAddAddreaaOverlay));
			firstName.sendKeys(fName);
			lastName.sendKeys(lName);
			addressLine1.sendKeys(address);
			city.sendKeys(cityName);
			Thread.sleep(1000);
			Actions act1 = new Actions(driver);
			act1.moveToElement(stateDropDownList).click().build().perform();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@id,'option-0')]")));
			for (int i = 0; i <= 58; i++) {
				WebElement drpDownValue = driver.findElement(By.xpath("" + "//div[contains(@id,'option-" + i + "')]"));
				if (drpDownValue.getText().equalsIgnoreCase(stateName)) {
					act1.click(drpDownValue).build().perform();
					break;
				}
			}
			Thread.sleep(1000);
			zipCode.sendKeys(zip);
			Thread.sleep(1000);
			phoneNumber.click();
			JSONArray phoneArray = new JSONArray(phoneIntArray);
			for(int i=0; i<phoneArray.length(); i++) {
				String PhoneNumber = phoneArray.get(i).toString();
				phoneNumber.sendKeys(PhoneNumber);
			}
			
			saveButton.click();
		}
	}
	
	
	@Step("Update Address Information if does not exist,  Method: {method} ")
	public void editShiipingAddress(String accountName, String lName, String address) throws Exception {
		
		if(editAddressLinks.size() == 4 ) {
			Thread.sleep(2000);
			WebElement recentEditLink = driver.findElement(By.xpath("//div[text()='" + accountName + "']"
					+ "/following-sibling::div/a[@class='buttonlabel panelEdit' and text()='Edit']"));
			recentEditLink.click();
			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.visibilityOf(verifyEditAddreaaOverlay));
			clearFieldValues(lastName);
			lastName.sendKeys(lName);
			clearFieldValues(addressLine1);
			addressLine1.sendKeys(address);
			saveButton.click();
		}
	}
	
	public void clearFieldValues(WebElement ele) throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOf(ele));
		String fieldLen = ele.getAttribute("value");
		int lengthText = fieldLen.length();

		for(int i = 0; i < lengthText; i++){
			ele.sendKeys(Keys.ARROW_LEFT);
			ele.sendKeys(Keys.DELETE);
		}
	}
	
	@Step("Remove Address Information if does not exist,  Method: {method} ")
	public void removeShiipingAddress() throws Exception {
		Thread.sleep(2000);
		if(editAddressLinks.size() <= 4 ) {
			for(int i = 3; i >=0; i--){
				editAddressLinks.get(i).click();
				Thread.sleep(2000);
				removeButton.click();	
			}
		}
	}
	
	
	
	@Step("Get Shipping Address One Information,  Method: {method} ")
	public String getAddressOne() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(addressOne));
		String AddressOne = addressOne.getText();
		return AddressOne;
	}
	
	@Step("Get Shipping Address Two Information,  Method: {method} ")
	public String getAddressTwo() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(addressTwo));
		String AddressTwo = addressTwo.getText();
		return AddressTwo;	
	}
	
	@Step("Validate multiple shipping addresses exist,  Method: {method} ")
	public boolean multipleAddresses() throws Exception {
		
		boolean AddressOneExist;
		boolean AddressTwoExist;
		
		try 
		{
			Thread.sleep(3000);
			AddressOneExist = addressOne.isDisplayed();
			AddressTwoExist = addressTwo.isDisplayed();
			
			if(AddressOneExist && AddressTwoExist)
			{
				return true;
			}	
		} catch (NoSuchElementException e) {
				throw new NoSuchElementException("Error: No Shipping Address exist " 
						+ e.getMessage());	
		}
		
		return false;
	}
	
	@Step("Update the Address Two,  Method: {method} ")
	public void editAddressTwo() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(addressTwoUpdate));
		addressTwoUpdate.click();
		
		wait.until(ExpectedConditions.visibilityOf(addressTwoSave));
		addressTwoSave.click();
	}

}
