package com.wwnorton.WebsiteAutomation.objectFactory;

import org.json.JSONArray;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class NortonCustomFormPage {
	
	WebDriver driver;

	
	@FindBy(how = How.XPATH, using = "//button/span[text()='Contact Norton Custom']")
	public WebElement contactNortonCustomButton;
	
	@FindBy(how = How.XPATH, using = "//h4[@class='modal-title' and text()='Your Contact Information']")
	public WebElement verifyContactInformationOverlay;
	
	@FindBy(how = How.XPATH, using = "//input[@id='name']")
	public WebElement contactName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='email']")
	public WebElement contactEmail;
	
	@FindBy(how = How.XPATH, using = "//input[@id='phoneNumber']")
	public WebElement phoneNumber;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'selectDropdown')]")
	public WebElement selectRole;
	
	@FindBy(how = How.XPATH, using = "//button[@id='ProfileAddressSubmitButton' and text()='Continue']")
	public WebElement continueButton;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'State')]/following-sibling::div[contains(@class,'selectDropdown')]")
	public WebElement SchoolStateDropdown;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Name')]/following-sibling::div[contains(@class,'selectDropdown')]")
	public WebElement SchoolNameDropdown;
	
	@FindBy(how = How.XPATH, using = "//input[@id='courseSubject']")
	public WebElement courseSubject;
	
	@FindBy(how = How.XPATH, using = "//input[@id='courseName']")
	public WebElement courseName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='courseEnrollment']")
	public WebElement courseEnrollment;
	
	@FindBy(how = How.XPATH, using = "//textarea[@id='aboutYourRequest']")
	public WebElement aboutYourRequest;
	
	@FindBy(how = How.XPATH, using = "//button[@id='ProfileAddressSubmitButton' and text()='Submit']")
	public WebElement submitButton;
	
	@FindBy(how = How.XPATH, using = "//h4[@class='modal-title' and text()='Confirmation']")
	public WebElement verifyConnfirmationOverlay;
	
	@FindBy(how = How.XPATH, using = "//button[@id='AddEmailSubmitButton' and text()='Close']")
	public WebElement closeButton;
	
	
	public NortonCustomFormPage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}
	

	@Step("Click Contact Norton Custom Button on Norton Custom Contact Page ,   Method: {method}")
	public boolean clickContactNortonCustomButton() {

		boolean contactNortonCustomButtonExist = false;
		try {
				contactNortonCustomButtonExist = contactNortonCustomButton.isDisplayed();
				contactNortonCustomButton.click();

			} catch (Exception e) {
				return false;
		}
		
		return contactNortonCustomButtonExist;
	}
	
	
	@Step("Verify Contact Information Overlay ,   Method: {method}")
	public boolean verifyContactInformationOverlay() {

		boolean verifyContactInformationOverlayExist = false;
		try {
				verifyContactInformationOverlayExist = verifyContactInformationOverlay.isDisplayed();
				
		} catch (Exception e) {
			e.getMessage();
		}
		
		return verifyContactInformationOverlayExist;
	}
	
	
	@Step("Submit Contact Information Form,   Method: {method}")
	public void submitContactInformationForm(String name, String emailID, int[] phoneIntArray, String role) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//h4[@class='modal-title']")));
		
		contactName.sendKeys(name);
		contactEmail.sendKeys(emailID);
		Thread.sleep(1000);
		phoneNumber.click();
		JSONArray phoneArray = new JSONArray(phoneIntArray);
		for(int i=0; i<phoneArray.length(); i++) {
			String PhoneNumber = phoneArray.get(i).toString();
			phoneNumber.sendKeys(PhoneNumber);

		}
		
		Actions act = new Actions(driver);
		act.moveToElement(selectRole).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-2-option-0']")));
		for (int i = 0; i <= 58; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-2-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(role)) {
				act.click(drpDownValue).build().perform();
				break;
			}
		}
		
		Thread.sleep(2000);
		continueButton.click();

	}
	
	
	@Step("Select school state and school name for USA College,   Method: {method}")
	public void selectSchoolStateUSACollege(String stateNameUSA, String schNameUSA) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		Thread.sleep(1000);
		Actions act1 = new Actions(driver);
		act1.moveToElement(SchoolStateDropdown).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-3-option-0']")));
		for (int i = 0; i <= 58; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-3-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(stateNameUSA)) {
				act1.click(drpDownValue).build().perform();
				break;
			}

		}

		Thread.sleep(2000);
		Actions act2 = new Actions(driver);
		act2.moveToElement(SchoolNameDropdown).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-4-option-0']")));
		for (int i = 0; i <= 5; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-4-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(schNameUSA)) {
				act2.click(drpDownValue).build().perform();
				break;
			}
		}
	}
	
	
	@Step("Submit Request Details Form,   Method: {method}")
	public void submitRequestDetailsForm(String courseSub, String courseText, String courseEnr, String comments) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//h4[@class='modal-title']")));
		
		courseSubject.sendKeys(courseSub);
		courseName.sendKeys(courseText);
		courseEnrollment.sendKeys(courseEnr);
		aboutYourRequest.sendKeys(comments);
		Thread.sleep(2000);
		submitButton.click();

	}
	
	@Step("Verify Contact Information Overlay ,   Method: {method}")
	public boolean verifyConnfirmationOverlay() {

		boolean verifyConfirmationOverlayExist = false;
		try {
			verifyConfirmationOverlayExist = verifyConnfirmationOverlay.isDisplayed();
				closeButton.click();		
				
		} catch (Exception e) {
			e.getMessage();
		}
		
		return verifyConfirmationOverlayExist;
	}

}
