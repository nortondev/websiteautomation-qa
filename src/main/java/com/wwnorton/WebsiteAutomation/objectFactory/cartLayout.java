package com.wwnorton.WebsiteAutomation.objectFactory;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class cartLayout {
	
	WebDriver driver;
	
	
	// Finding Web Elements on Book Details page with respect to Add to Cart using PageFactory.
	
	
	@FindBy(how = How.XPATH, using = "//button[@id='CheckoutButton']")
	WebElement checkoutButton;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Email']")
	WebElement userName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='loginPassword']")
	WebElement password;
	
	@FindBy(how = How.XPATH, using = "//button[@id='LoginSubmitButton']")
	WebElement logInButton;
	
	@FindBy(how = How.CSS, using = "#creatAccountEmail")
	WebElement createAccountEmail;
	
	@FindBy(how = How.CSS, using = "#CreateAccountSubmitButton")
	WebElement createAccountButton;
	
	@FindBy(how = How.XPATH, using = "//input[@id='accountFirstName']")
	WebElement createAccountfName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='accountLastName']")
	WebElement createAccountlName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='accountReTypeEmail']")
	WebElement reTypeEmail;
	
	@FindBy(how = How.XPATH, using = "//input[@id='accountPassword']")
	WebElement Password;

	@FindBy(how = How.XPATH, using = "//button[@id='LoginSubmitButton']")
	WebElement loginButton;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
	WebElement continueButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='text-center float-left ModalWizardHeader']")
	WebElement accountConfirmation;
	
	@FindBy(how = How.XPATH, using = "//div[@aria-expanded='true']/div/div/li/div/div/div/button[@class='btn btn-panelButton' and text()='Add']")
	WebElement webOfferAddButton;
	
	@FindBy(how = How.XPATH, using = "//span[@class='promoCheckMarkText' and text()='Promo Applied']")
	WebElement webOfferPromo;
	
	@FindBy(how = How.XPATH, using = "//div[@class='cart-item']/div/div[@class='cartBookPrice']")
	WebElement cartBookPrice;
	
	
	
	
	// Initializing Web Driver and PageFactory.
	
	public cartLayout() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);	
	}

	
	// Navigate to Checkout page as New/Existing user.
	
	
	@Step("Click on Checkout,  Method: {method} ")
	public void clickCheckout() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(checkoutButton));
		checkoutButton.click();	
	}
	
	@Step("Login with an Existing user,  Method: {method} ")
	public void accountLogin() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(userName));
		userName.sendKeys();
		wait.until(ExpectedConditions.visibilityOf(password));
		password.sendKeys();
		
		Thread.sleep(3000);
		logInButton.click();
		
	}
	

	@Step("Create a New Login Account,  Method: {method} ")
	public String createAccount() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		createAccountEmail.sendKeys();
		createAccountButton.click();
		createAccountfName.sendKeys();
		createAccountlName.sendKeys();
		reTypeEmail.sendKeys();
		Password.sendKeys();
		loginButton.click();

		wait.until(ExpectedConditions.visibilityOf(accountConfirmation));
		String confirmationText = accountConfirmation.getText();
		return confirmationText;
		
	}
	
	
	
	@Step("Continue to Checkout,  Method: {method} ")
	public void continueCheckout() throws Exception {	
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(continueButton));
		continueButton.click();
		
	}
	
	@Step("Add Web Offeer in Checkout,  Method: {method} ")
	public boolean addWebOffer() throws Exception {	
		WebDriverWait wait = new WebDriverWait(driver, 25);
		
		boolean webOfferAddButtonExist = false;
		
		try {
				wait.until(ExpectedConditions.visibilityOf(webOfferAddButton));
				webOfferAddButtonExist = webOfferAddButton.isDisplayed();
				if (webOfferAddButtonExist == true) {
					webOfferAddButton.click();
				}
		} catch (Exception e) {
			e.getMessage();
		}
		
		return webOfferAddButtonExist;
		
	}
	
	
	@Step("Verify Web Offer Promo Text,  Method: {method} ")
	public String webOfferPromoTextExist() throws Exception {	
		WebDriverWait wait = new WebDriverWait(driver, 25);
		boolean webOfferPromoTextExist = false;
		String promotionalText = null;
		
		try {
				wait.until(ExpectedConditions.visibilityOf(webOfferPromo));
				webOfferPromoTextExist = webOfferPromo.isDisplayed();
				if (webOfferPromoTextExist == true) {
					promotionalText = webOfferPromo.getText();
				}
		} catch (Exception e) {
			e.getMessage();
		}
		
		return promotionalText;
		
	}
	
	@Step("Get Book Price in Cart Layout ,  Method: {method} ")
	public String getBookPrice_CartLayOut() throws Exception {	
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(cartBookPrice));
		return cartBookPrice.getText();
		
	}
	
}
