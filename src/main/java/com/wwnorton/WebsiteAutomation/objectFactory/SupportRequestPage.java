package com.wwnorton.WebsiteAutomation.objectFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class SupportRequestPage {
	WebDriver driver;


	@FindBy(how = How.XPATH, using = "//input[@id='txtFullName']")
	public WebElement firstName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='txtEmail']")
	public WebElement email;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Staff']/preceding-sibling::span")
	public WebElement yourRole;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Country']/following-sibling::div[contains(@class, selectDropdown)]")
	public WebElement countryDrpDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='State/Province']/following-sibling::div[contains(@class, selectDropdown)]")
	public WebElement stateDrpDown;

	@FindBy(how = How.XPATH, using = "//label[@for='schoolType0']/span[@data-style='radio']")
	public WebElement collegeRadioOption;
	
	@FindBy(how = How.XPATH, using = "//label[text()='School Name']/following-sibling::div[contains(@class, selectDropdown)]")
	public WebElement schoolNameDrpDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Category']/following-sibling::div[contains(@class, selectDropdown)]")
	public WebElement categoryDrpDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Type of Issue']/following-sibling::div[contains(@class, selectDropdown)]")
	public WebElement typeOfIssueDrpDown;
	
	@FindBy(how = How.XPATH, using = "//label[text()='Discipline']/following-sibling::div[contains(@class, selectDropdown)]")
	public WebElement disciplineDrpDown;
	
	@FindBy(how = How.XPATH, using = "//input[@id='txtBookTitle']")
	public WebElement bookTitle;
	
	@FindBy(how = How.XPATH, using = "//input[@id='txtEmailSubject']")
	public WebElement subject;
	
	@FindBy(how = How.XPATH, using = "//textarea[@id='comments']")
	public WebElement comments;
	
	@FindBy(how = How.XPATH, using = "//div[@id='agreeTermsField']/input")
	public WebElement tNc;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'submit-btn')]/button")
	public WebElement submitBtn;
	
	@FindBy(how = How.XPATH, using = "//span[@class='submittedSupportText' and text()='Your ticket has been assigned number: ']/b")
	public WebElement supportTicketNumber;
	
	
	

	public SupportRequestPage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}


	@Step("Enter Customer Information in the Support Ticket Request,   Method: {method}")
	public void enterCustomerInformation(String fName, String Email) throws InterruptedException {
	
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		firstName.sendKeys(fName);
		email.sendKeys(Email);
		yourRole.click();		
	
	}
	
	@Step("Enter School Information in the Support Ticket Request,   Method: {method}")
	public void enterSchoolInformation(String CntName, String StName, String schName) throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		countryDrpDown.click();
		Thread.sleep(2000);
		WebElement countryOption;
		String countryName;
		for(int i=0; i<3; i++)
		{
			countryOption = driver.findElement(By.id("react-select-2-option-" + i));
			countryName = countryOption.getText();
			if(countryName.equals(CntName))
			{
				countryOption.click();		
				break;
			}
		}
		
		Thread.sleep(2000);
		stateDrpDown.click();
		Thread.sleep(2000);
		WebElement stateOption;
		String stateName;
		for(int i=0; i<3; i++)
		{
			stateOption = driver.findElement(By.id("react-select-3-option-" + i));
			stateName = stateOption.getText();
			if(stateName.equals(StName))
			{
				//stateOption.click();		
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", stateOption);
				break;
			}
		}
		
		collegeRadioOption.click();
		schoolNameDrpDown.click();
		Thread.sleep(2000);
		WebElement schoolNameOption;
		String schoolName;
		for(int i=0; i<105; i++)
		{
			schoolNameOption = driver.findElement(By.id("react-select-4-option-" + i));
			schoolName = schoolNameOption.getText();
			if(schoolName.equals(schName))
			{
				//schoolNameOption.click();
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", schoolNameOption);
				break;
			}
		}
	
	}
	
	
	@Step("Enter School Information in the Support Ticket Request,   Method: {method}")
	public void enterRequestInformation(String CtgName, String issName, String dscName, String title, String sub, String cmts) throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		ReusableMethods.scrollToBottom(driver);
		categoryDrpDown.click();
		Thread.sleep(2000);
		//list of all Category options
		List<WebElement> selectCategoryValues=driver.findElements(
				By.xpath("//*[contains(@id,'react-select-')]"));
		
		for(int i=0; i <selectCategoryValues.size(); i++)
		 { 
			WebElement categoryValue = selectCategoryValues.get(i);
			if(categoryValue.getText().equalsIgnoreCase(CtgName)) 
			{
				categoryValue.click();
				break;				
			}
		 }
		
		Thread.sleep(2000);
		typeOfIssueDrpDown.click();
		//((JavascriptExecutor) driver).executeScript("arguments[0].click();", typeOfIssueDrpDown);
		Thread.sleep(3000);
		List<WebElement> selectIssueValues=driver.findElements(
				By.xpath("//*[contains(@id,'react-select-')]"));
		
		for(int i=0; i <selectIssueValues.size(); i++)
		 { 
			WebElement issueValue = selectIssueValues.get(i);
			if(issueValue.getText().equalsIgnoreCase(issName)) 
			{
				issueValue.click();
				break;				
			}
		 }
		Thread.sleep(2000);
		disciplineDrpDown.click();
		//((JavascriptExecutor) driver).executeScript("arguments[0].click();", disciplineDrpDown);
		Thread.sleep(2000);
		List<WebElement> selectDisciplineValues=driver.findElements(
				By.xpath("//*[contains(@id,'react-select-')]"));
		
		for(int i=0; i <selectDisciplineValues.size(); i++)
		 { 
			WebElement dscValue = selectDisciplineValues.get(i);
			if(dscValue.getText().equalsIgnoreCase(dscName)) 
			{
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", dscValue);
				//dscValue.click();
				break;				
			}
		 }
		Thread.sleep(1000);
		bookTitle.sendKeys(title);
		Thread.sleep(1000);
		subject.sendKeys(sub);
		Thread.sleep(1000);
		comments.clear();
		comments.sendKeys(cmts);
		Thread.sleep(1000);
		//tNc.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", tNc);
	}


	@Step("Submit Support Ticket Request,   Method: {method}")
	public void submitRequest() throws InterruptedException {
		Thread.sleep(2000);
		//((JavascriptExecutor) driver).executeScript("arguments[0].click();", submitBtn);
		submitBtn.click();
	}
	
	
	@Step("Check if Support Ticket submission confirmation is displayed,   Method: {method}")
	public boolean chkReqConfirmationMsg() throws InterruptedException {
		boolean msgExist= false;
		WebElement confMsg;
		try {
			confMsg = driver.findElement(
					By.xpath("//div[@class='header']/h1[text()='Your request has been submitted. ']"));
			msgExist = confMsg.isDisplayed();
				if (msgExist==true){
					return true;
				}	
			} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Error: No Shipping Address exist " 
					+ e.getMessage());	
			}

		return false;
	}
	
	
	@Step("Get Support Ticket Number,   Method: {method}")
	public String getRequestNumber() throws InterruptedException {
		return supportTicketNumber.getText();
	}
	
}