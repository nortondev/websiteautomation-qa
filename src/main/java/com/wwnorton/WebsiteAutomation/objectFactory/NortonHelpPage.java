package com.wwnorton.WebsiteAutomation.objectFactory;


import org.json.JSONArray;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class NortonHelpPage {
	
	WebDriver driver;
	
	@FindBy(how = How.XPATH, using = "//button[contains(@target-url,'booksellers')]")
	public WebElement learnMoreButton_BookSellers;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Retailer Request']")
	public WebElement retailerReqButton_BookSellers;
	
	@FindBy(how = How.XPATH, using = "//input[@id='firstName']")
	public WebElement fName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='lastName']")
	public WebElement lName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='address1']")
	public WebElement addressLine1;
	
	@FindBy(how = How.XPATH, using = "//input[@id='city']")
	public WebElement city;
	
	@FindBy(how = How.XPATH, using = "//label[@class='control-label']//following-sibling::div/div/div/div[1]")
	public WebElement stateDropdown;
	
	@FindBy(how = How.XPATH, using = "//input[@id='zip']")
	public WebElement zipCode;
	
	@FindBy(how = How.XPATH, using = "//input[@id='phone']")
	public WebElement phoneNumber;
	
	@FindBy(how = How.XPATH, using = "//input[@id='emailAddress']")
	public WebElement emailID;
	
	@FindBy(how = How.XPATH, using = "//input[@id='storeType']")
	public WebElement storeType;
	
	@FindBy(how = How.XPATH, using = "//button[@id='ProfileAddressSubmitButton' and text()='Submit']")
	public WebElement submitButton;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Your retail information request has been submitted.']")
	public WebElement retailInfoRequestSubmittedText;
	
	@FindBy(how = How.XPATH, using = "//p[@class='notice']")
	public WebElement confirmationNotice;
	
	@FindBy(how = How.XPATH, using = "//button[@id='AddEmailSubmitButton' and text()='Close']")
	public WebElement closeButton;
	

	public NortonHelpPage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	@Step("Submit Request Retail Information Form,   Method: {method}")
	public void submitRequestRetailForm(String firstName, String lastName, String addressLine, String cityName, String stateName, int[] phoneIntArray, String zip, String email, String typeOfStore) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//h4[@class='modal-title']")));
		
		fName.sendKeys(firstName);
		lName.sendKeys(lastName);
		addressLine1.sendKeys(addressLine);
		city.sendKeys(cityName);
		
		Thread.sleep(1000);
		Actions act1 = new Actions(driver);
		act1.moveToElement(stateDropdown).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-2-option-0']")));
		for (int i = 0; i <= 58; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-2-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(stateName)) {
				act1.click(drpDownValue).build().perform();
				break;
			}
		}
		
		Thread.sleep(1000);
		zipCode.sendKeys(zip);
		Thread.sleep(1000);
		phoneNumber.click();
		JSONArray phoneArray = new JSONArray(phoneIntArray);
		for(int i=0; i<phoneArray.length(); i++) {
			String PhoneNumber = phoneArray.get(i).toString();
			phoneNumber.sendKeys(PhoneNumber);
		}
		
		emailID.sendKeys(email);
		storeType.sendKeys(typeOfStore);
		submitButton.click();

	}
	
	
	@Step("Verify Confirmation Overlay ,   Method: {method}")
	public boolean verifyConnfirmationOverlay() {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//h4[@class='modal-title' and text()='Confirmation']")));
		
		boolean retailInfoRequestSubmittedExist = false;
		try {
				retailInfoRequestSubmittedExist = retailInfoRequestSubmittedText.isDisplayed();		
				
		} catch (Exception e) {
			e.getMessage();
		}
		
		return retailInfoRequestSubmittedExist;
	}
	
	@Step("Verify Confirmation Notice in Overlay ,   Method: {method}")
	public String confirmationNotice() {	
		String notice = null;
		try {
				notice = confirmationNotice.getText();	
				
		} catch (Exception e) {
			e.getMessage();
		}
		
		return notice;
	}

}
