package com.wwnorton.WebsiteAutomation.objectFactory;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;

public class SocialMediaSites {

	WebDriver driver;
	String parentWindow;

	@FindBy(xpath = "//a[@href='https://www.facebook.com/wwnorton/']")
	WebElement facebookElement;

	@FindBy(xpath = "//a[@href='https://twitter.com/wwnorton']")
	WebElement twitterElement;

	@FindBy(xpath = "//a[@href='https://www.instagram.com/w.w.norton/']")
	WebElement instagramElement;
	

	public SocialMediaSites()

	{
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	public String clickOnFacebook() {
		parentWindow = driver.getWindowHandle();
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(facebookElement));
		facebookElement.click();
		return ReusableMethods.getNewWindowTab(parentWindow);
			
	}


	public String clickOnTwitter() {
		parentWindow = driver.getWindowHandle();
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(twitterElement));
		twitterElement.click();
		return ReusableMethods.getNewWindowTab(parentWindow);

	}

	public String clickOnInstagram() {
		parentWindow = driver.getWindowHandle();
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(instagramElement));
		instagramElement.click();
		return ReusableMethods.getNewWindowTab(parentWindow);

	}

}