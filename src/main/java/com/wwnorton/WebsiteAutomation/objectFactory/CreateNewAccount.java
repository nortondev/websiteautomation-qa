package com.wwnorton.WebsiteAutomation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.gson.JsonObject;
import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;
import com.wwnorton.WebsiteAutomation.utilities.FakerNames;
import com.wwnorton.WebsiteAutomation.utilities.ReadJsonFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class CreateNewAccount {

	WebDriver driver;

	@FindBy(id = "profileLogin")
	public WebElement loginElement;

	@FindBy(id = "creatAccountEmail")
	public WebElement createAccountEmail;

	@FindBy(id = "CreateAccountSubmitButton")
	public WebElement createAccountSubmitButton;

	@FindBy(id = "accountFirstName")
	public WebElement firstName;

	@FindBy(id = "accountLastName")
	public WebElement lastName;

	@FindBy(xpath = "//*[@id='accountEmail']")
	public WebElement accountEmail;

	@FindBy(xpath = "//*[@id='accountReTypeEmail']")
	public WebElement reTypeAccountEmail;

	@FindBy(linkText = "Show Password")
	public WebElement showPasswordLink;

	@FindBy(xpath = "//*[@id='accountPassword']")
	public  WebElement accountPassword;

	@FindBy(xpath = "//*[@id='LoginSubmitButton']")
	public WebElement createAccountFinalButton;
	
	@FindBy(xpath = "//span[@class='headerText emailHeaderText']")
	public WebElement chkYourEmailMsg;
	
	@FindBy(xpath = "//p[@class='notice']")
	public WebElement accounttConfirmationMsg;
	

	@FindBy(id = "profileLogin")
	public WebElement profileloginElement;
	
	@FindBy(xpath = "//a[text()='Help Desk ']")
	public WebElement helpDeskElement;

	@FindBy(xpath = "//a[text()='Log Out']")
	public WebElement logoutElement;

	@FindBy(xpath = "//button[text()='Continue']")
	public WebElement continueButton;
	
	@FindBy(xpath = "//span[@id='CreateAccountEmailId']")
	public WebElement existingEmailError;
	

	public CreateNewAccount() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	public String[] CreateNewUser() {
		ReadJsonFile readJsonObject = new ReadJsonFile();
		JsonObject jsonobject = readJsonObject.readJason();
		String pswd = jsonobject.getAsJsonObject("ExistingAccountCredentials").get("accountpw").getAsString();
		String usersfirstName = FakerNames.getStudentFName();
		String userslastName = FakerNames.getStudentFName();
		String existingAccountEmailID = usersfirstName + userslastName + "@mailinator.cc";
		clickOnLogin();
		createAccountEmail.sendKeys(existingAccountEmailID);
		createAccountSubmitButton.click();
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("accountFirstName")));
		firstName.sendKeys(usersfirstName);
		lastName.sendKeys(userslastName);
		reTypeAccountEmail.sendKeys(existingAccountEmailID);
		accountPassword.sendKeys(pswd);
		//ReusableMethods.scrollIntoView(driver, createAccountEmail);
		createAccountFinalButton.click();
		//createAccountEmail.click();
		continueButton.click();
		String[] credentials = new String[4];
		credentials[0] = usersfirstName;
		credentials[1] = userslastName;
		credentials[2] = existingAccountEmailID;
		credentials[3] = pswd;
		return credentials;
	}
	
	@Step("Click on Login link,   Method: {method}")
	public void clickOnLogin() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		loginElement.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("profileLogin")));

	}

	@Step("Enter New Account Email ID,   Method: {method}")
	public void enterNewAccountEmail(String  NewEmailID) {
		createAccountEmail.sendKeys(NewEmailID);
	}

	@Step("Click on Create New Account button,   Method: {method}")
	public void clickOnCreateAccountButton() {
		ReusableMethods.scrollToElement(driver, By.id("CreateAccountSubmitButton"));
		//createAccountSubmitButton.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", createAccountSubmitButton);
		
	}

	@Step("Enter New Account Name,   Method: {method}")
	public void enterAccountName(String accountFirstName, String accountLastName) {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("accountFirstName")));
		firstName.sendKeys(accountFirstName);
		lastName.sendKeys(accountLastName);

	}

	@Step("Validate Account Email,   Method: {method}")
	public void validateAccountEmail(String  NewEmailID) {
		Assert.assertEquals(NewEmailID, accountEmail.getAttribute("value"));

	}

	@Step("Retype New Account Email ID,   Method: {method}")
	public void retypeAccountEmail(String  NewEmailID) {
		reTypeAccountEmail.sendKeys(NewEmailID);
	}

	@Step("Click on Show Password button,   Method: {method}")
	public void clickOnShowPassword() {
		showPasswordLink.click();

	}

	@Step("Enter Account Password,   Method: {method}")
	public void enterAccountPassword(String pswd) {
		accountPassword.sendKeys(pswd);

	}

	@Step("Click on Submit Account button,   Method: {method}")
	public void clickCreateAccountButton() {
		//createAccountFinalButton.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", createAccountFinalButton);

	}

	
	@Step("Validate Account Confirmation Message,   Method: {method}")
	public String validatAccountConfirmationMessage() {
		
		return accounttConfirmationMsg.getText();

	}
	
	@Step("Click on Account Continue button,   Method: {method}")
	public void clickContinueButton() {
		continueButton.click();

	}
	
	
	@Step("Click on HelpDesk link,   Method: {method}")	
	public void clickHelpDeskLink() {
		helpDeskElement.click();

	}
	
	@Step("Click on LogOut link,   Method: {method}")	
	public void clickLogOut() {
		logoutElement.click();

	}


	@Step("Get Email Already Exist message,   Method: {method}")	
	public String getEmailAlreadyExistMsg() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(existingEmailError));
		return existingEmailError.getText();

	}

}
