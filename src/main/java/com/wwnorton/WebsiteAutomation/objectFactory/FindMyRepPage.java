package com.wwnorton.WebsiteAutomation.objectFactory;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.openqa.selenium.JavascriptExecutor;
import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class FindMyRepPage {

	WebDriver driver;

	@FindBy(how = How.XPATH, using = "//*[text()[contains(.,'Rep')]]")
	List<WebElement> FindMyRepButton;

	@FindBy(how = How.XPATH, using = "//h4[@class='modal-title' and text()='Find My Rep']")
	public WebElement findMyRepOverlay;

	@FindBy(xpath = "//*[@id='refCollegeCityName']/div/div[2]/div[2]")
	WebElement SchoolStateDropdown_College;

	@FindBy(xpath = "//*[@id='refSchoolCityName']/div/div[2]/div[2]")
	WebElement SchoolStateDropdown_Highschool;

	@FindBy(xpath = "//*[@id='txtCollegeName']/div/div[2]/div[2]")
	WebElement SchoolNameDropdown_College;

	@FindBy(xpath = "//*[@id='txtCollegeName']/div/div[2]/div[2]")
	WebElement SchoolNameDropdown_Highschool;

	@FindBy(xpath = "//button[text()='Search']")
	WebElement SearchButton;

	@FindBy(xpath = "//span[contains(text(),'representative')]/b")
	WebElement repNameAtHeader;

	@FindBy(xpath = "//div[@class='repname']")
	WebElement repName;

	@FindBy(xpath = "//div[@class='repcontent'][1]")
	WebElement repPhone;

	@FindBy(xpath = "//div[@class='repcontent'][2]")
	WebElement repEmail;

	@FindBy(xpath = "//*[@id='firstname']")
	WebElement yourFirstName;

	@FindBy(xpath = "//*[@id='lastname']")
	WebElement yourLasttName;

	@FindBy(xpath = "//*[@id='email']")
	WebElement yourEmailAdd;

	@FindBy(xpath = "//*[@id='refSelectedState']")
	WebElement yourSubject;

	@FindBy(xpath = "//*[@id='message']")
	WebElement yourMessage;

	@FindBy(xpath = "//button[@id='SaveButton' and text()='Send']")
	WebElement SendButton;

	@FindBy(xpath = "//h4[@class='modal-title' and text()='Confirmation']")
	public WebElement confirmationPopup;

	@FindBy(xpath = "//span[@class='headerText emailHeaderText' and text()='Email Sent']")
	public WebElement confirmationMsg;

	@FindBy(xpath = "//button[@id='AddEmailSubmitButton' and text()='Close']")
	public WebElement closeButton;

	@FindBy(xpath = "//label[text()=' Canada']")
	WebElement CanadaRadioButton;

	@FindBy(xpath = "//div[@class='css-v73v8k' and text()='Exam Copy Request']")
	WebElement SubjectDropDownOptionCA;

	@FindBy(id = "refSelectedState")
	WebElement SubjectDropDownCA;

	@FindBy(xpath = "//a[text()='Help Desk ']")
	WebElement helpDeskLink;

	@FindBy(xpath = "//button[@class='closeButton']/img")
	WebElement closeButton_FindMyRepOverlay;

	@FindBy(xpath = "//span[@class='TabContentTitle' and text()='High School']")
	WebElement Highschool_section;

	@FindBy(xpath = "//a[text()='International Representative']")
	WebElement link_InternationalRepresentive;

	@FindBy(how = How.XPATH, using = "//span[@class='selectedGCMenue' and text()='International Representatives']")
	public WebElement internationalRepMenu;

	@FindBy(how = How.XPATH, using = "//a[contains(.,'Find Your Rep')]")
	public WebElement findYourRep_Breadcrumb;

	@FindBy(how = How.XPATH, using = "//h1[contains(.,'Find Your Rep')]")
	public WebElement findYourRepHeaderText;
	
	@FindBy(how = How.XPATH, using = "//button[@target-url='booksellers']")
	public WebElement learnMoreButton_BookSellers;
	

	

	public FindMyRepPage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	@Step("Click on Find My Rep button,   Method: {method}")
	public void clickOnFindMyRepButton() throws InterruptedException {
		Thread.sleep(2000);
		FindMyRepButton.get(0).click();
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(findMyRepOverlay));

	}

	@Step("Select school state and school name for USA College,   Method: {method}")
	public void selectSchoolStateUSACollege(String stateNameUSA, String schNameUSA) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		Thread.sleep(2000);
		Actions act1 = new Actions(driver);
		act1.moveToElement(SchoolStateDropdown_College).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-2-option-0']")));
		for (int i = 0; i <= 58; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-2-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(stateNameUSA)) {
				act1.click(drpDownValue).build().perform();
				break;
			}

		}

		Thread.sleep(3000);
		Actions act2 = new Actions(driver);
		act2.moveToElement(SchoolNameDropdown_College).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-3-option-0']")));
		for (int i = 0; i <= 101; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-3-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(schNameUSA)) {
				act2.click(drpDownValue).build().perform();
				break;

			}
		}
	}

	@Step("Click on the search button   Method: {method}")
	public void clickOnSearchButton() throws InterruptedException {
		
		Thread.sleep(2000);
		SearchButton.click();

	}

	@Step("Get the Rep name present at the header,   Method: {method}")
	public String getRepNameAtHeader() {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(repNameAtHeader));

		String repnameatheader = (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].innerText;",
				repNameAtHeader);
		return repnameatheader;

	}

	@Step("Get the Rep name,   Method: {method}")
	public String getRepName() {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(repName));

		String repname = (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].innerText;",
				repName);
		return repname;

	}

	@Step("Get the Rep phone number,   Method: {method}")
	public String getRepPhoneNum() {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(repPhone));

		String repphone = (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].innerText;",
				repPhone);
		return repphone;

	}

	@Step("Get the Rep E-mail ID,   Method: {method}")
	public String getRepEmailId() {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(repEmail));

		String repemail = (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].innerText;",
				repEmail);
		return repemail;

	}

	@Step("Submit your contact details,   Method: {method}")
	public void submitContactDetails(String fName, String lName, String emailAdd, String message)
			throws InterruptedException {

		Thread.sleep(1000);
		yourFirstName.sendKeys(fName);
		Thread.sleep(1000);
		yourLasttName.sendKeys(lName);
		Thread.sleep(1000);
		yourEmailAdd.sendKeys(emailAdd);
		Thread.sleep(1000);
		yourMessage.sendKeys(message);

	}

	@Step("Select the subject option from drop-down,   Method: {method}")
	public void selectSubject(String subName) throws InterruptedException {

		Thread.sleep(2000);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		Actions act3 = new Actions(driver);
		act3.moveToElement(yourSubject).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-6-option-0']")));
		for (int i = 0; i <= 4; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-6-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(subName)) {
				act3.click(drpDownValue).build().perform();
				break;

			}
		}

	}

	@Step("Click on the send button   Method: {method}")
	public void clickOnSendButton() {

		SendButton.click();

	}

	@Step("Check Request Conformation Pop-Up,   Method: {method}")
	public boolean checkRequestConformationPopUpForFindMyRep() throws IOException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(confirmationPopup));

		boolean confirmationMsgExist = false;
		try {
			confirmationMsgExist = confirmationMsg.isDisplayed();
			if (confirmationMsgExist == true) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", closeButton);
			}
		} catch (Exception e) {
			return false;
		}

		return confirmationMsgExist;

	}

	@Step("Click on the Canada radio button,   Method: {method}")
	public void clickOnCanadaRadioButton() {

		CanadaRadioButton.click();

	}

	@Step("Click on HelpDesk link,   Method: {method}")
	public void clickHelpDeskLink_FindMyRep() {
		helpDeskLink.click();

	}

	@Step("Click on HelpDesk link,   Method: {method}")
	public void clickClose_FindMyRepOverlay() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(closeButton_FindMyRepOverlay));
		closeButton_FindMyRepOverlay.click();

	}

	@Step("Click on High School section,   Method: {method}")
	public void clickOnHighSchoolSection() {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(Highschool_section));
		Highschool_section.click();
	}

	@Step("Select school state and school name for USA High School,   Method: {method}")
	public void selectSchoolStateUSAHighschool(String stateNameUSA, String schNameUSA) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		Thread.sleep(1000);
		Actions act1 = new Actions(driver);
		act1.moveToElement(SchoolStateDropdown_Highschool).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-4-option-0']")));
		for (int i = 0; i <= 58; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-4-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(stateNameUSA)) {
				act1.click(drpDownValue).build().perform();
				break;
			}

		}

		Thread.sleep(2000);
		Actions act2 = new Actions(driver);
		act2.moveToElement(SchoolNameDropdown_Highschool).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-5-option-0']")));
		for (int i = 0; i <= 101; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-5-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(schNameUSA)) {
				act2.click(drpDownValue).build().perform();
				break;

			}
		}
	}

	@Step("Click on international representive link,   Method: {method}")
	public void clickOnInternationalRep() {

		link_InternationalRepresentive.click();

	}

	@Step("Get a new window for international representative,  Method: {method}")

	public String openInstructorRepWindow(String parentWindow) throws InterruptedException {

		return ReusableMethods.getNewWindow(parentWindow);

	}

	@Step("Get Instructor Representatives menu  ,  Method: {method} ")

	public String getInternationalRepMenu() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(internationalRepMenu));
		return internationalRepMenu.getText();

	}

	@Step("Click on the Find Your Rep breadcrumb ,  Method: {method} ")

	public void FindYourRepBreadcrumb() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(findYourRep_Breadcrumb));
		findYourRep_Breadcrumb.click();
	}

	@Step("Get Find Your Rep header text ,  Method: {method} ")

	public String getFindYourRepHeaderText() throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(findYourRepHeaderText));
		return findYourRepHeaderText.getText();
	}

	@Step("Select school state and school name for Canada College,   Method: {method}")
	public void selectSchoolStateCACollege(String stateNameCA, String schNameCA) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		Thread.sleep(1000);
		Actions act3 = new Actions(driver);
		act3.moveToElement(SchoolStateDropdown_College).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-2-option-0']")));
		for (int i = 0; i <= 12; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-2-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(stateNameCA)) {
				act3.click(drpDownValue).build().perform();
				break;
			}

		}

		Thread.sleep(2000);
		Actions act4 = new Actions(driver);
		act4.moveToElement(SchoolNameDropdown_College).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-3-option-0']")));
		for (int i = 0; i <= 96; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath("" + "//*[@id='react-select-3-option-" + i + "']"));
			if (drpDownValue.getText().equalsIgnoreCase(schNameCA)) {
				act4.click(drpDownValue).build().perform();
				break;

			}
		}
	}
}