package com.wwnorton.WebsiteAutomation.objectFactory;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

//import com.relevantcodes.extentreports.LogStatus;


public class LoginPage {

	WebDriver driver;

	@FindBy(id = "loginEmail")
	WebElement LoginEmailLink;

	@FindBy(id = "loginPassword")
	WebElement Password;

	@FindBy(id = "LoginSubmitButton")
	WebElement LoginSubmitButton;
	
	@FindBy(linkText = "Forgot your password?")
	WebElement forgotpassword;

	@FindBy(id = "accountEmail")
	WebElement accountEmailForgotPassword;

	@FindBy(className = "SubmitButton")
	WebElement forgotPasswordSubmitButton;
	
	@FindBy(id = "profileLogin")
	WebElement profileLogin;
	
	@FindBy(xpath = "//a[text()='Log Out']")
	public WebElement logoutElement;
	
	@FindBy(xpath = "//a[text()='Profile']")
	public WebElement profileElement;
	
	@FindBy(xpath = "//a[text()='Digital Products']")
	public WebElement digitalProductsElement;
	
	@FindBy(xpath = "//a[text()='Orders']")
	public WebElement ordersElement;
	
	@FindBy(xpath = "//span[@id='profileLogin' and text()='ACCOUNT']")
	public WebElement accountElement;
	
	@FindBy(how = How.XPATH, using = "//button[@aria-label='close']/img")
	public WebElement loginClose;
	

	public LoginPage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}
	
	@Step("Login with Existing Email ID and Password,   Method: {method}")
	public void loginWithExistingAccount(String EmailID, String password) throws IOException {
		LoginEmailLink.sendKeys(EmailID);
		Password.sendKeys(password);
		LoginSubmitButton.click();

	}
	
	@Step("Click on LogOut link,   Method: {method}")	
	public boolean checkLogOutExist() {
		
		boolean logoutElementExist = false;
		
		try {
				logoutElementExist = logoutElement.isDisplayed();
				if (logoutElementExist) {
					logoutElement.click();
				}
			
		} catch (Exception e) {
			return false;
			
		}
		
		return true;

	}
	
	
	public void clickOnForgotYourPassword(String emailID) throws IOException, InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(forgotpassword));
		forgotpassword.click();
		Thread.sleep(2000);
		accountEmailForgotPassword.sendKeys(emailID);
		forgotPasswordSubmitButton.click();
		
	}
	
	public void resetPasswordInMailinator(String emailID) throws IOException, InterruptedException {
		Thread.sleep(3000);
		driver.get("https://www.mailinator.com/v3/#/#inboxpane");
		String forgotPasswordEmailID = emailID;
		String updatedForgotPasswordEmailID = forgotPasswordEmailID.replace("@", "_");
		Thread.sleep(3000);
		driver.findElement(By.id("inbox_field")).sendKeys(updatedForgotPasswordEmailID);
		driver.findElement(By.id("go_inbox")).click();
		driver.findElement(By.linkText("Reset Password for W. W. Norton Account")).click();
		driver.switchTo().frame(0);
		driver.findElement(By.linkText("wwnorton.com/resetpassword")).click();
		Thread.sleep(5000);
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		
	}
	
	public void loginWithRResetPassword(String emailID, String password) throws IOException, InterruptedException {

		driver.findElement(By.id("accountPassword")).sendKeys(password);
		driver.findElement(By.id("accountRetypePassword")).sendKeys(password);
		driver.findElement(By.id("LoginSubmitButton")).click();
		driver.findElement(By.id("loginEmail")).sendKeys(emailID);
		driver.findElement(By.id("loginPassword")).sendKeys(password);
		driver.findElement(By.id("LoginSubmitButton")).click();
		Thread.sleep(1000);
		profileLogin.click();
	}
	
	@Step("Check Profile link,   Method: {method}")	
	public boolean checkProfileLinkExist() {
		
		boolean profileElementExist = false;
		
		try {
				profileElementExist = profileElement.isDisplayed();
			
		} catch (Exception e) {
			return false;
			
		}
		
		return profileElementExist;

	}
	
	@Step("Click on Profile link,   Method: {method}")
	public void clickProfileLink() throws IOException {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(profileElement));
		//profileElement.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", profileElement);

	}
	
	@Step("Check Digital Products link,   Method: {method}")	
	public boolean checkDigitalProductsLinkExist() {
		
		boolean digitalProductseExist = false;
		
		try {
				digitalProductseExist = digitalProductsElement.isDisplayed();
			
		} catch (Exception e) {
			return false;
			
		}
		
		return digitalProductseExist;

	}
	
	@Step("Click on Digital Products link,   Method: {method}")
	public void clickDigitalProductsLink() throws IOException {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(digitalProductsElement));
		//digitalProductsElement.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", digitalProductsElement);

	}
	
	
	@Step("Check Orders link,   Method: {method}")	
	public boolean checkOrdersLinkExist() {
		
		boolean ordersElementExist = false;
		
		try {
				ordersElementExist = ordersElement.isDisplayed();
			
		} catch (Exception e) {
			return false;
			
		}
		
		return ordersElementExist;

	}
	
	@Step("Click on Orders link,   Method: {method}")
	public void clickOrdersLink() throws IOException {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(ordersElement));
		//ordersElement.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", ordersElement);

	}
	
	@Step("Click on Account link,   Method: {method}")
	public void clickAccountLink() throws IOException {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(accountElement));
		//accountElement.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", accountElement);

	}
	
	@Step("Click on Account Close image,   Method: {method}")
	public void closeAccountLoginImg() throws IOException {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(loginClose));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", loginClose);

	}
	
	
}
