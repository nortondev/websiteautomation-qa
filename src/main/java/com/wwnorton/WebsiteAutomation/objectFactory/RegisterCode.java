package com.wwnorton.WebsiteAutomation.objectFactory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class RegisterCode {
	
WebDriver driver;

	
	//@FindBy(how = How.XPATH, using = "//button[text()='Register Code']")
	//public WebElement RegisterCodeButton;

	@FindBy(how = How.XPATH, using = "//div[@data-module='button']/button")
	public WebElement RegisterCodeButton;
	
	@FindBy(how = How.XPATH, using = "//h2[@class='text-center float-left ModalWizardHeader']")
	public WebElement floatingContainer;
	
	@FindBy(how = How.XPATH, using = "//div[@class='SchoolForm']/div/label[(text()='Country')]/following-sibling::div")
	public WebElement countryDrpDown;
	
	@FindBy(how = How.XPATH, using = "//div[@class='SchoolForm']/div/label[contains(text(),'State')]/following-sibling::div")
	public WebElement stateDrpDown;
	
	@FindBy(how = How.XPATH, using = "//div[@class='SchoolForm']/div/label[contains(text(),'Name')]/following-sibling::div")
	public WebElement schoolDrpDown;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Continue']")
	public WebElement continueButton;
	
	@FindBy(how = How.XPATH, using = "//input[@id='txtRegisterCode']")
	public WebElement accessCode;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'react-select-')]")
	public WebElement selectProduct;
	
	@FindBy(how = How.XPATH, using = "//div[@id='buttonContainer']/div/button[text()='Register Code']")
	public WebElement registerAccessCodeButton;
	
	@FindBy(how = How.XPATH, using = "//span[@class='headerText emailHeaderText' and text()='Successfully Added To Your Account']")
	public WebElement registerConfirmMsg;	
	
	
	public RegisterCode() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}
	

	@Step("Click Register Code Button ,   Method: {method}")
	public void clickRegisterCode() throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@data-module='button']/button")));
		RegisterCodeButton.click();
	}
	
	@Step("Click Register Code Button ,   Method: {method}")
	public void enterStudentInfo() throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//h2[@class='text-center float-left ModalWizardHeader']")));
		countryDrpDown.click();
		Thread.sleep(2000);
		WebElement countryOption;
		String country;
		for(int i=0; i<3; i++)
		{
			countryOption = driver.findElement(By.xpath("//div[contains(@id,'option-" + i + "')]"));
			country = countryOption.getText();
			if(country.equalsIgnoreCase("Canada"))
			{
				countryOption.click();
				break;
			}
		}
		
		stateDrpDown.click();
		Thread.sleep(2000);
		WebElement stateOption;
		String state;
		for(int i=0; i<4; i++)
		{
			stateOption = driver.findElement(By.xpath("//div[contains(@id,'option-" + i + "')]"));
			state = stateOption.getText();
			if(state.equals("British Columbia"))
			{
				stateOption.click();
				break;
			}
		}
		
		schoolDrpDown.click();
		Thread.sleep(2000);
		WebElement schoolOption;
		String school;
		for(int i=0; i<69; i++)
		{
			schoolOption = driver.findElement(By.xpath("//div[contains(@id,'option-" + i + "')]"));
			school = schoolOption.getText();
			if(school.equals("Alexander College, Vancouver"))
			{
				schoolOption.click();
				break;
			}
		}
	}
	
	@Step("Click ContinueButton ,   Method: {method}")
	public void clickContinueButton() throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(
		//		By.xpath("//button[text()='Register Code']")));
		continueButton.click();
	}
	
	@Step("Register Acceess Code and Register,   Method: {method}")
	public void registerAcceessCode(String regCode) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//h2[text()='Register A Code']")));
		accessCode.sendKeys(regCode);
		Thread.sleep(3000);
		selectProduct.click();
		Thread.sleep(2000);
		WebElement productOption;
			productOption = driver.findElement(By.xpath("//div[contains(@id,'option-0')]"));
			productOption.click();
		Thread.sleep(5000);
		registerAccessCodeButton.click();
	}

	@Step("Check Registration Confirmation message ,   Method: {method}")
	public boolean checkConfirmMsg() throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(registerConfirmMsg));
		try {
				if(registerConfirmMsg.isDisplayed() == true) {
					//dont do anything
				}
			} catch (Exception e) {
					e.getMessage();
					return false;
				}
		
		return true;
	}
	
}

