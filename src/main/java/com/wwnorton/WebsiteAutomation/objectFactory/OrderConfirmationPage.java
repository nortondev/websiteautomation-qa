package com.wwnorton.WebsiteAutomation.objectFactory;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class OrderConfirmationPage {
	
	WebDriver driver;
	
	//Get Order Details from the Order Confirmation page.
	
	@FindBy(how = How.XPATH, using = "//h1[@class='OrderConfirm']")
	WebElement orderConfirm;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'OrderDetailHeader')]/span")
	WebElement orderNumber;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"__next\"]/div/div/div/div/div/div[2]/div/div[2]/div[2]/div[9]")
	WebElement orderShippingAddress;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"__next\"]/div/div/div/div/div/div[2]/div/div[2]/div[2]/div[9]/div[2]")
	WebElement shippingAddressLine1;
	
	@FindBy(how = How.XPATH, using = "//a[text()='LOG OUT']")
	public WebElement logOutLink;
	
	
	// Initializing Web Driver and PageFactory.
	
	public OrderConfirmationPage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);	
	}
	
	@Step("Get Order Details,  Method: {method} ")
	public String orderDetails() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		ReusableMethods.scrollToTop(driver);
		wait.until(ExpectedConditions.visibilityOf(orderConfirm));
		String orderConfirmation = orderConfirm.getText();
		Assert.assertEquals(orderConfirmation, "Your Order Has Been Placed");

		return orderNumber.getText();
	}
	
	
	@Step("Order Shipping Address Exist,  Method: {method} ")
	public boolean orderShippingAddress() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOf(orderShippingAddress));
		boolean ShippingAddress = orderShippingAddress.isDisplayed();
		
		return ShippingAddress;
	}
	
	
	@Step("Get Shipping Address Line 1,  Method: {method} ")
	public String getShippingAddress() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOf(shippingAddressLine1));
		String ShippingAddressLine1 = shippingAddressLine1.getText();
		
		return ShippingAddressLine1;
	}
	
	// --> Added By Surabhi 
	
	@Step("Get Logged-out from order placed page ,  Method: {method} ")
	public void clickOnLogOut() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOf(logOutLink));
		
		logOutLink.click();
	}
}
