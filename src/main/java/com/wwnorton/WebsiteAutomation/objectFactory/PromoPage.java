package com.wwnorton.WebsiteAutomation.objectFactory;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class PromoPage {

	WebDriver driver;

	

	@FindBy(how = How.XPATH, using = "//a[@id='show_banner_details']")
	public WebElement detailsPromoPageHeaderBanner;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Coupon Code ']")
	public WebElement promoCodeDetails;

	@FindBy(how = How.XPATH, using = "//button[@class='closeButton']")
	public WebElement closeButtonForPromotionDetailsOverlay;
	
	@FindBy(how = How.XPATH, using = "//span[@class='itemLabel' and text()='College']")
	public WebElement collegeCategory;
	
	@FindBy(how = How.XPATH, using = "//a[text()='All Results']")
	public WebElement allResultsBreadCrumb;
	
	@FindBy(how = How.XPATH, using = "//span[@class='itemLabel' and text()='Nonfiction']")
	public WebElement nonFictionCategory;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Add to cart']")
	public WebElement addToCartFromPromoPage;
	
	@FindBy(how = How.XPATH, using = "//span[@class='promoCheckMarkText' and text()='Promo Applied']")
	WebElement promoCodePromoAppliedText;
	
	@FindBy(how = How.XPATH, using = "//button[@class='closeButton']")
	public WebElement removePromoCode;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Enter Promo Code']")
	public WebElement enterPromoCode;
	
	@FindBy(how = How.XPATH, using = "//button[@id='buttonsend']")
	public WebElement applyPromoCode;


	public PromoPage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	@Step("Click on Details link present in promo page at header banner,   Method: {method}")
	public void QAclickOnDetailsLinkAtHeaderPromoPage() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(detailsPromoPageHeaderBanner));
		detailsPromoPageHeaderBanner.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[@class='modal-title' and text()='Promotion Details']")));

	}
	
	@Step("Get promo code details in Promotion Details overlay,   Method: {method}")
	public String getPromoCodeDetails() throws IOException {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(promoCodeDetails));
		return promoCodeDetails.getText();

	}

	@Step("Close the promotion details overlay,   Method: {method}")
	public void closePromotionDetailsOverlay() throws IOException {
		
		closeButtonForPromotionDetailsOverlay.click();
	}
	
	
	@Step("Click on All Results breadcrumb,   Method: {method}")
	public void allResultsBreadCrumb() throws IOException {
		
		 allResultsBreadCrumb.click();
	}
	
	@Step("Add book into the cart from Promo page,   Method: {method}")
	public void addBookInToCart() throws IOException {
		
		addToCartFromPromoPage.click();
	}
	
	@Step("Remove promo code from the check-out page,   Method: {method}")
	public void removePromoCode() throws IOException {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(removePromoCode));
		
		removePromoCode.click();
	}
	
	@Step("Provide the promo code in Enter Promo Code text-box,   Method: {method}")
	public void providePromoCode(String promocode) throws InterruptedException {

		enterPromoCode.sendKeys(promocode);

}
	
	@Step("Re-apply the promo code,   Method: {method}")
	public void applyPromoCode() throws IOException {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(applyPromoCode));
		
		applyPromoCode.click();
	}
}
