package com.wwnorton.WebsiteAutomation.objectFactory;

import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class CartPayment {
	
	WebDriver driver;
	
	
	// Finding Web Elements on Book Details page with respect to Add to Cart using PageFactory.
	
	
	@FindBy(how = How.CSS, using = "#name")
	WebElement cardHolderName;		
	
	@FindBy(how = How.CSS, using = "#cardNumber")
	WebElement creditCardNumber;
	
	@FindBy(how = How.XPATH, using = "//label[(text()='Expiration Month')]/following-sibling::div/div/button/span")
	WebElement expMonthButton;
	
	@FindBy(how = How.XPATH, using = "//div[@id='yearExpiry']//span[contains(@class,'caret')]")
	WebElement expYearButton;
	
	@FindBy(how = How.XPATH, using = "//input[@id='securityCode']")
	WebElement securityCode;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"paymentPanel--body\"]/div/div/div/form/div/div[1]/span")
	WebElement errCCName;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"cardNumberField\"]/div/span")
	WebElement errCCNumber;
	
	@FindBy(how = How.XPATH, using = "//body/div[@id='__next']/div/div[contains(@class,'container logindash dashboardDesktop checkOutContainer')]/div/div/div[contains(@class,'checkoutBody')]/div[contains(@class,'container')]/div[contains(@class,'row')]/div[contains(@class,'cartPanelLayout col-md-7 col-sm-6 col-xs-12')]/div/div[@id='paymentPanel']/div[@id='paymentPanel--body']/div[contains(@class,'panel-body')]/div/div[contains(@class,'textCenter col-md-12 col-sm-12 col-xs-12')]/form/div/div[3]/div[1]/div[1]/div[1]/span[1]")
	WebElement errExpMonth;
	
	@FindBy(how = How.XPATH, using = "//div[@id='yearExpiry']//span[contains(@class,'errorMessage help-block')][contains(text(),'This field is required.')]")
	WebElement errExpYear;
	
	@FindBy(how = How.XPATH, using = "//div[@id='securtiyCodeContainer']//span[contains(@class,'errorMessage help-block')][contains(text(),'This field is required.')]")
	WebElement errSecurityCode;
	
	@FindBy(how = How.XPATH, using = "//input[@id='accountFirstName']")
	WebElement billingFirstName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='accountLastName']")
	WebElement billingLastName;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"address\"]")
	WebElement billingAddress;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"city\"]")
	WebElement billingCity;
	
	@FindBy(how = How.XPATH, using = "//div[@id='drpState']/label[text()='State']/following-sibling::div/div")
	WebElement billingStateButton;
	
	@FindBy(how = How.XPATH, using = "//li[@id='rbt-menu-item-1']")
	WebElement billingState;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"zipCode\"]")
	WebElement billingZip;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"phone\"]")
	WebElement billingPhone;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"paymentPanel--body\"]/div/div/div/form/div/div[5]/div[2]/span")
	WebElement errBillingfName;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"paymentPanel--body\"]/div/div/div/form/div/div[5]/div[3]/span")
	WebElement errBillinglName;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"paymentPanel--body\"]/div/div/div/form/div/div[5]/div[4]/span")
	WebElement errBillingAddress;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"paymentPanel--body\"]/div/div/div/form/div/div[5]/div[6]/span")
	WebElement errBillingCity;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"drpState\"]/div/span")
	WebElement errBillingState;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"zipCodeParent\"]/div/span")
	WebElement errBillingZip;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"phoneNumberField\"]/div/span")
	WebElement errBillingPhone;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'nopadding addressCheckbox')]/div/input")
	WebElement billingAddressCheckbox;
	
	@FindBy(how = How.XPATH, using = "//div[@id='agreeTermsField']/input")
	WebElement tncCheckbox;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"agreeTermsField\"]/span[2]")
	WebElement tncError;
	
	@FindBy(how = How.XPATH, using = "//button[@id='ProfileAddressSubmitButton']")
	WebElement submitOrder;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"zipCodeParent\"]/div/span")
	WebElement billingZipError;
	
	
	// Initializing Web Driver and PageFactory.
	
	public CartPayment() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}


	// Enter Payment information in Checkout page.
	
	
	@Step("Enter Payment Information,  Method: {method} ")
	public void enterPaymentInfo(String cHName, String expMonth, String expYear, String secCode) throws Exception {
		ReusableMethods.scrollIntoView(driver, cardHolderName); 
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(cardHolderName));
		cardHolderName.sendKeys(cHName);
        creditCardNumber.click();
		int[] ccIntArray = {4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
		JSONArray ccArray = new JSONArray(ccIntArray);
		
		for(int i=0; i<ccArray.length(); i++) {
            String ccNumber = ccArray.get(i).toString();
            creditCardNumber.sendKeys(ccNumber);
		}
		
		wait.until(ExpectedConditions.visibilityOf(expMonthButton));
		expMonthButton.click();
		Thread.sleep(2000);
		WebElement monthOption;
		String month;
		
		String monthArray[] = {"January" , "February" ,"March" ,"April" ,"May" ,"June" ,"July" ,"August" ,"September" ,"October" ,"November" ,"December"}; 
		
		for(int i=0; i<monthArray.length; i++) {
			monthOption = driver.findElement(By.xpath("//a[contains(text()," + "'" + monthArray[i] + "')]"));
			month = monthOption.getText();
			
			if(month.equals(expMonth))
			{
				monthOption.click();
				break;
			}
		}

		wait.until(ExpectedConditions.visibilityOf(expYearButton));
		expYearButton.click();
	
		WebElement yearOption ;
		String year;
		
		for(int i=1; i<7; i++) {
			yearOption = driver.findElement(By.xpath("//*[@id='yearExpiry']//li[" + i + "]"));
			year = yearOption.getText();
			if(year.equals(expYear))
			{
				yearOption.click();
				break;
			}
		}
		
		securityCode.sendKeys(secCode);
		
	}
	
	
	@Step("Enter Billing Information,  Method: {method} ")
	public void enterBillingInfo(String fName, String lName, String prmAdd, String City, String State, String Zip) throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		TimeUnit.SECONDS.sleep(25);	
	
		wait.until(ExpectedConditions.visibilityOf(billingFirstName));
		billingFirstName.sendKeys(fName);
		Thread.sleep(1000);
		billingLastName.sendKeys(lName);
		Thread.sleep(1000);
		billingAddress.sendKeys(prmAdd);
		Thread.sleep(1000);
		billingCity.sendKeys(City);
		Thread.sleep(2000);
		billingStateButton.click();
		Thread.sleep(2000);
		WebElement stateOption;
		String stateName;
		for(int i=0; i<51; i++)
		{
			stateOption = driver.findElement(By.xpath("//*[contains(@id, '"+ i +"')]"));
			stateName = stateOption.getText();
			if(stateName.equals(State))
			{
				stateOption.click();
				break;
			}
		}						
		Thread.sleep(2000);
		int zipAsInt = Integer.parseInt(Zip);
		billingZip.sendKeys(String.valueOf(zipAsInt));
		Thread.sleep(2000);
		billingPhone.click();
		int[] phoneIntArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
		JSONArray phoneArray = new JSONArray(phoneIntArray);

			for(int i=0; i<phoneArray.length(); i++) {
				String PhoneNumber = phoneArray.get(i).toString();
				billingPhone.sendKeys(PhoneNumber);
			}
		}
	
	
	@Step("Verify the error message for Payment information,  Method: {method} ")
	public String[] paymentErr() throws Exception {
		
		String[] PaymentErrArray = new String[5];
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(errCCName));
		PaymentErrArray[0] = errCCName.getText();
		
		wait.until(ExpectedConditions.visibilityOf(errCCNumber));
		PaymentErrArray[1] = errCCNumber.getText();
		
		wait.until(ExpectedConditions.visibilityOf(errExpMonth));
		PaymentErrArray[2] = errExpMonth.getText();
		
		wait.until(ExpectedConditions.visibilityOf(errExpYear));
		PaymentErrArray[3] = errExpYear.getText();
		
		wait.until(ExpectedConditions.visibilityOf(errSecurityCode));
		PaymentErrArray[4] = errSecurityCode.getText();
		
		return PaymentErrArray;
		
	}
	
	
	@Step("Verify the error message for Billing information,  Method: {method} ")
	public String[] billingErr() throws Exception {
		
		String[] BillingErrArray = new String[7];
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(errBillingfName));
		BillingErrArray[0] = errBillingfName.getText();
		
		wait.until(ExpectedConditions.visibilityOf(errBillinglName));
		BillingErrArray[1] = errBillinglName.getText();
		
		wait.until(ExpectedConditions.visibilityOf(errBillingAddress));
		BillingErrArray[2] = errBillingAddress.getText();
		
		wait.until(ExpectedConditions.visibilityOf(errBillingCity));
		BillingErrArray[3] = errBillingCity.getText();
		
		wait.until(ExpectedConditions.visibilityOf(errBillingState));
		BillingErrArray[4] = errBillingState.getText();
		
		wait.until(ExpectedConditions.visibilityOf(errBillingZip));
		BillingErrArray[5] = errBillingZip.getText();
		
		wait.until(ExpectedConditions.visibilityOf(errBillingPhone));
		BillingErrArray[6] = errBillingPhone.getText();
		
		return BillingErrArray;
		
		
	}
	
	
	@Step("Select Billing Information,  Method: {method} ")
	public void checkBillingAddress() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOf(billingAddressCheckbox));
		billingAddressCheckbox.click();
		
	}
	
	
	@Step("Accept Terms and Conditions,  Method: {method} ")
	public void checkTnC() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf(tncCheckbox));
		//tncCheckbox.click();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", tncCheckbox);
	}
	
	
	@Step("Verify error for Accept Terms and Conditions,  Method: {method} ")
	public String errorTnC() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(tncError));
		return tncError.getText();
		
	}
	
	
	@Step("Place an Order,  Method: {method} ")
	public void placeOrder() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,500);
		wait.until(ExpectedConditions.visibilityOf(submitOrder));
		submitOrder.click();
		
	}
	
	
	@Step("Remove Billing ZipCode,  Method: {method} ")
	public void removeBillingZipCode() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(billingZip));
		String zipLen = billingZip.getAttribute("value");
		int lenText = zipLen.length();

		for(int i = 0; i < lenText; i++){
			billingZip.sendKeys(Keys.ARROW_LEFT);
			billingZip.sendKeys(Keys.DELETE);
			
			}
		}
	
	
	@Step("Re-enter Billing ZipCode,  Method: {method} ")	
	public void enterBillingZipCode() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(billingZip));
		String zip = "08817";
		int zipAsInt = Integer.parseInt(zip);
		billingZip.sendKeys(String.valueOf(zipAsInt));
		
	}
	
	
	@Step("Re-enter Billing ZipCode,  Method: {method} ")	
	public String billingZipCodeErr() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(billingZipError));
		return billingZipError.getText();
		
	}

}

