package com.wwnorton.WebsiteAutomation.objectFactory;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;


public class APWorkshopPage {
	
WebDriver driver;
	
	@FindBy(xpath = "//div[@data-module ='button']/button")
	public WebElement apWorkShopButton;
	
	@FindBy(xpath = "//h4[@class='modal-title' and text()='Your Contact Information']")
	public WebElement contactInformationFormTitle;
	
	@FindBy(xpath = "//input[@id='name']")
	public WebElement fullNameElement;
	
	@FindBy(xpath = "//input[@id='schoolName']")
	public WebElement schoolNameElement;
	
	@FindBy(xpath = "//input[@id='schoolCity']")
	public WebElement schoolCityElement;
	
	@FindBy(xpath = "//div[contains(@class,'selectDropdown')]")
	public WebElement schoolStateDrpDownList;
	
	@FindBy(xpath = "//input[@id='titleAtSchool']")
	public WebElement titleAtschoolElement;
	
	@FindBy(xpath = "//input[@id='schoolEmail']")
	public WebElement emailIDEelement;
	
	@FindBy(xpath = "//button[@id='ProfileAddressSubmitButton']")
	public WebElement nextButton;
	
	@FindBy(xpath = "//h4[@class='modal-title' and text()='Workshop or Institute Information']")
	public WebElement workshopInformationFormTitle;
	
	@FindBy(xpath = "//div[text()='Please select a subject...']")
	public WebElement subjectDrpDownList;
	
	@FindBy(xpath = "//input[@id='workshopInstitute']")
	public WebElement instNameElement;
	
	@FindBy(xpath = "//input[@id='city']")
	public WebElement instCityNameElement;
	
	@FindBy(xpath = "//div[@id='drpState']/div/div[contains(@class,'selectDropdown')]")
	public WebElement instStateDrpDownList;
	
	@FindBy(xpath = "//img[@class='date-picker-icon']")
	public WebElement workshopDatePicker;
	
	@FindBy(xpath = "//input[@id='enrollment']")
	public WebElement enrollmentElement;

	@FindBy(xpath = "//input[@id='coordinatorName']")
	public WebElement coordinatorElement;
	
	@FindBy(xpath = "//input[@id='instituteCoordinatorEmail']")
	public WebElement instituteCoordinatorElement;
	
	@FindBy(xpath = "//textarea[@id='comments']")
	public WebElement commentsElement;
	
	@FindBy(xpath = "//button[@id='ProfileAddressSubmitButton' and text()='Submit']")
	public WebElement submitButton;
	
	@FindBy(xpath = "//h4[@class='modal-title' and text()='Confirmation']")
	public WebElement confirmationPopup;
	
	@FindBy(xpath = "//span[@class='headerText emailHeaderText' and text()='Your request has been delivered.']")
	public WebElement confirmationMsg;
	
	@FindBy(xpath = "//button[@id='AddEmailSubmitButton' and text()='Close']")
	public WebElement closeButton;
	
	
	
	public APWorkshopPage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}
	
	
	@Step("Click on AP WorkShop Button,   Method: {method}")
	public void clickAPWorkShopButton() throws IOException {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(apWorkShopButton));
		apWorkShopButton.click();

	}
	
	@Step("Submit AP Contact Information Form,   Method: {method}")
	public void submitAPContactInformationForm(String fName, String sName, String sCityName, 
			String stateName, String titleName, String emailid) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(contactInformationFormTitle));
		
		fullNameElement.sendKeys(fName);
		schoolNameElement.sendKeys(sName);
		schoolCityElement.sendKeys(sCityName);
		Thread.sleep(1000);
		Actions act = new Actions(driver);
		act.moveToElement(schoolStateDrpDownList).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-2-option-0']")));
		for(int i=0; i<=58; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath(""
					+ "//*[@id='react-select-2-option-" + i + "']"));
			if(drpDownValue.getText().equalsIgnoreCase(stateName)) {
				act.click(drpDownValue).build().perform();
				break;
			}
		}
		
		titleAtschoolElement.sendKeys(titleName);
		emailIDEelement.sendKeys(emailid);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", nextButton);

	}
	
	
	@Step("Submit AP Contact Information Form,   Method: {method}")
	public void submitWorkshopInformationForm(String subName, String instName, String instCityName, 
			String stateName, String currnetDay, String enrollmentValue, String coordinatorName, String emailid) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(workshopInformationFormTitle));
		
		Thread.sleep(1000);
		Actions act = new Actions(driver);
		act.moveToElement(schoolStateDrpDownList).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-3-option-0']")));
		for(int i=0; i<=58; i++) {
			WebElement drpDownValue = driver.findElement(By.xpath(""
					+ "//*[@id='react-select-3-option-" + i + "']"));                                                   
			if(drpDownValue.getText().equalsIgnoreCase(subName)) {
				act.click(drpDownValue).build().perform();
				break;
			}
		}
		
		instNameElement.sendKeys(instName);
		instCityNameElement.sendKeys(instCityName);
		
		Thread.sleep(1000);
		act.moveToElement(instStateDrpDownList).click().build().perform();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='react-select-4-option-0']")));
		for(int i=0; i<=58; i++) {
			Thread.sleep(1000);
			WebElement drpDownValue = driver.findElement(By.xpath(""
					+ "//*[@id='react-select-4-option-" + i + "']"));                                                   
			if(drpDownValue.getText().equalsIgnoreCase(stateName)) {
				act.click(drpDownValue).build().perform();
				break;
			}
		}
		
		workshopDatePicker.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='react-datepicker__month']")));
		WebElement calendarDate = driver.findElement(By.xpath(""
			+ "//div[@class='react-datepicker__month']/div/div[contains(@class,'today')]"));
			if(calendarDate.getText().equalsIgnoreCase(currnetDay)) {
				((JavascriptExecutor) driver).executeScript("arguments[0].click();", calendarDate);
			}

		enrollmentElement.sendKeys(enrollmentValue);
		coordinatorElement.sendKeys(coordinatorName);
		instituteCoordinatorElement.sendKeys(emailid);
		commentsElement.sendKeys("Test, please disregard.");
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", submitButton);

	}
	
	@Step("Check Request Conformation Pop-Up,   Method: {method}")
	public boolean checkRequestConformationPopUp() throws IOException {
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(confirmationPopup));
		
		boolean confirmationMsgExist = false;
		try {
				confirmationMsgExist = confirmationMsg.isDisplayed();
				if (confirmationMsgExist == true) {
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", closeButton);
				}
		} catch (Exception e) {
			return false;
		}
		
		return confirmationMsgExist;
	
	}
	
}
