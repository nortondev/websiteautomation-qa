package com.wwnorton.WebsiteAutomation.objectFactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class HomePage {

	WebDriver driver;

	@FindBy(linkText = "READER")
	public WebElement readerElement;

	@FindBy(how = How.XPATH, using = "//h1[contains(.,'Reader')]")
	public WebElement readerText;

	@FindBy(linkText = "STUDENT")
	public WebElement studentElement;

	@FindBy(how = How.XPATH, using = "//h1[contains(.,'Student')]") 
	public WebElement studentText;

	@FindBy(linkText = "EDUCATOR")
	public WebElement educatorElement;

	@FindBy(how = How.XPATH, using = "//h1[contains(.,'Educator')]")
	public WebElement educatorText;

	@FindBy(linkText = "WHO WE ARE")
	public WebElement whoweareElement;

	@FindBy(how = How.XPATH, using = "//h1[contains(.,'Who We Are')]")
	public WebElement whoWWeAreText;

	@FindBy(linkText = "CAREERS")
	public WebElement careersElement;

	@FindBy(how = How.XPATH, using = "//h1[contains(.,'Careers')]")
	public WebElement careersText;

	@FindBy(linkText = "HELP")
	public WebElement helpElement;

	@FindBy(how = How.XPATH, using = "//h1[contains(.,'W. W. Norton Help')]")
	public WebElement helpText;

	@FindBy(linkText = "ACCESSIBILITY")
	public WebElement accessibilityElement;

	@FindBy(how = How.XPATH, using = "//h1[contains(.,'Accessibility')]") 
	public WebElement accessibilityText;

	@FindBy(linkText = "PRIVACY POLICY")
	public WebElement privacypolicyElement;

	@FindBy(how = How.XPATH, using = "//h1[contains(.,'Privacy Policy')]") 
	public WebElement privacyPolicyText;

	@FindBy(linkText = "TERMS OF USE")
	public WebElement termsofuseElement;

	@FindBy(how = How.XPATH, using = "//h1[contains(.,'Terms of Use')]") 
	public WebElement termsOfUseText;

	@FindBy(linkText = "CONTACT US")
	public WebElement contactusElement;

	@FindBy(how = How.XPATH, using = "//h1[contains(.,'Contact')]")
	public WebElement contactUsText;

	@FindBy(how = How.XPATH, using = "//div[@id='TopCenterImage']/a")
	public WebElement seagullicon;

	@FindBy(how = How.XPATH, using = "//div[@class='iconlogo']/a")
	public WebElement seagulliconCheckOut;

	@FindBy(how = How.XPATH, using = "//div[@class='headerDefault ']/p[text()='W • W • NORTON & COMPANY INDEPENDENT & EMPLOYEE-OWNED']")
	public WebElement seagullIconText;

	@FindBy(how = How.XPATH, using = "//input[@name='email']")
	public WebElement newsletter;

	@FindBy(id = "newsletterSend")
	public WebElement sendnewsletter;

	@FindBy(className = "successLabel")
	public WebElement successLabel;

	@FindBy(className = "errorLabel")
	public WebElement errormessageforincorrectEmail;

	@FindBy(how = How.XPATH, using = "//input[@id='globalInput']")
	public WebElement searchTextBox;

	@FindBy(how = How.XPATH, using = "//button[@class='searchButton']")
	public WebElement searchButton;

	@FindBy(how = How.XPATH, using = "//button[@target-url='/catalog' and text()='Shop Books']")
	public WebElement shopBookButton;

	@FindBy(how = How.XPATH, using = "//button[text()='Shop Books']")
	public WebElement shopBooksButton;

	@FindBy(how = How.XPATH, using = "//button[text()='Shop Textbooks']")
	public WebElement shopTextBooksButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='withoutBackground']")
	public List<WebElement> shopBooks_BrowseButtonList;
	
	@FindBy(how = How.XPATH, using = "//button[@class='withoutBackground']/span[text()='Explore']")
	public List<WebElement> shopBooks_ExploreButtonList;
	
	@FindBy(how = How.XPATH, using = "//div[@data-module='button']/button[contains(.,'Browse')]")
	public WebElement youngReader_BrowseButton;
	
	@FindBy(how = How.XPATH, using = "//div[@data-module='button']/button")
	public List <WebElement> shopBooks_Reader;
	
	@FindBy(how = How.XPATH, using = "//div[@data-module='button']/button[text()='Browse Textbooks']")
	public WebElement browseTextBooks;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Fiction']")
	public WebElement fictionLabel;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Nonfiction']")
	public WebElement nonfictionLabel;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Poetry']")
	public WebElement poetryLabel;
	
	@FindBy(how = How.XPATH, using = "//h1[text()='College']")
	public WebElement shopBooks_CollegeHeaderText;
	
	@FindBy(how = How.XPATH, using = "//h1[text()='Fiction']")
	public WebElement shopBooks_FictionHeaderText;
	
	@FindBy(how = How.XPATH, using = "//div[@class='headerDefault ']/h1")
	public WebElement shopBooks_YoungReaderHeaderText;
	

	@FindBy(how = How.XPATH, using = "//div[@class='headerDefault ']/h1")
	public WebElement shopBooks_NonFictionHeaderText;
	
	@FindBy(how = How.XPATH, using = "//h1[text()='Poetry']")
	public WebElement shopBooks_PoetryHeaderText;
	
	@FindBy(how = How.XPATH, using = "//div[@class='headerDefault ']/h1")
	public WebElement NonFictionHeaderText;

	@FindBy(how = How.XPATH, using = "//h1[text()='High School']")
	public WebElement HighSchool_HeaderText;

	
	@FindBy(how = How.XPATH, using = "//button[@class='slick-arrow slick-next']")
	public List <WebElement> carousel_NextButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='slick-arrow slick-prev']")
	public WebElement carousel_PreviousButton;
	
	@FindBy(how = How.XPATH, using = "//button[@class='slick-arrow slick-next slick-disabled']")
	public WebElement carousel_NextButtonDisabled;
	
	@FindBy(how = How.XPATH, using = "//button[@class='slick-arrow slick-prev slick-disabled']")
	public WebElement carousel_PreviousButtonDisabled;
	
	@FindBy(how = How.XPATH, using = "//span/h2[text()='NEW & FORTHCOMING']")
	public WebElement bookCarouselSection;
	
	@FindBy(how = How.XPATH, using = "//span/h2[text()='FIND YOUR INSTRUCTOR RESOURCES']")
	public WebElement instructorResourcesSection;
	
	@FindBy(how = How.XPATH, using = "//a[text()='Details']")
	public WebElement detailsHeaderLink;
	
	@FindBy(how = How.XPATH, using = "//button[@id='AddEmailSubmitButton' and text()='Close']")
	public WebElement closeButtonDetailsOverlay;
	
	@FindBy(how = How.XPATH, using = "//button[@class='searchClearButton']")
	public WebElement clearResultsButton;
	
	@FindBy(how = How.XPATH, using = "//button[contains(@target-url,'support') and text()='Learn More']")
	public WebElement learnMoreButton;
	
	@FindBy(how = How.XPATH, using = "//button[text()='Submit Support Ticket']")
	public WebElement supportTicketButton;

	@FindBy(how = How.XPATH, using ="//div[@class='banner-close']//*[name()='svg']")
	public WebElement bannerclose;
	
	public HomePage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}
	
	
	@Step("Click on HeaderLink - Reader,   Method: {method}")
	public void QAclickOnReader() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		readerElement.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(.,'Reader')]")));

	}

	@Step("Click on HeaderLink - Student,   Method: {method}")
	public void QAclickOnStudent() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		studentElement.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(.,'Student')]")));
	}

	@Step("Click on HeaderLink - Educator,   Method: {method}")
	public void QAclickOnEducator() {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		educatorElement.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(.,'Educator')]")));
	}

	@Step("Click on FooterLink - Who We Are,   Method: {method}")
	public void QAclickOnWhoWeAre() {
		boolean isbanner= driver.findElements(By.xpath("//div[@class='banner-close']//*[name()='svg']")).size() > 0;
		if(isbanner==true) {
			bannerclose.click();
		}
		WebDriverWait wait = new WebDriverWait(driver, 25);
		whoweareElement.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(.,'Who We Are')]")));
	}

	@Step("Click on FooterLink - Carreers,   Method: {method}")
	public void QAclickOnCareers() {
		boolean isbanner= driver.findElements(By.xpath("//div[@class='banner-close']//*[name()='svg']")).size() > 0;
		if(isbanner==true) {
			bannerclose.click();
		}
		WebDriverWait wait = new WebDriverWait(driver, 25);
		careersElement.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(.,'Careers')]")));
	}

	@Step("Click on FooterLink - Help,   Method: {method}")
	public void QAclickOnHelp() {
		boolean isbanner= driver.findElements(By.xpath("//div[@class='banner-close']//*[name()='svg']")).size() > 0;
		if(isbanner==true) {
			bannerclose.click();
		}
		WebDriverWait wait = new WebDriverWait(driver, 25);
		helpElement.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(.,'W. W. Norton Help')]")));
	}

	@Step("Click on FooterLink - Accessibility,   Method: {method}")
	public void QAclickOnAccessibility() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		accessibilityElement.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(.,'Accessibility')]")));
	}

	@Step("Click on FooterLink - Privacy Policy,   Method: {method}")
	public void QAclickOnPrivacyPolicy() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		privacypolicyElement.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(.,'Privacy Policy')]")));
	}

	@Step("Click on FooterLink - Terms Of Use,   Method: {method}")
	public void QAclickOnTermsOfUse() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		termsofuseElement.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(.,'Terms of Use')]")));
	}

	@Step("Click on FooterLink - Contact Us,   Method: {method}")
	public void QAclickOnContactUs() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		contactusElement.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(.,'Contact')]"))); 
	}

	@Step("Click on HeaderImage - Seagull Icon,   Method: {method}")
	public void clickOnSeagull() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", seagullicon);
		//seagullicon.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//div[@class='headerDefault ']/p[text()='W • W • NORTON & COMPANY INDEPENDENT & EMPLOYEE-OWNED']")));
	}

	@Step("Click on CheckOut - Seagull Icon,   Method: {method}")
	public void clickOnSeagull_CheckOut() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		seagulliconCheckOut.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
				"//div[@class='headerDefault ']/p[text()='W • W • NORTON & COMPANY INDEPENDENT & EMPLOYEE-OWNED']")));
	}

	@Step("Subscribe Valid Email ID for News Letter,   Method: {method}")
	public String QAsendEmailForNewsLetter(String emailID) {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='email']")));
		newsletter.sendKeys(emailID);
		sendnewsletter.click();

		String successMsg = null;

		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("successLabel")));

			if (successLabel.isDisplayed() == true) {
				successMsg = successLabel.getText();
			}

		} catch (Exception e) {
			return successMsg;
		}

		return successMsg;

	}

	@Step("Validate Error message for Invalid Email Id for News Letter,   Method: {method}")
	public String ValidateErrorForIncorrectEmail_NewsLetter(String incorrectEmailID) {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='email']")));

		String errorMsg = null;
		newsletter.sendKeys(incorrectEmailID);

		try {
			if (sendnewsletter.isEnabled() == false) {
				errorMsg = errormessageforincorrectEmail.getText();
			}

		} catch (Exception e) {
			return errorMsg;
		}

		return errorMsg;

	}

	@Step("Search Book on Home Page,   Method: {method}")
	public void searchBook(String bookName) {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(searchTextBox));
		searchTextBox.click();
		searchTextBox.sendKeys(bookName);
		searchButton.click();

	}
	
	@Step("Verify Book Header Text on Home Page,   Method: {method}")
	public String getBookHeaderText(String bookName) {
		WebElement HeaderText = driver.findElement(By.xpath("//h1[text()='" + bookName + "']"));
		return HeaderText.getText();

	}
	
	@Step("Verify Book Header Text on Home Page,   Method: {method}")
	public String getNoResultsText() {
		List <WebElement> NoResultsText = driver.findElements(By.xpath("//span[@class='noResults']"));
		return NoResultsText.get(0).getText();

	}
	
	@Step("Get the Author Name in book results,   Method: {method}")
	public String getAuthorNameInBookResults(String author) {
		WebElement authorName = driver.findElement(By.xpath("//span[text()='" + author + "']"));
		return authorName.getText();
	}
	
	@Step("Verify Book Name in Result list,   Method: {method}")
	public String getBookName(String bookName) {
		WebElement bookName_RessultsList = driver.findElement(By.xpath("//h2[@class='ResultTitle' and text()='" + bookName + "']"));
		return bookName_RessultsList.getText();

	}
	
	@Step("Verify Search Book Text in Result list,   Method: {method}")
	public boolean validateSearchBookText(String bookName) {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(searchTextBox));
		List <WebElement> bookName_RessultsList = driver.findElements(By.xpath("//h2[@class='ResultTitle' and contains(text(),'" + bookName + "')]"));
		for (int i=0; i<=bookName_RessultsList.size(); i++) {
			if (bookName_RessultsList.get(i).getText().contains(bookName)) {
				return true;
			}
		}
		
		return false;

	}
	
	@Step("Search for the author,   Method: {method}")
	public void searchAuthor(String authName) {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(searchTextBox));
		searchTextBox.click();
		searchTextBox.sendKeys(authName);
		searchButton.click();

	}

	@Step("Check Shop Book Button on Home Page,   Method: {method}")
	public boolean checkShopBookButton() {
		boolean isbanner= driver.findElements(By.xpath("//div[@class='banner-close']//*[name()='svg']")).size() > 0;
		if(isbanner==true) {
			bannerclose.click();
		}
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(shopBookButton));
		return shopBookButton.isEnabled();

	}
	
	@Step("Check Shop TextBook Button on Home Page,   Method: {method}")
	public boolean checkShopTextBookButton() {
		boolean isbanner= driver.findElements(By.xpath("//div[@class='banner-close']//*[name()='svg']")).size() > 0;
		if(isbanner==true) {
			bannerclose.click();
		}
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(shopBookButton));
		return shopTextBooksButton.isEnabled();

	}

	// --> Added By Surabhi

	@Step("Click on Shop Books button,   Method: {method}")
	public void clickOnShopBooksButton() {
		boolean isbanner= driver.findElements(By.xpath("//div[@class='banner-close']//*[name()='svg']")).size() > 0;
		if(isbanner==true) {
			bannerclose.click();
		}
		WebDriverWait wait = new WebDriverWait(driver, 25);
		shopBooksButton.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='All Books']")));

	}

	// --> Added By Surabhi

	@Step("Click on Shop Text Books button,   Method: {method}")
	public void clickOnShopTextBooksButton() {
		boolean isbanner= driver.findElements(By.xpath("//div[@class='banner-close']//*[name()='svg']")).size() > 0;
		if(isbanner==true) {
			bannerclose.click();
		}
		WebDriverWait wait = new WebDriverWait(driver, 25);
		shopTextBooksButton.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Textbooks']")));

	}
	

	// --> Added By Surabhi

	@Step("Click on Details header link,   Method: {method}")
	public void QAclickOnDetailsLinkHeader() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		detailsHeaderLink.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[@class='modal-title' and text()='Details']")));

	}
	
	@Step("Close the Details overlay,   Method: {method}")
	public void QAcloseDetailsOverlay() {
		
		closeButtonDetailsOverlay.click();
		

	}
	
	
	// --> Added By Ashish
	
	@Step("browseBookCarousel with Next button,   Method: {method}")
	public void browseBookCarousel_Next() {
			while (checkBookCarousel_Next() == true) {
					carousel_NextButton.get(0).click();
				}
	}
	
	public boolean checkBookCarousel_Next() {	
		try {
				return carousel_NextButton.get(0).isDisplayed();
			} catch(Exception e) {
					return false;
			}
	}
	
	
	// --> Added By Ashish
	
	@Step("Click on Shop Text Books button,   Method: {method}")
	public void browseBookCarousel_Previous() {

			while( checkBookCarousel_Previous() == true) {
					carousel_PreviousButton.click();
				}
	}
	
	public boolean checkBookCarousel_Previous() {	
		try {
				return carousel_PreviousButton.isDisplayed();
			} catch(Exception e) {
					return false;
				}
		
		}
	
	
	@Step("Click on Shop Browse button in Educator page,   Method: {method}")
	public void clickOnShopBooks_BrowseButton() {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		String buttonLabel = shopBooks_BrowseButtonList.get(1).getText();
		if(buttonLabel.equalsIgnoreCase("Browse") || buttonLabel.equalsIgnoreCase("Shop College")) {
			shopBooks_BrowseButtonList.get(1).click();
		} else {
			shopBooks_BrowseButtonList.get(0).click();
		}
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='College']")));

	}
	
	
	@Step("Click on Fiction Browse button in Reader page,   Method: {method}")
	public void clickOnFiction_BrowseButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='row']")));
		shopBooks_Reader.get(0).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Books']")));
		fictionLabel.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Fiction']")));

	}
	
	@Step("Click on Non-Fiction Browse button in Reader page,   Method: {method}")
	public void clickOnNonFiction_BrowseButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='row']")));
		shopBooks_Reader.get(0).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Books']")));
		nonfictionLabel.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='headerDefault ']/h1")));

	}
	
	@Step("Click on Poetry Browse button in Reader page,   Method: {method}")
	public void clickOnPoetry_BrowseButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='row']")));
		shopBooks_Reader.get(0).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Books']")));
		poetryLabel.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Poetry']")));

	}
	
	@Step("Click on Fiction Browse button in Reader page,   Method: {method}")
	public void clickOnYoungReader_BrowseButton() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='row']")));
		shopBooks_Reader.get(4).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1/p[contains(text(),'Young Readers')]")));
		try {
				if (youngReader_BrowseButton.isDisplayed() == true) {
					youngReader_BrowseButton.click();
				}
		} catch (Exception e) {e.getMessage();}
	}
	
	
	@Step("Click on Learn More Button,   Method: {method}")
	public void clickLearnMoreButton() {	
		learnMoreButton.click();
	}
	
	@Step("Click on support Ticket Button,   Method: {method}")
	public void clicksupportTicketButton() {
		ReusableMethods.scrollToElement(driver, 
				By.xpath("//button[text()='Submit Support Ticket']"));
		supportTicketButton.click();
	}
	
	
	@Step("Click on Fiction Browse button in Reader page,   Method: {method}")
	public boolean browseTextBooks() throws InterruptedException {
		boolean browseTextBooksExist = false;
		try {
				if(browseTextBooks.isDisplayed()) {
					browseTextBooksExist = true;
				}
		} catch (Exception e) {e.getMessage();}
		
		return browseTextBooksExist;
	}
}

