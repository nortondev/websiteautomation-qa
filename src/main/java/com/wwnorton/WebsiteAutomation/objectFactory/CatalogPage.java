package com.wwnorton.WebsiteAutomation.objectFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;


public class CatalogPage {

	WebDriver driver;

	
	@FindBy(how = How.XPATH, using = "//span[text()='Religion']")
	public WebElement religionCategory;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Bibles']")
	public WebElement biblesCategory;
	
	@FindBy(how = How.XPATH, using = "//span[text()='King James Version']")
	public WebElement kingJamesVersionCategory;
	
	@FindBy(how = How.XPATH, using = "//h2[@class='ResultTitle'][1]")
	public WebElement clickOnTheEnglishBibleBook;
	
	@FindBy(how = How.XPATH, using = "//li[@class='CategoryItem'][1]")
	public WebElement firstCategory;
	
	@FindBy(how = How.XPATH, using = "//li[@class='CategoryItem'][2]")
	public WebElement secoundCategory;
	
	@FindBy(how = How.XPATH, using = "//li[@class='CategoryItem'][3]")
	public WebElement thirdCategory;
	
	@FindBy(how = How.XPATH, using = "//li[@class='CategoryItem'][4]")
	public WebElement fourthCategory;
	
	@FindBy(how = How.XPATH, using = "//li[@class='CategoryItem'][5]")
	public WebElement fifthCategory;
	
	@FindBy(how = How.XPATH, using = "//li[@class='CategoryItem'][6]")
	public WebElement sixthCategory;
	
	@FindBy(how = How.XPATH, using = "//a[text()='Instructor Resources']")
	public WebElement instructorResourceButton;
	
	public CatalogPage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}
	
	// --> Added By Surabhi 
	
		@Step("Click On Non-Fiction category,   Method: {method}")
		public void clickOnFourthCategory() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			fourthCategory.click();
			//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Nonfiction']")));
			
		}
		
		// --> Added By Surabhi 
		
		@Step("Click On Religion category,   Method: {method}")
		public void clickOnReligionCategory() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			religionCategory.click();
			//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Religion']")));
			
		}
		
	// --> Added By Surabhi 
		
		@Step("Click On Bibles category,   Method: {method}")
		public void clickOnBiblesCategory() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			biblesCategory.click();
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='Bibles']")));
			
		}
		
		@Step("Click On King James Version category,   Method: {method}")
		public void clickOnKingJamesVersionCategory() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			kingJamesVersionCategory.click();
			//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[text()='King James Version']")));
			
		}

		@Step("Click On the book 9780393347043 The English Book,   Method: {method}")
		public void clickOnBookTheEnglishBible() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			clickOnTheEnglishBibleBook.click();
			
			
		}

		@Step("Click On the College category,   Method: {method}")
		public void clickOnFirstCategory() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			firstCategory.click();
			
			
		}
		
		@Step("Click On the Secound category,   Method: {method}")
		public boolean clickOnSecoundCategory() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			
			try {
					secoundCategory.click();
					return true;
			} catch (Exception e) {
				return false;
			}
			
		}
		
		@Step("Instructor resource buttons in catalog page,   Method: {method}")
		public void clickOnInstructorResourceButton() {
			WebDriverWait wait = new WebDriverWait(driver, 25);
			
			if (instructorResourceButton.isDisplayed() == true) {

				instructorResourceButton.click();
			}

			else {
				System.out.println("Instructor Resource buttons are not displaying in catalog page");
			}
			
			
		}
}
