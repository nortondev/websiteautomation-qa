package com.wwnorton.WebsiteAutomation.objectFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;



public class BookSearchResultsPage {
	
WebDriver driver;
BookDetailsPage bdp;
	
	
	// Finding Web Elements on Book Search Results page using PageFactory.
	

	@FindBy(how = How.XPATH, using = "//body//h1[1]")
	WebElement searchResultsText;
	
	@FindBy (how = How.XPATH, using = "//button[contains(text(),'Show More')]")
	WebElement showMoreText;
	
	@FindBy (how = How.XPATH, using = "//div[@class='ResultTitle']/a/h2")
	public List <WebElement> firstBook;
	
	@FindBy (how = How.XPATH, using = "//div[@class='ResultAuthors']/span")
	List<WebElement> bookAuthor;
	
	@FindBy (how = How.XPATH, using = "//span[@class='itemLabel' and text()='College']")
	WebElement categoryCollege;
	
	@FindBy (how = How.XPATH, using = "//span[@class='itemLabel' and text()='Fiction']")
	WebElement categoryFiction;
	
	@FindBy (how = How.XPATH, using = "//span[@class='itemLabel' and text()='High School']")
	WebElement categoryHighSchool;
	
	@FindBy (how = How.XPATH, using = "//span[@class='itemLabel' and text()='Nonfiction']")
	WebElement categoryNonFiction;
	
	@FindBy (how = How.XPATH, using = "//span[@class='itemLabel' and text()='Poetry']")
	WebElement categoryPoetry;
	
	@FindBy (how = How.XPATH, using = "//span[@class='itemLabel' and text()='Young Readers']")
	WebElement categoryYoungReaders;
	
	@FindBy (how = How.XPATH, using = "//span[@class='text']")
	WebElement filterName;
	
	@FindBy (how = How.XPATH, using = "//button[@class='center [object Object] ']")
	WebElement resetFiltersButton;
	
	@FindBy (how = How.XPATH, using = "//div[@id='sortDropDown']/div")
	WebElement sortDropDown;
	
	@FindBy (how = How.XPATH, using = "//span[text()='Suzy Pepper Rollins']")
	WebElement authorNameInBookResults1;
	
	@FindBy (how = How.XPATH, using = "//ul[@data-testid='dropdown']")
	WebElement dropDownFilters;
	
	@FindBy (how = How.XPATH, using = "//ul[@data-testid='dropdown']/li")
	List<WebElement> allDropDownFilters;
	
	@FindBy (how = How.XPATH, using = "//span[text()='Barbara Russano Hanning']")
	WebElement authorNameInBookResults2;

	@FindBy (how = How.XPATH, using = "//a[@href='/books/9781324016847']")
	WebElement clickOnBookResult1;
	
	@FindBy (how = How.XPATH, using = "//a[@href='/books/9780393421583']")
	WebElement clickOnBookResult2;
	
	@FindBy (how = How.XPATH, using = "//span[@class='itemLabel' and text()='High School']")
	WebElement HighSchoolLabel;
	
	@FindBy (how = How.XPATH, using = "//span[@class='itemLabel' and text()='Language Arts']")
	WebElement LanguageArtsLabel;
	
	@FindBy (how = How.XPATH, using = "//span[@class='itemLabel' and text()='Honors English']")
	WebElement HonorsEnglishLabel;
	
	@FindBy (how = How.XPATH, using = "//button[@aria-label='close']")
	public WebElement buttonClose;
	
	@FindBy (how = How.XPATH, using = "//span[@class='itemLabel' and text()='Young Readers']")
	WebElement YoungReadersLabel;
	
	@FindBy (how = How.XPATH, using = "//span[@class='itemLabel' and text()='Ages 9-12']")
	WebElement AgeLabel;
	
	@FindBy (how = How.XPATH, using = "//span[@class='itemLabel' and text()='Fiction']")
	WebElement FictionLabel;
	
	@FindBy (how = How.XPATH, using = "//span[@class='itemLabel' and text()='General']")
	WebElement GeneralLabel;
	
	
	// Initializing Web Driver and PageFactory.
	
	public BookSearchResultsPage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}
	
	
	// Capture Search key on Search Results page.
	
	@Step("Get Search Key,  Method: {method} ")
	public String getSearchResultsText() throws Exception {
		
		//WebDriverWait wait = new WebDriverWait(driver,3);
		//TimeUnit.SECONDS.sleep(3);
		//wait.until(ExpectedConditions.visibilityOf(searchResultsText));
		Thread.sleep(3000);
		return searchResultsText.getText();
		
	}
	
	
	// Validate Show More button exist at the bottom of the search result list and click on it if exist.
	
	@Step("Validate Show More exist?,  Method: {method} ")
	public void showMoreExist() throws Exception {

		try {
			
			Thread.sleep(5000);
			if(showMoreText.isDisplayed()) {
				
				WebDriverWait wait = new WebDriverWait(driver,5);
				TimeUnit.SECONDS.sleep(5);
				
				wait.until(ExpectedConditions.visibilityOf(showMoreText));
				showMoreText.click();
				
				Thread.sleep(3000);
				
			}
	}
	
		catch (NoSuchElementException e) {
		
			throw new Exception ("Show More button is NOT displayed");
			
			}
	}
	
	
	//Click on first book link in Search result list.
		
	@Step("Select Book from Search Results,  Method: {method} ")
	public void selectBookFromList(int i) throws Exception {
		
		WebDriverWait wait = new WebDriverWait (driver, 25);
		wait.until(ExpectedConditions.visibilityOf(firstBook.get(0)));
		firstBook.get(i).click();				
	}
	
	
	@Step("Select a Book from Search Results list,  Method: {method} ")
	public String selectBook(String bookName) throws Exception {

			WebElement ResultTitleBook;
			String strBookName = null;
			String authorName = null;
			
			for (int i=4; i<=33; i++) 
			{
				Thread.sleep(1000);
				ResultTitleBook = driver.findElement(By.xpath("//div[@class='ResultContainer']//div[" + i + "]//a//h2"));
				strBookName = ResultTitleBook.getText();
				authorName = bookAuthor.get(i-4).getText();
			
				if(strBookName.contains(bookName)) 
					{
						ResultTitleBook.click();
						break;
							
					}
		
			}
			
			return authorName;	
	}
	
	
	@Step("Select a Text Book from Search Results list,  Method: {method} ")
	public String selectTextBook() throws Exception {

			WebElement ResultTitleTextBook;
			String TextBookName = null;
			
			for (int i=1; i<=30; i++) 
			{	
				Thread.sleep(1000);
				ResultTitleTextBook = driver.findElement(By.xpath("//*[@id=\"SearchQueryResult\"]/div[6]/div[3]/div[" + i + "]/div[2]/div[1]/div[1]/a"));
				
				TextBookName = ResultTitleTextBook.getText();
				
				if(TextBookName.contains("bookName")) 
					{
						ResultTitleTextBook.click();
						break;
							
					}
		
			}
			
			return TextBookName;
			
	}
	
	
	@Step("Select a Not Yet Published Book from Search Results list,  Method: {method} ")
	public String selectNYPBook() throws Exception {

			WebElement ResultTitleNYPBook;
			String NYPBookName = null;
			
			for (int i=1; i<=30; i++) 
			{
				Thread.sleep(1000);
				ResultTitleNYPBook = driver.findElement(By.xpath("//*[@id=\"SearchQueryResult\"]/div[6]/div[3]/div[" + i + "]/div[2]/div[1]/div[1]/a"));
				
				NYPBookName = ResultTitleNYPBook.getText();
				
				if(NYPBookName.contains("NYPBookName")) 
					{
						ResultTitleNYPBook.click();
						break;
							
					}
		
			}
			
			return NYPBookName;
			
	}
	
	
	@Step("Select a Not Yet Published eBook from Search Results list,  Method: {method} ")
	public String selectNYPeBook() throws Exception {

			WebElement ResultTitleNYPeBook;
			String NYPeBookName = null;
			
			for (int i=1; i<=30; i++) 
			{
				Thread.sleep(1000);
				ResultTitleNYPeBook = driver.findElement(By.xpath("//*[@id=\"SearchQueryResult\"]/div[6]/div[3]/div[" + i + "]/div[2]/div[1]/div[1]/a"));
				
				NYPeBookName = ResultTitleNYPeBook.getText();
				
				if(NYPeBookName.contains("NYPeBookName")) 
					{
						ResultTitleNYPeBook.click();
						break;
							
					}
		
			}
			
			return NYPeBookName;
			
	}
	
	
	@Step("Select a eBook Item from Search Results list,  Method: {method} ")
	public String selecteItemBook() throws Exception {

			WebElement ResultTitleeItemBook;
			String eItemBookName = null;
			
			for (int i=1; i<=30; i++) 
			{
				Thread.sleep(1000);
				ResultTitleeItemBook = driver.findElement(By.xpath("//*[@id=\"SearchQueryResult\"]/div[6]/div[3]/div[" + i + "]/div[2]/div[1]/div[1]/a"));
				
				eItemBookName = ResultTitleeItemBook.getText();
				
				if(eItemBookName.contains("eItemBookName")) 
					{
					
						ResultTitleeItemBook.click();
						break;
							
					}
		
			}
			
			return eItemBookName;	
	}
	
	
	@Step("Validate All links in Categories section,  Method: {method} ")
	public boolean checkAllCategoryLinks() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,5);
		TimeUnit.SECONDS.sleep(5);
		
		try {
				wait.until(ExpectedConditions.visibilityOf(categoryCollege));
				categoryCollege.isDisplayed();
				categoryFiction.isDisplayed();
				categoryHighSchool.isDisplayed();
				categoryNonFiction.isDisplayed();
				categoryPoetry.isDisplayed();
				categoryYoungReaders.isDisplayed();

			} catch (NoSuchElementException e) {
				return false;
			}
		
		return true;
	}
	
	@Step("Apply Filter option,  Method: {method} ")
	public void clickFilterOption(String filterOption, String filterName) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		driver.findElement(By.xpath("//span[@class='picky__placeholder' and text()='" + filterOption + "']")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//input[@aria-label='" + filterName + "']")));		
		driver.findElement(By.xpath("//input[@aria-label='" + filterName + "']")).click();
	}
	
	@Step("Get Filter Name,  Method: {method} ")
	public String getFilterName() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(filterName));
		return filterName.getText();
	}
	
	@Step("Click Reset filter button,  Method: {method} ")
	public void clickResetFilterButton(String filterOption) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		driver.findElement(By.xpath("//span[@class='picky__placeholder' and text()='" + filterOption + "']")).click();
		wait.until(ExpectedConditions.visibilityOf(resetFiltersButton));
		resetFiltersButton.click();
	}
	
	@Step("click on Sort DropDown,  Method: {method} ")
	public void clickSortDropDown() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(sortDropDown));
		sortDropDown.click();
	}
	
	@Step("click on Sort DropDown,  Method: {method} ")
	public void selectSortOption(String sortName) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@id='react-select-2-option-0']")));
		WebElement filterOption; 
		for(int i=0; i<4; i++) {
			filterOption = driver.findElement(By.xpath("//div[@id='react-select-2-option-" + i + "']"));
			if(filterOption.getText().equals(sortName)) {
				filterOption.click();
				break;
			}
		}
	}
	
		
	@Step("click on Sort DropDown,  Method: {method} ")
	public String getSelectedSortOption(String sortName) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[contains(@class,'css') and text()='" + sortName + "']")));
		WebElement filterOption; 
			filterOption = driver.findElement(
					By.xpath("//div[contains(@class,'css') and text()='" + sortName + "']"));
			return filterOption.getText();	
	}
	
	@Step("Check on Editions and Volumes Filter DropDown,  Method: {method} ")
	public String getBookListFilterOption(String filterName) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//button/span[@class='picky__placeholder' and text()='" + filterName + "']")));
		WebElement filterOption; 
			filterOption = driver.findElement(
					By.xpath("//button/span[@class='picky__placeholder' and text()='" + filterName + "']"));
			return filterOption.getText();
	}
	
	@Step("Check on Editions and Volumes Filter DropDown,  Method: {method} ")
	public boolean clickBookListFilterOption(String filterName) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 25);
		
		try {
				wait.until(ExpectedConditions.visibilityOfElementLocated(
						By.xpath("//button/span[@class='picky__placeholder' and text()='" + filterName + "']")));
				Thread.sleep(1000);
				WebElement filterOption; 
					filterOption = driver.findElement(
							By.xpath("//button/span[@class='picky__placeholder' and text()='" + filterName + "']"));
					filterOption.click();
					return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	@Step("Checked all fitlers in Filter DropDown,  Method: {method} ")
	public String clickAllFiltersInFilterDropDown() throws Exception {
		WebElement filterOption;
		
		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(dropDownFilters));
		
		int TotalFilters = allDropDownFilters.size();
		int i=0;
		int updatedFilters = 0;
		while(i<TotalFilters) {
			filterOption = driver.findElement(
					By.xpath("//ul/li[contains(@id,'option-" + i + "')]/input"));
			filterOption.click();
			Thread.sleep(3000);
			updatedFilters = allDropDownFilters.size();
			if (updatedFilters == 1) {
				break;
			}
			
			i++;
		}
		
/*		for(int i=0; i<TotalFilters; i++) {
			filterOption = driver.findElement(
					By.xpath("//ul/li[contains(@id,'option-" + i + "')]/input"));
			filterOption.click();
		}
*/
		
		return Integer.toString(updatedFilters);
	}
	

	@Step("Get the Author Name Suzy Pepper Rollins in book results,   Method: {method}")
	public String getAuthorNameInBookResults1() {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(authorNameInBookResults1));

		String authName = authorNameInBookResults1.getText();
		return authName;

	}
	

	@Step("Click on the book result,   Method: {method}")
	public void clickOnTheBookResult1() {

		clickOnBookResult1.click();

	}
	
	@Step("Get the Author Name Barbara Russano Hanning in book results,   Method: {method}")
	public String getAuthorNameInBookResults2() {

		WebDriverWait wait = new WebDriverWait(driver, 25);
		wait.until(ExpectedConditions.visibilityOf(authorNameInBookResults2));

		String authName = authorNameInBookResults2.getText();
		return authName;

	}
	
	@Step("Click on the book result,   Method: {method}")
	public void clickOnTheBookResult2() {

		clickOnBookResult2.click();

	}
	
	@Step("Click on High School Category,   Method: {method}")
	public void navigateToHighScoolCategory() {
		HighSchoolLabel.click();
	}
	
	@Step("Click on Language Arts Category,   Method: {method}")
	public void navigateToLanguageArtsCategory() {
		LanguageArtsLabel.click();
	}
	
	@Step("Click on Honors English Category,   Method: {method}")
	public void navigateToHonorsEnglishCategory() {
		HonorsEnglishLabel.click();
	}
	
	@Step("Click on Young Reader Category,   Method: {method}")
	public void navigateToYoungReaderCategory() {
		YoungReadersLabel.click();
	}
	
	@Step("Click on Age Category,   Method: {method}")
	public void navigateToAgeCategory() {
		AgeLabel.click();
	}
	
	@Step("Click on Fiction Category,   Method: {method}")
	public void navigateToFictionCategory() {
		FictionLabel.click();
	}
	
	@Step("Click on General Category,   Method: {method}")
	public void navigateToGeneralCategory() {
		GeneralLabel.click();
	}
	
}
