package com.wwnorton.WebsiteAutomation.objectFactory;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;

import ru.yandex.qatools.allure.annotations.Step;

public class ContactUsPage {

	WebDriver driver;

	@FindBy(xpath = "//a[text()='Permissions']")
	WebElement permissionLink;
	
	@FindBy(xpath = "//a[contains(text(),'Website Feedback')]")
	WebElement feedbackLink;
	
	@FindBy(xpath = "//a[text()='Submit a Correction']")
	WebElement correctionLink;

	@FindBy(xpath = "//button[text()='Request Form']")
	WebElement requestFormButton;
	
	@FindBy(xpath = "//button[text()='Correction Form']")
	WebElement correctionFormButton;
	
	@FindBy(xpath = "//button[text()='Feedback Form']")
	WebElement feedbackFormButton;

	@FindBy(xpath = "//h4[@class='modal-title' and text()='Contact Information']")
	WebElement contactInformationFormTitle;

	@FindBy(xpath = "//input[@id='name']")
	WebElement fullNameElement;

	@FindBy(xpath = "//input[@id='companyName']")
	WebElement companyNameElement;

	@FindBy(xpath = "//input[@id='address1']")
	WebElement addressLine1Element;

	@FindBy(xpath = "//input[@id='address2']")
	WebElement addressLine2Element;

	@FindBy(xpath = "//input[@id='city']")
	WebElement cityNameElement;

	@FindBy(xpath = "//input[@id='state']")
	WebElement stateNameElement;

	@FindBy(xpath = "//input[@id='zip']")
	WebElement zipCodeNameElement;

	@FindBy(xpath = "//input[@id='phone']")
	WebElement phoneNoElement;

	@FindBy(xpath = "//input[@id='fax']")
	WebElement faxNoElement;

	@FindBy(xpath = "//input[@id='emailAddress']")
	WebElement emailIdElement;

	@FindBy(xpath = "//input[@id='productTitle']")
	WebElement productTitleElement;
	
	@FindBy(xpath = "//input[@id='isbn']")
	WebElement ISBNElement;
	
	@FindBy(xpath = "//input[@id='pageNbrOrUrl']")
	WebElement pageNoURLlement;
	
	@FindBy(xpath = "//*[@id='CommonModelPop']/div/div/div[2]/div/div/div/div/form/div/div[6]/div[2]/textarea")
	WebElement quoteTheErrorlement;	

	@FindBy(xpath = "//*[@id='CommonModelPop']/div/div/div[2]/div/div/div/div/form/div/div[7]/div/div[2]/textarea")
	WebElement suggestACorrectionElement;
	
	@FindBy(xpath = "//button[@id='ProfileAddressSubmitButton']")
	WebElement contactInfoContinueButton;

	@FindBy(xpath = "//input[@id='authorEditor']")
	WebElement authorEditorElement;

	@FindBy(xpath = "//input[@id='isbn']")
	WebElement isbnElement;

	@FindBy(xpath = "//input[@id='title']")
	WebElement titleElement;

	@FindBy(xpath = "//input[@id='copyrightLine']")
	WebElement copyRightElement;

	@FindBy(xpath = "//input[@id='page']")
	WebElement pageElement;

	@FindBy(xpath = "//input[@id='titleSelection']")
	WebElement titleSelectionElement;

	@FindBy(xpath = "//input[@id='totalPages']")
	WebElement totalPagesElement;

	@FindBy(xpath = "//input[@id='totalWords']")
	WebElement totalWordsElement;

	@FindBy(xpath = "//input[@id='totalLines']")
	WebElement totalLinesElement;

	@FindBy(xpath = "//input[@id='totalIllustration']")
	WebElement totalIllustrationElement;

	@FindBy(xpath = "//input[@id='yourPublicationTitle']")
	WebElement yourPublicationTitleElement;

	@FindBy(xpath = "//input[@id='yourPublicationAuthorEditor']")
	WebElement yourPublicationAuthorEditorElement;

	@FindBy(xpath = "//input[@id='yourPublicationPublisher']")
	WebElement yourPublicationPublisherElement;

	@FindBy(className = "react-datepicker__input-container")
	WebElement PublicationDate;

	@FindBy(xpath = "//input[@id='otherPublicationPubFormat']")
	WebElement FormateTypeElement;

	@FindBy(xpath = "//input[@id='yourPublicationNbrPages']")
	WebElement NoOfPagesElement;

	@FindBy(xpath = "//input[@id='yourPublicationTotalQty']")
	WebElement PubTotalQtyElement;

	@FindBy(xpath = "//input[@id='yourPublicationPrice']")
	WebElement PubPriceElement;

	@FindBy(xpath = "//textarea[@id='yourPublicationAddComment']")
	WebElement AddCommElement;

	@FindBy(xpath = "//textarea[@id='additinalComment']")
	WebElement AddCommentsFeedback;
	
	@FindBy(xpath = "//button[@id='ProfileAddressSubmitButton']")
	WebElement SubmitFormButton;
	
	@FindBy(xpath = "//h4[@class='modal-title' and text()='Confirmation']")
	public WebElement confirmationPopup;
	
	@FindBy(xpath = "//span[@class='headerText emailHeaderText' and text()='Your permissions request has been submitted.']")
	public WebElement confirmationMsg_Permission;
	
	@FindBy(xpath = "//span[@class='headerText emailHeaderText' and text()='Your website feedback has been submitted.']")
	public WebElement confirmationMsg_Feedback;
	
	@FindBy(xpath = "//span[@class='headerText emailHeaderText' and text()='Your correction has been submitted.']")
	public WebElement confirmationMsg_Correction;
	
	@FindBy(xpath = "//button[@id='AddEmailSubmitButton' and text()='Close']")
	public WebElement closeButton;

	public ContactUsPage() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);

	}

	@Step("Click on Permission link,   Method: {method}")
	public void clickOnPermissionLink() throws IOException {
		permissionLink.click();

	}

	@Step("Click on Request Form button,   Method: {method}")
	public void clickOnRequestFormButton() throws IOException {
		requestFormButton.click();

	}

	@Step("Submit Permission Contact Information Overlay,   Method: {method}")
	public void submitPermissionContactInformationForm(String fName, String companyName, String addLine1,
			String addLine2, String cityName, String stateName, String zipCode, String phnNo, String faxNo,
			String emailId) throws InterruptedException {

		Thread.sleep(1000);
		fullNameElement.sendKeys(fName);
		Thread.sleep(1000);
		companyNameElement.sendKeys(companyName);
		Thread.sleep(1000);
		addressLine1Element.sendKeys(addLine1);
		Thread.sleep(1000);
		addressLine2Element.sendKeys(addLine2);
		Thread.sleep(1000);
		addressLine2Element.sendKeys(addLine2);
		Thread.sleep(1000);
		cityNameElement.sendKeys(cityName);
		Thread.sleep(1000);
		stateNameElement.sendKeys(stateName);
		Thread.sleep(1000);
		zipCodeNameElement.sendKeys(zipCode);
		Thread.sleep(1000);
		phoneNoElement.click();
		Thread.sleep(1000);
		phoneNoElement.sendKeys(phnNo);
		Thread.sleep(1000);
		faxNoElement.click();
		Thread.sleep(1000);
		faxNoElement.sendKeys(faxNo);
		Thread.sleep(1000);
		emailIdElement.sendKeys(emailId);
		Thread.sleep(1000);
		contactInfoContinueButton.click();

	}

	@Step("Submit Permission Book Information Overlay   Method: {method}")
	public void submitPermissionBookInformationForm(String authorName, String ISBN, String Title, String copyRightLine,
			String pages, String titleOfSelection, String TotalNoPages, String wordCount, String noOfLines,
			String totalIllus, String emailId) throws InterruptedException {

		Thread.sleep(1000);
		authorEditorElement.sendKeys(authorName);
		Thread.sleep(1000);
		isbnElement.sendKeys(ISBN);
		Thread.sleep(1000);
		titleElement.sendKeys(Title);
		Thread.sleep(1000);
		copyRightElement.sendKeys(copyRightLine);
		Thread.sleep(1000);
		pageElement.sendKeys(pages);
		Thread.sleep(1000);
		titleSelectionElement.sendKeys(titleOfSelection);
		Thread.sleep(1000);
		totalPagesElement.sendKeys(TotalNoPages);
		Thread.sleep(1000);
		totalWordsElement.sendKeys(wordCount);
		Thread.sleep(1000);
		totalLinesElement.sendKeys(noOfLines);
		Thread.sleep(1000);
		totalIllustrationElement.sendKeys(totalIllus);
		Thread.sleep(1000);
		contactInfoContinueButton.click();

	}

	@Step("Submit Permission Your Publication overlay,   Method: {method}")
	public void submitPermissionYourPublicationForm(String yPubTitle, String yPubAuthEdit, String yPublisher,
			String currnetDay, String FType, String NoOfPages, String TQty, String PubPrice, String AddComments)
			throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 50);
		yourPublicationTitleElement.sendKeys(yPubTitle);
		Thread.sleep(1000);
		yourPublicationAuthorEditorElement.sendKeys(yPubAuthEdit);
		Thread.sleep(1000);
		yourPublicationPublisherElement.sendKeys(yPublisher);
		Thread.sleep(1000);

		PublicationDate.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='react-datepicker__month']")));
		WebElement calendarDate = driver.findElement(By.xpath(""
				+ "//div[@class='react-datepicker__month']/div/div[contains(@class,'today')]"));
		if (calendarDate.getText().equalsIgnoreCase(currnetDay)) {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", calendarDate);

			List<WebElement> els = driver.findElements(By.xpath("//input[@type='checkbox']"));
			for (WebElement el : els) {
				if (!el.isSelected()) {
					el.click();
				}

			}
		}
		
		Thread.sleep(1000);
		FormateTypeElement.sendKeys(FType);
		Thread.sleep(1000);
		NoOfPagesElement.sendKeys(NoOfPages);
		Thread.sleep(1000);
		PubTotalQtyElement.sendKeys(TQty);
		Thread.sleep(1000);
		PubPriceElement.sendKeys(PubPrice);
		Thread.sleep(1000);
		AddCommElement.sendKeys(AddComments);
		Thread.sleep(1000);
		SubmitFormButton.click();

	}
	
	@Step("Check confirmation pop-up & confirmation message for Permission form,   Method: {method}")
	public boolean checkPermissionConfirmationMessage() throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(confirmationPopup));
		boolean confirmationMsgExist = false;
		try {
				confirmationMsgExist = confirmationMsg_Permission.isDisplayed();
				if (confirmationMsgExist == true) {
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", closeButton);
				}
		} catch (Exception e) {
			return false;
		}
		
		return confirmationMsgExist;

}
	
	@Step("Click on Website feedback link,   Method: {method}")
	public void clickOnFeedbackLink() throws IOException {
		feedbackLink.click();

	}
	
	@Step("Click on Feedback Form button,   Method: {method}")
	public void clickOnFeedbackFormButton() throws IOException {
		feedbackFormButton.click();

	}
	
	@Step("Provide details in website feedback overlay,   Method: {method}")
	public void submitFeedbackForm(String Name ,String emailId , String Comments) throws InterruptedException {

		fullNameElement.sendKeys(Name);
		emailIdElement.sendKeys(emailId);
		AddCommentsFeedback.sendKeys(Comments);
		SubmitFormButton.click();
	}
	
	@Step("Check confirmation pop-up & confirmation message for Feedback form,   Method: {method}")
	public boolean checkFeedbackConfirmationMessage() throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(confirmationPopup));
		boolean confirmationMsgExist = false;
		try {
				confirmationMsgExist = confirmationMsg_Feedback.isDisplayed();
				if (confirmationMsgExist == true) {
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", closeButton);
				}
		} catch (Exception e) {
			return false;
		}
		
		return confirmationMsgExist;

}
	@Step("Click on Submit A Correction link,   Method: {method}")
	public void clickOnSubmitACorrectionLink() throws IOException {
		correctionLink.click();

	}
	
	@Step("Click on Correction Form button,   Method: {method}")
	public void clickOnCorrectionFormButton() throws IOException {
		correctionFormButton.click();

	}
	
	@Step("Provide details in Submit A Correction overlay,   Method: {method}")
	public void submitCorrectionForm(String Name ,String emailId , String ProdTitle , String ISBN , String PageNoURL , String QuoteTheError , String SuggestACorrection) throws InterruptedException {
		
		Thread.sleep(1000);
		fullNameElement.sendKeys(Name);
		Thread.sleep(1000);
		emailIdElement.sendKeys(emailId);
		Thread.sleep(1000);
		productTitleElement.sendKeys(ProdTitle);
		Thread.sleep(1000);
		ISBNElement.sendKeys(ISBN);
		Thread.sleep(1000);
		pageNoURLlement.sendKeys(PageNoURL);
		Thread.sleep(1000);
		quoteTheErrorlement.sendKeys(QuoteTheError);
		Thread.sleep(1000);
		suggestACorrectionElement.sendKeys(SuggestACorrection);
		Thread.sleep(1000);
		SubmitFormButton.click();
	}
	
	@Step("Check confirmation pop-up & confirmation message for Correction form,   Method: {method}")
	public boolean checkCorrectionConfirmationMessage() throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 50);
		wait.until(ExpectedConditions.visibilityOf(confirmationPopup));
		boolean confirmationMsgExist = false;
		try {
				confirmationMsgExist = confirmationMsg_Correction.isDisplayed();
				if (confirmationMsgExist == true) {
					((JavascriptExecutor) driver).executeScript("arguments[0].click();", closeButton);
				}
		} catch (Exception e) {
			return false;
		}
		
		return confirmationMsgExist;

}
}