package com.wwnorton.WebsiteAutomation.objectFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.wwnorton.WebsiteAutomation.utilities.BaseDriver;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;

import ru.yandex.qatools.allure.annotations.Step;

public class CartShipping {
	
	WebDriver driver;
	
	
	// Finding Web Elements on Book Details page with respect to Add to Cart using PageFactory.
	
	
	@FindBy(how = How.XPATH, using = "//div[@id='studentInformationPanel--heading']")
	WebElement studentPanel;
	
	@FindBy(how = How.XPATH, using = "//input[@value='student']")
	WebElement radioButtonStudent;
	
	@FindBy(how = How.XPATH, using = "//input[@value='nonstudent']")
	WebElement radioButtonNotStudent;
	
	//div[@id='studentInformationPanel--body']//div[2]//label[1]
	
	@FindBy(how = How.XPATH, using = "//span[@class='PolicyTitle']//a[contains(text(),'international')]")
	WebElement policyLinkOne;
	
	@FindBy(how = How.XPATH, using = "//span[@class='PolicyTitle']//a[contains(text(),'high school orders')]")
	WebElement policyLinkTwo;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"refCollegeCityName\"]/div/div[1]")
	WebElement enterCity;  //*[@id=\"noanim-tab-example-pane-1\"]/div/div[1]/div/div/input           
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"refCollegeCityName\"]/div/div[1]/div/ul/li[1]")
	WebElement selectCity;
	
	@FindBy(how = How.XPATH, using = "//div[@class='css-bg1rzq-control']//div[@class='css-1hwfws3']")
	WebElement schoolDropDown;        
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"react-select-2-option-0\"]")
	WebElement selectSchool;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Save and Continue')]")
	WebElement savenContinueButton;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"firstName\"]")
	WebElement shippingFirstName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='lastName']")
	WebElement shippingLastName;
	
	@FindBy(how = How.XPATH, using = "//input[@id='address1']")
	WebElement shippingAddress;
	
	@FindBy(how = How.XPATH, using = "//input[@id='city']")
	WebElement shippingCity;
	
	@FindBy(how = How.XPATH, using = "//div[@id='drpState']/label[text()='State']/following-sibling::div/div/div/div[2]/div[2]")
	WebElement shippingStateButton;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"react-select-2-option-36\"]")
	WebElement shippingState;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"drpState\"]/div/div/div/div[2]/div[1]")
	WebElement removeShippingState;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"zip\"]")
	WebElement shippingZip;
	
	@FindBy(how = How.XPATH, using = "//input[@id='phone']")
	WebElement shippingPhone;
	
	@FindBy(how = How.CSS, using = "//label.shippingMethods0:before")
	WebElement upsgRadioOption;
	
	@FindBy(how = How.XPATH, using = "//button[@id='ProfileAddressSubmitButton']")
	public WebElement submitButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='buttonlabel panelEdit']")
	WebElement editShippingButton;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"shippingCarrier\"]/div/span[2]")
	WebElement shippingMethodErr;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"shippingCarrier\"]/div/div/div[1]/span/label")
	WebElement freeShippingText;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"__next\"]/div/div/div/div/div[2]/div/div/div[3]/div[1]/div[3]/div[4]/div/div[2]")
	WebElement shippingTaxValue;           
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"shippingCarrier\"]/div/div/div[2]/span/label")
	WebElement UPShippingText;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"__next\"]/div/div/div/div/div[2]/div/div/div[3]/div[1]/div[3]/div[4]/div/div[2]")
	WebElement UPSshippingTaxValue;  
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"shippingCarrier\"]/div/div/div[1]/span/label/span")
	WebElement strikeOutShippingPrice;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"shippingCarrier\"]/div/div/div[1]/span/label/b")
	WebElement discountedShippingPrice;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"zipCodeParent\"]/div/span")
	WebElement shippingZipError;
	
	@FindBy(how = How.XPATH, using = "//h4[@class='modal-title' and (text()='Verify Your Shipping Address')]")
	WebElement shippingAddressModal;
	
	@FindBy(how = How.XPATH, using = "//input[@id='UPSShippingInput0']")
	public WebElement selectShippingAddress_Modal;
	
	@FindBy(how = How.XPATH, using = "//button[@id='UPSShippingSaveAndContinue']")
	public WebElement UPSShippingSaveAndContinue_Modal;
	
	@FindBy(xpath = "//div[@class='OrderConfirm' and text()='Your Exam Copy Request Has Been Submitted']")
	WebElement message_SuccessfullExamCopy;
	
	
	// Initializing Web Driver and PageFactory.
	
	public CartShipping() {
		this.driver = BaseDriver.getDriver();
		PageFactory.initElements(driver, this);
	}


	
	@Step("Select a Student option,  Method: {method} ")
	public void studentInfo() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		boolean studentPanelExist = false;

		try 
		{

			studentPanelExist = studentPanel.isDisplayed();

		}
		
		catch (NoSuchElementException e) 
		{

			studentPanelExist = false;
			
		}

		if(studentPanelExist) 
		{
		
			wait.until(ExpectedConditions.visibilityOf(radioButtonStudent));
			radioButtonStudent.click();
		
			wait.until(ExpectedConditions.visibilityOf(savenContinueButton));
			savenContinueButton.click();
		}
		
		else
		{
			
		}
		
	}
	
	
	@Step("Select Not a Student option,  Method: {method} ")
	public void selectNotStudentOption() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 25);

		boolean studentPanelExist = false;

		try 
		{
			WebDriverWait innerwait = new WebDriverWait(driver,5);
			TimeUnit.SECONDS.sleep(5);
			studentPanelExist = studentPanel.isDisplayed();

		}
		
		catch (NoSuchElementException e) 
		{

			studentPanelExist = false;
			
		}

		if(studentPanelExist) 
		{
		
			wait.until(ExpectedConditions.visibilityOf(radioButtonNotStudent));
			radioButtonNotStudent.click();
		
			wait.until(ExpectedConditions.visibilityOf(savenContinueButton));
			savenContinueButton.click();
		
		}
		
		else 
		{
			
		}
	}
	
	
	@Step("Verify the Policy links are exist,  Method: {method} ")
	public boolean policyLinks() throws Exception {
		
		boolean PolicyLinkOneExist;
		boolean PolicyLinkTwoExist;
		
		Thread.sleep(3000);
		
		try 
		{
			PolicyLinkOneExist = policyLinkOne.isDisplayed();
			PolicyLinkTwoExist = policyLinkTwo.isDisplayed();
			
			if(PolicyLinkOneExist && PolicyLinkTwoExist)
			{
				
				return true;
				
			}
			
		}
		
		catch (NoSuchElementException e) {
			
			throw new NoSuchElementException("Error: Policy Links are NOT displayed " + e.getMessage());
			
		}
		
		return false;
		
	}
	
	
	@Step("Student Information Exist,  Method: {method} ")
	public boolean studentInfoExist() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);	

		boolean studentPanelExist = false;

		try 
		{

			studentPanelExist = studentPanel.isDisplayed();

		}
		
		catch (NoSuchElementException e) 
		{

			studentPanelExist = false;
			//throw new Exception ("Student Panel is NOT displayed");
			
		}

		if(studentPanelExist) 
		{

			wait.until(ExpectedConditions.visibilityOf(radioButtonStudent));
			radioButtonStudent.click();
			
			//wait.until(ExpectedConditions.visibilityOf(enterCity));
			Thread.sleep(3000);
			enterCity.click();
			
			Thread.sleep(3000);
			enterCity.sendKeys("NJ");
			
			wait.until(ExpectedConditions.visibilityOf(selectCity));
			selectCity.click();
			
			wait.until(ExpectedConditions.visibilityOf(schoolDropDown));
			schoolDropDown.click();
			
			wait.until(ExpectedConditions.visibilityOf(selectSchool));
			selectSchool.click();
			
			//wait.until(ExpectedConditions.visibilityOf(savenContinueButton));
			Thread.sleep(3000);
			savenContinueButton.click();

		}
		
		return studentPanelExist;


	}
	
	
	@Step("Shipping Information Exist,  Method: {method} ")
	public void shippingInfo() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);	

		boolean studentPanelExist = false;

		try 
		{

			studentPanelExist = studentPanel.isDisplayed();

		}
		catch (NoSuchElementException e) 
		{

			studentPanelExist = false;
		}
		catch (Exception e) 
		{

			e.printStackTrace();
		}

		if(studentPanelExist) 
		{

			wait.until(ExpectedConditions.visibilityOf(radioButtonNotStudent));
			Thread.sleep(3000);
			radioButtonNotStudent.click();

			wait.until(ExpectedConditions.visibilityOf(savenContinueButton));
			savenContinueButton.click();
		
		}
		
		Thread.sleep(3000);
		String shippingName = shippingFirstName.getAttribute("value");
		
		if (shippingName.isEmpty() == false) 
		{ 

			//wait.until(ExpectedConditions.visibilityOf(radioButtonNotStudent));
			Thread.sleep(3000);
			
			wait.until(ExpectedConditions.visibilityOf(upsgRadioOption)); 
			upsgRadioOption.click();
		}
		
		else 
		{

			wait.until(ExpectedConditions.visibilityOf(shippingFirstName));
			shippingFirstName.sendKeys();

			shippingLastName.sendKeys();
			shippingAddress.sendKeys();
			shippingCity.sendKeys();

			shippingStateButton.click();
			Thread.sleep(2000);
			WebElement stateOption;
			String stateName;
			for(int i=0; i<51; i++)
			{
				//stateOption = driver.findElement(By.xpath("//*[@id=\"react-select-2-option-" + i + "\"]"));
				stateOption = driver.findElement(By.id("react-select-2-option-" + i));
				stateName = stateOption.getText();
				if(stateName.equals("ShippingInformation"))
				{
					stateOption.click();
					break;
				}
			}						

			//wait.until(ExpectedConditions.visibilityOf(shippingZip));
			String zip = "08817";
			int zipAsInt = Integer.parseInt(zip);
			Thread.sleep(3000);
			shippingZip.sendKeys(String.valueOf(zipAsInt));

			Thread.sleep(3000);
			shippingPhone.click();

			int[] phoneIntArray = {6, 0, 9, 7, 7, 5, 7, 6, 7, 4};
			JSONArray phoneArray = new JSONArray(phoneIntArray);

			for(int i=0; i<phoneArray.length(); i++) {

				String PhoneNumber = phoneArray.get(i).toString();
				shippingPhone.sendKeys(PhoneNumber);

			}
		}
	}
	
	public void enterShippingInfo(String fName, String lName, String prmAdd, String City, String State, String Zip) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,25);
		wait.until(ExpectedConditions.visibilityOf(shippingFirstName));
		String shippingName = shippingFirstName.getAttribute("value");
		ReusableMethods.waitForPageLoad(driver);
		
		if (shippingName.isEmpty() == false) {

			clearShippingFieldValues(shippingFirstName);
			shippingFirstName.sendKeys(fName);
			clearShippingFieldValues(shippingLastName);
			shippingLastName.sendKeys(lName);
			clearShippingFieldValues(shippingAddress);
			shippingAddress.sendKeys(prmAdd);
			clearShippingFieldValues(shippingCity);
			shippingCity.sendKeys(City);

			shippingStateButton.click();
			Thread.sleep(2000);
			WebElement stateOption;
			String stateName;
			for(int i=0; i<51; i++)
			{
				stateOption = driver.findElement(By.id("react-select-2-option-" + i));
				stateName = stateOption.getText();
				if(stateName.equals(State))
				{
					stateOption.click();
					break;
				}
			}						

			wait.until(ExpectedConditions.visibilityOf(shippingZip));
			int zipAsInt = Integer.parseInt(Zip);
			Thread.sleep(1000);
			clearShippingFieldValues(shippingZip);
			shippingZip.sendKeys(String.valueOf(zipAsInt));
		}
		
		else {
				shippingFirstName.sendKeys(fName);
				shippingLastName.sendKeys(lName);
				shippingAddress.sendKeys(prmAdd);
				shippingCity.sendKeys(City);
				//Thread.sleep(15000);
				//((JavascriptExecutor) driver).executeScript("arguments[0].click();", shippingStateButton);
				shippingStateButton.click();
				Thread.sleep(2000);
				WebElement stateOption;
				String stateName;
				for(int i=0; i<51; i++)
				{
					stateOption = driver.findElement(By.id("react-select-2-option-" + i));
					stateName = stateOption.getText();
					if(stateName.equals(State))
					{
						stateOption.click();
						break;
					}
				}						

				wait.until(ExpectedConditions.visibilityOf(shippingZip));
				int zipAsInt = Integer.parseInt(Zip);
				Thread.sleep(1000);
				shippingZip.sendKeys(String.valueOf(zipAsInt));

				Thread.sleep(1000);
				shippingPhone.click();

				int[] phoneIntArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
				JSONArray phoneArray = new JSONArray(phoneIntArray);

				for(int i=0; i<phoneArray.length(); i++) {

					String PhoneNumber = phoneArray.get(i).toString();
					Thread.sleep(1000);
					shippingPhone.sendKeys(PhoneNumber);
				}
		}
		
	}
	
	
	public void clearShippingFieldValues(WebElement ele) throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOf(ele));
		String fieldLen = ele.getAttribute("value");
		int lengthText = fieldLen.length();

		for(int i = 0; i < lengthText; i++){
			ele.sendKeys(Keys.ARROW_LEFT);
			ele.sendKeys(Keys.DELETE);
		}
	}

	
	@Step("Select Shipping Method,  Method: {method} ")
	public void shippingMethod() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver, 5);
		if(upsgRadioOption.isSelected() == true) {
			wait.until(ExpectedConditions.visibilityOf(submitButton));
			submitButton.click();
		} 
		else {
			
				wait.until(ExpectedConditions.visibilityOf(upsgRadioOption));
				upsgRadioOption.click();
			
				wait.until(ExpectedConditions.visibilityOf(submitButton));
				submitButton.click();
			}
	
		}
	
		
		@Step("Submit Shipping Method,  Method: {method} ")
		public void submitShippingMethod() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(submitButton));
			Thread.sleep(3000);
			submitButton.click();

		}


		@Step("Verify Shipping Method Error,  Method: {method} ")
		public String shippingMethodErr() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(shippingMethodErr));
			
			return shippingMethodErr.getText();

		}
		
		
		@Step("Free Shipping option Eixst and Selected ?, Method: {method} ")
		public boolean[] freeShippingExist(String locator) throws Exception {
			
			boolean[] smValueArray = new boolean[2];
			
			try 
			{	
				Thread.sleep(3000);
				smValueArray[0] = freeShippingText.isDisplayed();
				
				Thread.sleep(3000);
				smValueArray[1] = freeShippingText.isSelected();
			
				if(smValueArray[0])
				{
					if(smValueArray[1])
					{
						smValueArray[1] = true;
					}
					
					else
					{
						smValueArray[1] = false;
						
					}
				}
			
		    } catch (NullPointerException | NoSuchElementException e) {
		        System.err.println("Unable to locate Free Shipping Method '" + locator + "'");
		        
		    } catch (Exception e) {
		        System.err.println("Unable to check display status of locator '" + locator + "'");
		        e.printStackTrace();
		    }
			
			return smValueArray;
			
		}
		
		
		@Step("Get the list of Shipping Methods,  Method: {method} ")
		public String[] shippingMethodList(String locator) throws Exception {
		
		String[] smTextArray = new String[5];
		
		try 
		{
			//Get all web elements into list					
			//List<WebElement> smList=driver.findElements(By.xpath("//*[@id=\"shippingCarrier\"]/div/div"));
			
			WebElement smLabel;
			
			for(int i=1; i<5; i++)
			{

				smLabel = driver.findElement(By.xpath("//*[@id=\"shippingCarrier\"]/div/div/div[" + i + "]/span/label"));
				smTextArray[i] = smLabel.getText();
			}
		}
		
		catch (IndexOutOfBoundsException e) {
			
			System.err.println("Unexpected Error occurred" + locator + "'");
			
		}
	    
		return smTextArray;
		
	}
		
		
		@Step("Verify Shipping Methods selected by default,  Method: {method} ")
		public boolean[] shippingMethodListSelected(String locator) throws Exception {
		
			boolean[] smBooleanArray = new boolean[4];
		
		try 
		{
			//Get all web elements into list					
			//List<WebElement> smList=driver.findElements(By.xpath("//*[@id=\"shippingCarrier\"]/div/div"));
			
			WebElement smLabel;
			
			int i;
			
			for(i=1; i<4; i++)
			{

				smLabel = driver.findElement(By.xpath("//*[@id=\"shippingCarrier\"]/div/div/div[" + i + "]/span/label"));
				smBooleanArray[i] = smLabel.isSelected();
			}
		}
		
			catch (IndexOutOfBoundsException e) {
			
				System.err.println("Unexpected Error occurred" + locator + "'");
			
			}
		
		return smBooleanArray;
		
	}
		
		
		@Step("Get Free Shipping Tax value,  Method: {method} ")
		public String getFreeShippingTaxValue() throws Exception {
			
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(shippingTaxValue));
			
			return shippingTaxValue.getText();
			
		}
		
		
		@Step("Get UPS Shipping Tax value,  Method: {method} ")
		public String getUPSShippingTaxValue() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(UPShippingText));
			UPShippingText.click();
			
			wait.until(ExpectedConditions.visibilityOf(submitButton));
			submitButton.click();
			
			//wait.until(ExpectedConditions.visibilityOf(UPSshippingTaxValue));
			Thread.sleep(3000);
			return UPSshippingTaxValue.getText();
			
		}
		
		
		@Step("Return Free Shipping Tax value when Free Shipping selected,  Method: {method} ")
		public String returnFreeShippingTaxValue() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(freeShippingText));
			freeShippingText.click();
			
			wait.until(ExpectedConditions.visibilityOf(submitButton));
			submitButton.click();
			
			Thread.sleep(3000);
			return shippingTaxValue.getText();
			
		}
		
		
		@Step("Validate StrikeOut and Discounted Shipping prices,  Method: {method} ")
		public String[] getDiscountedShipping() throws Exception {
			
			boolean discountedShippingExist = false;
			boolean strikeOutShippingExist = false;
			
			String[] shippingPriceArray = new String[2];
			
			try 
			{
				strikeOutShippingExist = strikeOutShippingPrice.isDisplayed();
				discountedShippingExist = discountedShippingPrice.isDisplayed();

			}
			catch (NoSuchElementException e) 
			{

				strikeOutShippingExist = false; 
				discountedShippingExist = false;
				
			}
			catch (Exception e) 
			{

				e.printStackTrace();
			}

			if(strikeOutShippingExist && discountedShippingExist) 
			{
				WebDriverWait wait = new WebDriverWait(driver,3);
				TimeUnit.SECONDS.sleep(3);
				
				wait.until(ExpectedConditions.visibilityOf(strikeOutShippingPrice));
				shippingPriceArray[0] = strikeOutShippingPrice.getText();

				wait.until(ExpectedConditions.visibilityOf(discountedShippingPrice));
				shippingPriceArray[1] = discountedShippingPrice.getText();
		
			}
			
			return shippingPriceArray;
		}
		
		
		@Step("Update Shipping Information: {method} ")
		public void updateShipping() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(editShippingButton));
			editShippingButton.click();
			
		}
		
		
		@Step("Update Shipping State and Zip Code: {method} ")
		public void updateZipCode() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(shippingCity));
			String shippingCityLen = shippingCity.getAttribute("value");
			int lengthText = shippingCityLen.length();

			for(int i = 0; i < lengthText; i++){
				shippingCity.sendKeys(Keys.ARROW_LEFT);
				shippingCity.sendKeys(Keys.DELETE);
			}
			
			shippingCity.sendKeys();
			
			String JsonStateName;
			JsonStateName = "NJ";
			
//			wait.until(ExpectedConditions.visibilityOf(removeShippingState));
//			removeShippingState.click();
			
			Thread.sleep(3000);
			shippingStateButton.click();
			
			//stateOption = driver.findElements(By.className("css-fk865s-option"));
			
//			WebElement stateName;
//
//			for(int i=0; i <57; i++)
//			 { 
//				Thread.sleep(3000);
//				stateName = driver.findElement(By.id("react-select-2-option-" + i)); 
//				
//				 if(JsonStateName == stateName.getText()) 
//				 {
//					 stateName.click();
//					 break;
//				 }
//					 
//			 }
				
				//list of all State options
				List<WebElement> selectStateValues=driver.findElements(By.id("react-select-2-input"));
				//String[] noOfStateArray = new String selectStateValues.size();
				
				System.out.println(selectStateValues.size());
				
				for(int i=0; i <=selectStateValues.size(); i++)
				 { 
					WebElement stateValue = selectStateValues.get(i);
					System.out.println(stateValue);
				
					if(JsonStateName == stateValue.getText()) 
					{
						stateValue.click();
						break;				
					}

				 }
			
			wait.until(ExpectedConditions.visibilityOf(shippingZip));
			String zipLen = shippingZip.getAttribute("value");
			int lenText = zipLen.length();

			for(int i = 0; i < lenText; i++){
				shippingZip.sendKeys(Keys.ARROW_LEFT);
				shippingZip.sendKeys(Keys.DELETE);
			}
			
			String zip = "08817";
			int zipAsInt = Integer.parseInt(zip);

			shippingZip.sendKeys(String.valueOf(zipAsInt));
			
		}
		
		
		@Step("Remove Shipping ZipCode,  Method: {method} ")
		public void removeShippingZipCode() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(shippingZip));
			String zipLen = shippingZip.getAttribute("value");
			int lenText = zipLen.length();

			for(int i = 0; i < lenText; i++){
				shippingZip.sendKeys(Keys.ARROW_LEFT);
				shippingZip.sendKeys(Keys.DELETE);
			}
		}
			
		@Step("Re-enter Shipping ZipCode2,  Method: {method} ")	
		public void enterShippingZipCode2() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(shippingZip));
			String zip = "08817";
			int zipAsInt = Integer.parseInt(zip);
			shippingZip.sendKeys(String.valueOf(zipAsInt));
			
			Actions act = new Actions(driver);
			wait.until(ExpectedConditions.visibilityOf(shippingPhone));
			act.moveToElement(shippingPhone).click().build().perform();
			
		}
		
		
		@Step("Re-enter Shipping ZipCode,  Method: {method} ")	
		public String shippingZipCodeErr() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(shippingZipError));
			return shippingZipError.getText();
			
		}
		
		@Step("Re-enter Shipping ZipCode1,  Method: {method} ")	
		public void enterShippingZipCode1() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver,3);
			TimeUnit.SECONDS.sleep(3);
			
			wait.until(ExpectedConditions.visibilityOf(shippingZip));
			String zip = "08817";
			int zipAsInt = Integer.parseInt(zip);
			shippingZip.sendKeys(String.valueOf(zipAsInt));
			
			Actions act = new Actions(driver);
			wait.until(ExpectedConditions.visibilityOf(shippingPhone));
			act.moveToElement(shippingPhone).click().build().perform();
			
		}
		
		
		@Step("Check Verify Shipping Address modal,  Method: {method} ")	
		public boolean checkShippingAddressModalExist() throws Exception {
			
			WebDriverWait wait = new WebDriverWait(driver, 25);
			
			boolean shippingAddressModalExist = false;
			try {
					wait.until(ExpectedConditions.visibilityOf(shippingAddressModal));
					shippingAddressModalExist = shippingAddressModal.isDisplayed();
			} catch(Exception e) {
				return  false;
			}
			
			return shippingAddressModalExist;
			
		}
	
		@Step("Get the successful exam copy request submission message,   Method: {method}")
		public String getSuccessfulExamCopySubmission() {

			WebDriverWait wait = new WebDriverWait(driver, 25);
			wait.until(ExpectedConditions.visibilityOf(message_SuccessfullExamCopy));

			String successExamCopyMsg = (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].innerText;",message_SuccessfullExamCopy);
			return successExamCopyMsg;

		}
		
}
