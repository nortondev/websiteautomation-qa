package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;

import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;


import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifySearchBookCriteria extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Search Book Criteria.")
	@Stories("AS-170 - Verify Search Book Criteria.")
	@Test
	
	public void validateSearchBookCriteria() throws Exception {
		
		String searchKey1 = "Frozen";
		String searchKey2 = "Yoga";
		String searchKey3 = "qwerty";
		String authorName = "Clark Spencer Larsen";
		
		hp = new HomePage();
		hp.searchBook(searchKey1);
		Thread.sleep(2000);
		Assert.assertEquals(hp.validateSearchBookText(searchKey1), true);
		Thread.sleep(2000);
		hp.clearResultsButton.click();
		
		Thread.sleep(2000);
		hp.searchBook(searchKey2);
		Thread.sleep(2000);
		Assert.assertEquals(hp.validateSearchBookText(searchKey2), true);
		Thread.sleep(2000);
		hp.clearResultsButton.click();
		
		Thread.sleep(2000);
		hp.searchBook(searchKey3);
		Thread.sleep(2000);
		Assert.assertTrue(hp.getNoResultsText().contains(" No Results for"));
		Thread.sleep(2000);
		hp.clearResultsButton.click();
		Thread.sleep(2000);
		hp.searchAuthor(authorName);
		Thread.sleep(2000);
		Assert.assertEquals(authorName, hp.getAuthorNameInBookResults(authorName));
		Thread.sleep(2000);
		
	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
