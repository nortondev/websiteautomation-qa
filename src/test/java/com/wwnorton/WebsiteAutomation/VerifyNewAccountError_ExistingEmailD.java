package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyNewAccountError_ExistingEmailD extends PropertiesFile {

	CreateNewAccount CNA;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify new account error with Existing Email address.")
	@Stories("AS-139 - Verify new account error with Existing Email address.")
	@Test
	
	public void VerifyNewAccountError() throws Exception {

		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String existingAccountEmailID = (String) gContext.getAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1);
		
		CNA = new CreateNewAccount();
		CNA.clickOnLogin();
		
		Thread.sleep(2000);
		CNA.enterNewAccountEmail(existingAccountEmailID);
		CNA.clickOnCreateAccountButton();
		
		Thread.sleep(2000);
		String emailAlreadyExistMsg = CNA.getEmailAlreadyExistMsg();
		Assert.assertEquals("Email already exists", emailAlreadyExistMsg);

	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
