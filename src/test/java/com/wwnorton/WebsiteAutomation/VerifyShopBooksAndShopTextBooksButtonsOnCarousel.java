package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyShopBooksAndShopTextBooksButtonsOnCarousel extends PropertiesFile {

	HomePage hp;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify ShopBooks and Shop TextBooks Buttons OnCarousel.")
	@Stories("AS-158 - Verify ShopBooks and Shop TextBooks Buttons OnCarousel.")
	@Test
	public void VerifyShopBooksAndShopTextBooksButtons() throws Exception {

		driver = getDriver();
		
		hp = new HomePage();
		Assert.assertEquals(hp.checkShopBookButton(), true);
		Thread.sleep(2000);
		Assert.assertEquals(hp.checkShopTextBookButton(), true);
		Thread.sleep(2000);
		hp.clickOnShopBooksButton();
		Thread.sleep(2000);
		hp.clickOnSeagull();
		Thread.sleep(2000);
		hp.clickOnShopTextBooksButton();
		Thread.sleep(2000);
		hp.clickOnSeagull();
		Thread.sleep(2000);
		ReusableMethods.scrollIntoView(driver, hp.bookCarouselSection);
		hp.browseBookCarousel_Next();
		hp.browseBookCarousel_Previous();

	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
