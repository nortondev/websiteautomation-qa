package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.BookDetailsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class SearchAuthorUsingAuthorName extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	CreateNewAccount CNA;
	LoginPage lp;
	BookSearchResultsPage bookResultsPage;
	BookDetailsPage bdp;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Search author using author name")
	@Stories("AS-169 - Search author using author name")
	@Test

	public void VerifyAuthorsName() throws Exception {

		driver = getDriver();
		String authorName1 = "Suzy Pepper Rollins";
		Thread.sleep(2000);
		hp = new HomePage();
		hp.searchAuthor(authorName1);
		Thread.sleep(2000);
		bookResultsPage = new BookSearchResultsPage();
		Assert.assertEquals("Suzy Pepper Rollins", bookResultsPage.getAuthorNameInBookResults1()); 
		bookResultsPage.clickOnTheBookResult1();	
		bdp = new BookDetailsPage();
		bdp.checkBookDetails();
		String authorName2 = "Barbara Russano Hanning";
		hp.searchAuthor(authorName2);
		Thread.sleep(2000);
		Assert.assertEquals("Barbara Russano Hanning", bookResultsPage.getAuthorNameInBookResults2()); 
		bookResultsPage.clickOnTheBookResult2();
		bdp.checkBookDetails();
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
