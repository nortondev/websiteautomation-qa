package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.APWorkshopPage;
import com.wwnorton.WebsiteAutomation.objectFactory.ContactUsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class VerifyWebsiteFeedbackForm extends PropertiesFile {

	public static String urlLink;

	HomePage hp;
	ContactUsPage feedback;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify website feedback form")
	@Stories("AS-191 - Verify Contact Us-Feedback Form Submission.")
	@Test

	public void ContactUsFeedbackForm() throws Exception {

		driver = getDriver();

		ReusableMethods.scrollToBottom(driver);
		hp = new HomePage();
		hp.QAclickOnContactUs();

		feedback = new ContactUsPage();
		Thread.sleep(2000);
		feedback.clickOnFeedbackLink();
		ReusableMethods.scrollToBottom(driver);
		feedback.clickOnFeedbackFormButton();

		// Fill details in website feedback overlay

		Thread.sleep(2000);
		String fName = "Test Pls Disregard";
		String emailId = "test@test.com";
		String comment = "This is the test by WWN QA team , Please disregard";
		feedback.submitFeedbackForm(fName, emailId, comment);
		Thread.sleep(3000);
		Assert.assertEquals(feedback.checkFeedbackConfirmationMessage(), true);
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
