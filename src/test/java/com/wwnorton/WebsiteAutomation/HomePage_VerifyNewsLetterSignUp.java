package com.wwnorton.WebsiteAutomation;


import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class HomePage_VerifyNewsLetterSignUp extends PropertiesFile {

	HomePage hp;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	//Date Time Stamp
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Signup for News Letters.")
	@Stories("AS-136 - Verify Signup for News Letter.")
	@Test
	public void VerifyNewsLetterSignUp() throws Exception {

		driver = getDriver();
		
		//SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		//String existingAccountEmailID = (String) gContext.getAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1);
		
		hp = new HomePage();
		ReusableMethods.scrollToBottom(driver);
		String existingAccountEmailID = "WebsiteNewUserName1@mailinator.com";
		String successMessage = hp.QAsendEmailForNewsLetter(existingAccountEmailID);
		Assert.assertEquals(successMessage, "Thanks for signing up!");
		System.out.println("test");
		Thread.sleep(3000);
		String incorrectEmailID = "abcd";
		String errorMessage = hp.ValidateErrorForIncorrectEmail_NewsLetter(incorrectEmailID);
		Thread.sleep(2000);
		Assert.assertEquals(errorMessage, "Please enter a valid email address");

	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
