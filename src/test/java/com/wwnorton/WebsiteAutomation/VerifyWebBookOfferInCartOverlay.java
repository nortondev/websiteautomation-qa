
package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.AccountDashBoardPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookDetailsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CartItemDetails;
import com.wwnorton.WebsiteAutomation.objectFactory.CartPayment;
import com.wwnorton.WebsiteAutomation.objectFactory.CartShipping;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.objectFactory.OrderConfirmationPage;
import com.wwnorton.WebsiteAutomation.objectFactory.cartLayout;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyWebBookOfferInCartOverlay extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	CreateNewAccount CNA;
	LoginPage lp;
	BookSearchResultsPage bookResultsPage;
	BookDetailsPage bdp;
	cartLayout cLayOut;
	CartItemDetails cid;
	CartShipping cs;
	CartPayment cp;
	OrderConfirmationPage ocp;
	AccountDashBoardPage adp;

	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Web Book Offer In Cart Overlay.")
	@Stories("AS-192 - Verify Web Book Offer In Cart Overlay.")
	@Test
	
	public void VerifyWebBookOffer() throws Exception {

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String existingAccountEmailID = (String) gContext.getAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1);
		
		//String existingAccountEmailID = "QASept302021@maildrop.cc";
		
		driver = getDriver();
		String bookName = "Norton Chaucer";

		CNA = new CreateNewAccount();
		CNA.clickOnLogin();
		
		Thread.sleep(2000);
		String pswd = "Password@123";
		
		lp = new LoginPage();
		lp.loginWithExistingAccount(existingAccountEmailID, pswd);
		lp.closeAccountLoginImg();
		
		Thread.sleep(2000);
		hp = new HomePage();
		hp.searchBook(bookName);
		
		Thread.sleep(2000);
		bookResultsPage = new BookSearchResultsPage();
		bookResultsPage.selectBook(bookName);
		
		Thread.sleep(2000);
		bdp = new BookDetailsPage();
		/*ReusableMethods.scrollIntoView(driver, bdp.bookFormatLayout); 
		String headerOption_Book = "Loose Leaf";
		Thread.sleep(2000);
		bdp.clickBookBinding(headerOption_Book);
		Thread.sleep(2000);*/
		bdp.addToCart();
		Thread.sleep(2000);
		cLayOut = new cartLayout();
		Assert.assertEquals(cLayOut.addWebOffer(), true);
		
		Thread.sleep(2000);
		Assert.assertEquals(cLayOut.webOfferPromoTextExist(), "Promo Applied");
		cLayOut.clickCheckout();
		
		Thread.sleep(2000);
		cid = new CartItemDetails();
		ReusableMethods.scrollIntoView(driver, cid.yourCartItemsEle);
		String cartTotal = cid.getTotalCartAmount();
		cid.clickRemove_PromoBook("The Canterbury Tales Handbook");
		Thread.sleep(2000);
		String updatedCartTotal_Remove = cid.getTotalCartAmount();
		Assert.assertEquals(cartTotal.equals(updatedCartTotal_Remove), false);
		Thread.sleep(2000);
		Assert.assertEquals(cLayOut.addWebOffer(), true);
		Thread.sleep(2000);
		String updatedCartTotal_Add = cid.getTotalCartAmount();
		Assert.assertEquals(cartTotal.equals(updatedCartTotal_Add), true);
		Thread.sleep(2000);
		cs = new CartShipping();
		cs.selectNotStudentOption();
		Thread.sleep(2000);
		String  fName = "Test";
		String  lName = "Account";
		String  prmAddress = "45 COUNTY ROAD 13";
		String  city = "CLANTON";
		String state = "Alabama";
		String  zip = "35045";
		cs.enterShippingInfo(fName, lName, prmAddress, city, state, zip);
		Thread.sleep(2000);
		//cs.shippingMethod();
		cs.submitButton.click();
		Thread.sleep(2000);
		if(cs.checkShippingAddressModalExist() == true) {
			cs.selectShippingAddress_Modal.click();
			cs.UPSShippingSaveAndContinue_Modal.click();
			Thread.sleep(3000);
			//cs.shippingMethod();
			cs.submitButton.click();
		}
		
		Thread.sleep(2000);
		String cHName = "Test Account"; 
		String expMonth = "December";
		String expYear = "2023"; 
		String secCode = "123";
		
		cp = new CartPayment();
		cp.enterPaymentInfo(cHName, expMonth, expYear, secCode);
		
		Thread.sleep(2000);
		cp.checkBillingAddress();
		Thread.sleep(2000);
		cp.checkTnC();
		Thread.sleep(2000);
		cp.placeOrder();
		
		Thread.sleep(2000);
		ocp = new OrderConfirmationPage();
		String OrderNumber_OrderConfirmation = ocp.orderDetails();
		Thread.sleep(2000);
		hp = new HomePage();
		hp.clickOnSeagull_CheckOut();
		Thread.sleep(2000);
		lp = new LoginPage();
		lp.clickAccountLink();
		Thread.sleep(2000);
		lp.clickOrdersLink();
		Thread.sleep(2000);
/*		adp = new AccountDashBoardPage();
		adp.clickAccountDetailLink();
		Thread.sleep(2000);
		String OrderNumber_OrderDetails = adp.getOrderNumber();
		String[] arrSplit = OrderNumber_OrderDetails.split("#");
		Assert.assertTrue(OrderNumber_OrderConfirmation.equals(arrSplit[1]));
*/
		Thread.sleep(2000);
		lp.clickAccountLink();
		Thread.sleep(2000);
		lp.checkLogOutExist();
	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
