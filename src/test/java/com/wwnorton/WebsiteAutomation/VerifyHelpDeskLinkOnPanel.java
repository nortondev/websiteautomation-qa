package com.wwnorton.WebsiteAutomation;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.FindMyRepPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyHelpDeskLinkOnPanel extends PropertiesFile {
	
	public static String urlLink;
	HomePage hp;
	LoginPage lp;
	CreateNewAccount CNA;
	FindMyRepPage fyr;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify help desk links present on every panel .")
	@Stories("AS-157 - Verify help desk links present on every panel.")
	@Test
	public void VerifyHelpDeskLink() throws Exception {

		driver = getDriver();
		urlLink = PropertiesFile.url;
		String readerLinkURL = urlLink + "help";
		
		hp = new HomePage();
		CNA = new CreateNewAccount();
		CNA.clickOnLogin();
		Thread.sleep(2000);
		CNA.clickHelpDeskLink();
		String parentWindow = driver.getWindowHandle();
		String helpDeskURL_CreateAccount = ReusableMethods.getNewWindowTab(parentWindow);
		Thread.sleep(2000);
		Assert.assertEquals(readerLinkURL, helpDeskURL_CreateAccount);
		
		Thread.sleep(2000);
		lp = new LoginPage();
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", lp.loginClose);
		Thread.sleep(2000);
		hp.QAclickOnEducator();
		fyr = new FindMyRepPage();
		Thread.sleep(2000);
		fyr.clickOnFindMyRepButton();
		fyr.clickHelpDeskLink_FindMyRep();
		String helpDeskURL_FindMyRep = ReusableMethods.getNewWindowTab(parentWindow);
		Thread.sleep(2000);
		Assert.assertEquals(readerLinkURL, helpDeskURL_FindMyRep);
		Thread.sleep(2000);
		fyr.clickClose_FindMyRepOverlay();

	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
