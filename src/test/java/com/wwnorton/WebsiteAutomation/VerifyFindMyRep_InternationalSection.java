package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.AccountDashBoardPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookDetailsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CartPayment;
import com.wwnorton.WebsiteAutomation.objectFactory.CartShipping;
import com.wwnorton.WebsiteAutomation.objectFactory.CatalogPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.FindMyRepPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.InstructorResourcePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.objectFactory.OrderConfirmationPage;
import com.wwnorton.WebsiteAutomation.objectFactory.SocialMediaSites;
import com.wwnorton.WebsiteAutomation.objectFactory.cartLayout;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class VerifyFindMyRep_InternationalSection extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	BookSearchResultsPage bookResultsPage;
	BookDetailsPage bdp;
	CatalogPage ca;
	cartLayout cLayOut;
	LoginPage lp;
	CartShipping cs;
	CartPayment cp;
	OrderConfirmationPage ocp;
	AccountDashBoardPage adp;
	InstructorResourcePage ins;
	FindMyRepPage fyr;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Find My Rep for International Representative")
	@Stories("AS-156 - Find My Rep for International Representative")
	@Test  

	public void VerifyFindMyInternationalRep() throws Exception {

		driver = getDriver();
		urlLink = PropertiesFile.url;
		String internationalRepLinkURL = urlLink + "find-your-rep/international-representatives";

		Thread.sleep(2000);
		hp = new HomePage();
		hp.QAclickOnEducator();

		// Send Find My Rep requests for International Representative

		fyr = new FindMyRepPage();
		fyr.clickOnFindMyRepButton();
		fyr.clickOnInternationalRep();
		Thread.sleep(2000);

		String parentWin = driver.getWindowHandle();

		String parentWindow = fyr.openInstructorRepWindow(parentWin);
		Assert.assertEquals(fyr.getInternationalRepMenu(), "International Representatives");
		Assert.assertEquals(internationalRepLinkURL, driver.getCurrentUrl());
		Thread.sleep(2000);
		fyr.FindYourRepBreadcrumb();
		driver.close();
		driver.switchTo().window(parentWindow);
		Thread.sleep(2000);
		fyr.clickClose_FindMyRepOverlay();

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
