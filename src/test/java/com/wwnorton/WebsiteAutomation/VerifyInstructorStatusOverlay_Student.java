package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.InstructorResourcePage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyInstructorStatusOverlay_Student extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	InstructorResourcePage irp;

	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Instructor Status Overlay for I am Student functionality.")
	@Stories("AS-182 - Verify Instructor Status Overlay for I am Student functionality.")
	@Test
	
	public void validateInstructorStatusOverlay_Student() throws Exception {

		hp = new HomePage();
		hp.QAclickOnEducator();
		Thread.sleep(2000);
		ReusableMethods.scrollIntoView(driver, hp.instructorResourcesSection);
		irp = new InstructorResourcePage();
		irp.clickViewInstructorResourcesButton();
		Thread.sleep(2000);
		irp.clickInstructorResourceButton(0);
		Thread.sleep(2000);
		irp.verifyRequestAccessButton();
		irp.clickRequestAccessButton_BDP();
		Thread.sleep(2000);
		Assert.assertEquals(irp.verifyInstructorResourceOverlay_Student(), true);
		Thread.sleep(2000);
		Assert.assertEquals(irp.verifyInstructorStatus_Student(), true);
		Thread.sleep(2000);
		
	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}