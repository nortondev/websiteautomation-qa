package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.BookDetailsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CartPayment;
import com.wwnorton.WebsiteAutomation.objectFactory.CartShipping;
import com.wwnorton.WebsiteAutomation.objectFactory.CatalogPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.objectFactory.OrderConfirmationPage;
import com.wwnorton.WebsiteAutomation.objectFactory.PromoPage;
import com.wwnorton.WebsiteAutomation.objectFactory.cartLayout;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class VerifyPromoCode extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	PromoPage pp;
	CatalogPage ca;
	BookSearchResultsPage bsr;
	BookDetailsPage bdp;
	cartLayout cLayOut;
	LoginPage lp;
	CartShipping cs;
	CartPayment cp;
	OrderConfirmationPage ocp;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Promo code in checkout page")
	@Stories("AS-165 - Verify Promo code in check-out page")
	@Test
	public void verifyPromoCode() throws Exception {

		driver = getDriver();
		SeleniumTestsContext gContext =
		SeleniumTestsContextManager.getGlobalContext();
		String existingAccountEmailID = (String)
		gContext.getAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1);

		//String existingAccountEmailID = "Test06-Dec@maildrop.cc";

		urlLink = PropertiesFile.url;
		driver.get(urlLink + "promo/IPNBPUZZLE");
		//driver.get(urlLink + "promo/YRSWORK21");
		
		pp = new PromoPage();

		pp.addBookInToCart();

		cLayOut = new cartLayout();
		Thread.sleep(2000);
		Assert.assertEquals(cLayOut.webOfferPromoTextExist(), "Promo Applied");
		cLayOut.clickCheckout();
		Thread.sleep(2000);
		String pswd = "Password@123";
		lp = new LoginPage();
		lp.loginWithExistingAccount(existingAccountEmailID, pswd);
		Thread.sleep(2000);

		// Remove promocode from check-out page

		pp.removePromoCode();
		pp.providePromoCode("IPNBPUZZLE");
		//pp.providePromoCode("YRSWORK21");
		pp.applyPromoCode();

		cLayOut = new cartLayout();
		Thread.sleep(2000);
		Assert.assertEquals(cLayOut.webOfferPromoTextExist(), "Promo Applied");

		// Fill the Shipping Information

		cs = new CartShipping();
		Thread.sleep(2000);
		String fName = "Test";
		String lName = "QAAccount";
		String prmAddress = "45 COUNTY ROAD 13";
		String city = "CLANTON";
		String state = "Alabama";
		String zip = "35045";
		cs.enterShippingInfo(fName, lName, prmAddress, city, state, zip);
		Thread.sleep(2000);
		cs.submitButton.click();

		Thread.sleep(2000);
		if (cs.checkShippingAddressModalExist() == true) {
			cs.selectShippingAddress_Modal.click();
			cs.UPSShippingSaveAndContinue_Modal.click();
			Thread.sleep(3000);
			cs.submitButton.click();
		}

		// Fill the Payment Information
		Thread.sleep(2000);
		String cHName = "Test Account";
		String expMonth = "December";
		String expYear = "2023";
		String secCode = "123";

		cp = new CartPayment();
		cp.enterPaymentInfo(cHName, expMonth, expYear, secCode);

		Thread.sleep(2000);

		// Check the billing address details

		Thread.sleep(2000);
		cp.checkBillingAddress();
		Thread.sleep(2000);

		// Accept the terms and conditions

		Thread.sleep(2000);
		cp.checkTnC();

		// Place the order

		cp.placeOrder();

		// Order successfully placed

		Thread.sleep(2000);
		ocp = new OrderConfirmationPage();

		ocp.clickOnLogOut();

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
