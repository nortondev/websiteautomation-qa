package com.wwnorton.WebsiteAutomation;


import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


import com.wwnorton.WebsiteAutomation.objectFactory.SocialMediaSites;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class HomePage_VerifySocialMediaSites extends PropertiesFile {

	SocialMediaSites SMS;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Social Media Sites in Norton Website.")
	@Stories("AS-137 - Verify Social Media Sites in Norton Website.")
	@Test
	
	public void VerifySocialMediaSites() throws Exception {

		driver = getDriver();
		
		SMS = new SocialMediaSites();
		String faceBookURL = SMS.clickOnFacebook();
		Assert.assertTrue(faceBookURL.contains("https://www.facebook.com/wwnorton/"));
		
		Thread.sleep(2000);
		String twitterURL = SMS.clickOnTwitter();
		Assert.assertTrue(twitterURL.contains("https://twitter.com/wwnorton"));
		
		Thread.sleep(2000);
		String instagramURL = SMS.clickOnInstagram();
		Assert.assertTrue(instagramURL.contains("https://www.instagram.com/w.w.norton/"));

	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
