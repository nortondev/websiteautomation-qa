package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.objectFactory.RegisterCode;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReadJsonFile;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

//import Utilities.ReadJsonFile;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class RedeemLegacyAccessCode extends PropertiesFile {
	
	public static String urlLink;
	RegisterCode RC;
	LoginPage lp;
	
	ReadJsonFile readJasonObject = new ReadJsonFile();
	JsonObject jsonObj = readJasonObject.readJason();
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Reddem Legacy Access Code.")
	@Stories("AS-194 - Reddem Legacy Access Code.")
	@Test
	public void ReddemLegacyCode() throws Exception {

								
		driver = getDriver();
		urlLink = PropertiesFile.url;
		
		//SS
		ReadJsonFile readJsonObject = new ReadJsonFile();
		JsonObject jsonobject = readJsonObject.readJason();
		String existingAccountEmailID = jsonobject.getAsJsonObject("ExistingAccountCredentials").get("accountEmail").getAsString();
		String pswd = jsonobject.getAsJsonObject("ExistingAccountCredentials").get("accountpw").getAsString();
		
		
		//String code = jsonObj.getAsJsonObject("LegacyAccessCodes").get("regCode").getAsString();
		String code = "GMRE-LWVK";
		String registerCodeLinkURL_Lower = urlLink + "reg-code";
		String registerCodeLinkURL_Prod = urlLink + "register-code";

		if(urlLink.equalsIgnoreCase("https://wwnorton.com/")) {
			driver.get(registerCodeLinkURL_Prod);
		} else if (urlLink.equalsIgnoreCase("https://auth-qa.wwnorton.net/")) {
			driver.get(registerCodeLinkURL_Lower);
		} else if(urlLink.equalsIgnoreCase("https://auth-stg.wwnorton.net/")) {
			driver.get(registerCodeLinkURL_Lower);
		} else if(urlLink.equalsIgnoreCase("https://ecp-stg.wwnorton.net/")) {
			//driver.get(registerCodeLinkURL_Prod);
			driver.get(registerCodeLinkURL_Lower);
		} else if(urlLink.equalsIgnoreCase("https://ecp-qa.wwnorton.net/")) {
			driver.get(registerCodeLinkURL_Lower);
		}

		RegisterCode RC = new RegisterCode();
		RC.clickRegisterCode();
		Thread.sleep(2000);
		//String pswd = "Password@123";
		lp = new LoginPage();
		lp.loginWithExistingAccount(existingAccountEmailID, pswd);
		Thread.sleep(2000);
		RC.enterStudentInfo();
		Thread.sleep(2000);
		RC.clickContinueButton();
		Thread.sleep(2000);
		RC.registerAcceessCode(code);
		Thread.sleep(2000);
		Assert.assertEquals(RC.checkConfirmMsg(), true);
		Thread.sleep(2000);
	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}