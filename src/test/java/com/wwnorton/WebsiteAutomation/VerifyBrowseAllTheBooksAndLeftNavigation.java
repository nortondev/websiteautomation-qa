	package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.AccountDashBoardPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookDetailsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CartPayment;
import com.wwnorton.WebsiteAutomation.objectFactory.CartShipping;
import com.wwnorton.WebsiteAutomation.objectFactory.CatalogPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.objectFactory.OrderConfirmationPage;
import com.wwnorton.WebsiteAutomation.objectFactory.SocialMediaSites;
import com.wwnorton.WebsiteAutomation.objectFactory.cartLayout;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class VerifyBrowseAllTheBooksAndLeftNavigation extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	BookSearchResultsPage bookResultsPage;
	BookDetailsPage bdp;
	CatalogPage ca;
	cartLayout cLayOut;
	LoginPage lp;
	CartShipping cs;
	CartPayment cp;
	OrderConfirmationPage ocp;
	AccountDashBoardPage adp;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify All books and left navigations")
	@Stories("AS-150 - Click on browse all books button, apply left navigation , add into the cart till check-out page")
	@Test

	public void VerifyBrowseAllTheBooks() throws Exception {

		driver = getDriver();
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String existingAccountEmailID = (String) gContext.getAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1);

		//String existingAccountEmailID = "smaheshwari@wwnorton.com";
		
		urlLink = PropertiesFile.url;

		Thread.sleep(2000);
		hp = new HomePage();
		hp.clickOnShopBooksButton();

		ca = new CatalogPage();

		ca.clickOnFourthCategory();
		Thread.sleep(3000);
		ca.clickOnReligionCategory();
		Thread.sleep(3000);
		ca.clickOnBiblesCategory();
		Thread.sleep(3000);
		ca.clickOnKingJamesVersionCategory();
		Thread.sleep(3000);
		
		String leftNavigationsURL = urlLink + "catalog/nonfiction/religion/bibles/king-james-version";
		Assert.assertEquals(leftNavigationsURL, driver.getCurrentUrl());
		Thread.sleep(2000);
		ca.clickOnBookTheEnglishBible();

		bdp = new BookDetailsPage();
		Thread.sleep(2000);
		ReusableMethods.scrollIntoView(driver, bdp.readMoreLink); 
		bdp.addToCart();
		//bdp.addtoCart.click();

		cLayOut = new cartLayout();
		cLayOut.clickCheckout(); 

		String pswd = "Password@123";
		lp = new LoginPage();
		lp.loginWithExistingAccount(existingAccountEmailID, pswd);
		Thread.sleep(2000);

		// Fill the Shipping Information

		cs = new CartShipping();
		Thread.sleep(2000);
		String fName = "Test";
		String lName = "QAAccount";
		String prmAddress = "45 COUNTY ROAD 13";
		String city = "CLANTON";
		String state = "Alabama";
		String zip = "35045";
		cs.enterShippingInfo(fName, lName, prmAddress, city, state, zip);
		Thread.sleep(2000);
		cs.submitButton.click();

		Thread.sleep(2000);
		if (cs.checkShippingAddressModalExist() == true) {
			cs.selectShippingAddress_Modal.click();
			cs.UPSShippingSaveAndContinue_Modal.click();
			Thread.sleep(3000);
			// cs.shippingMethod();
			cs.submitButton.click();
		}

		// Fill the Payment Information
		Thread.sleep(2000);
		String cHName = "Test Account";
		String expMonth = "December";
		String expYear = "2023";
		String secCode = "123";

		cp = new CartPayment();
		cp.enterPaymentInfo(cHName, expMonth, expYear, secCode);

		Thread.sleep(2000);

		// Check the billing address details

		Thread.sleep(2000);
		cp.checkBillingAddress();
		Thread.sleep(2000);

		// Accept the terms and conditions

		Thread.sleep(2000);
		cp.checkTnC();

		// Place the order

		cp.placeOrder();

		// Order successfully placed

		Thread.sleep(2000);
		ocp = new OrderConfirmationPage();
		Thread.sleep(2000);
		try {
			if(urlLink.equalsIgnoreCase("https://wwnorton.com/")){
				String Prod_orderPlacedURL = urlLink + "order-placed";
				Assert.assertEquals(driver.getCurrentUrl(), Prod_orderPlacedURL);
				
		}else if(urlLink.equalsIgnoreCase("https://qa.wwnorton.net/")) {
			String Lower_orderPlacedURL = urlLink + "checkout";
			Assert.assertEquals(driver.getCurrentUrl(), Lower_orderPlacedURL);
			
		}else if (urlLink.equalsIgnoreCase("https://ecp-qa.wwnorton.net/")) {
			String Lower_orderPlacedURL = urlLink + "checkout";
			Assert.assertEquals(driver.getCurrentUrl(), Lower_orderPlacedURL);
			
		}else if (urlLink.equalsIgnoreCase("https://auth-qa.wwnorton.net/")) {
			String Lower_orderPlacedURL = urlLink + "order-placed";
			Assert.assertEquals(driver.getCurrentUrl(), Lower_orderPlacedURL);
			
		}else if (urlLink.equalsIgnoreCase("https://ecp-stg.wwnorton.net/")) {
			String Lower_orderPlacedURL = urlLink + "order-placed";
			Assert.assertEquals(driver.getCurrentUrl(), Lower_orderPlacedURL);
			
		}else if (urlLink.equalsIgnoreCase("https://auth-stg.wwnorton.net/")) {
			String Lower_orderPlacedURL = urlLink + "checkout";
			Assert.assertEquals(driver.getCurrentUrl(), Lower_orderPlacedURL);
		}
	} catch (Exception e) {
		e.printStackTrace();
	}

		ocp.clickOnLogOut();

	} 

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
