package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.BookDetailsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyAuthorsNameInADP extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	CreateNewAccount CNA;
	LoginPage lp;
	BookSearchResultsPage bookResultsPage;
	BookDetailsPage bdp;

	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Authors Name in Author Details page.")
	@Stories("AS-146 - Verify Authors Name in Author Details page.")
	@Test
	
	public void VerifyAuthorsName() throws Exception {

		driver = getDriver();
		String bookName = "Genesis: The Deep Origin of Societies";

		Thread.sleep(2000);
		hp = new HomePage();
		hp.searchBook(bookName);
		
		Thread.sleep(2000);
		bookResultsPage = new BookSearchResultsPage();
		String authorName =bookResultsPage.selectBook(bookName);
		
		Thread.sleep(2000);
		bdp = new BookDetailsPage();
		
		String bookAuthorName = bdp.getAuthorName();
		Assert.assertTrue(authorName.equals(bookAuthorName));
		
		Thread.sleep(2000);
		String authorTitle = bdp.getAuthorTitle();
		Assert.assertTrue(authorName.equals(authorTitle));
		
		Thread.sleep(2000);
		ReusableMethods.scrollIntoView(driver, bdp.recommendedBooksSection);
		bdp.clickOnRecommendedBook();
		Thread.sleep(2000);
		Assert.assertEquals(bdp.checkBookDetailsOverview(), true);
		Thread.sleep(2000);
	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
