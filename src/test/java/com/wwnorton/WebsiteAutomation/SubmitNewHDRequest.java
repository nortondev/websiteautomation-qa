package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.SupportRequestPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class SubmitNewHDRequest extends PropertiesFile {
	
	public static String urlLink;
	HomePage hp;
	SupportRequestPage srp;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Submit new HD request.")
	@Stories("AS-181 - Submit new HD request.")
	@Test
	public void SubmitSupportTicket() throws Exception {

		driver = getDriver();
		urlLink = PropertiesFile.url;
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String existingAccountEmailID = (String) gContext.getAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1);
		//String existingAccountEmailID = "smaheshwari@wwnorton.com";
		hp = new HomePage();
		ReusableMethods.scrollToBottom(driver);
		Thread.sleep(2000);
		hp.QAclickOnHelp();
		Thread.sleep(2000);
		hp.clickLearnMoreButton();
		Thread.sleep(2000);
		hp.clicksupportTicketButton();
		Thread.sleep(2000);
		srp = new SupportRequestPage();
		String name = "TestName";
		String Email = "test@test.com";
		srp.enterCustomerInformation(name, Email);
		String country= "United States";
		String state = "Alabama";
		String schName= "Air War College, Birmingham";
		ReusableMethods.scrollToBottom(driver);
		srp.enterSchoolInformation(country, state, schName);
		String ctg = "InQuizitive";
		String issue = "Access Issue";
		String dcp = "English";
		String title = "Test Book";
		String sub = "Test Subject";
		String comments = "Test, Please disregard";
		srp.enterRequestInformation(ctg, issue, dcp, title, sub, comments);
		ReusableMethods.scrollToBottom(driver);
		srp.submitRequest();
		Thread.sleep(5000);
		Assert.assertEquals(srp.chkReqConfirmationMsg(), true);
		Thread.sleep(2000);
	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}