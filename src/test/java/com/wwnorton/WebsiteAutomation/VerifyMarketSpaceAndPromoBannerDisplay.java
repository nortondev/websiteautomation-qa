package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.PromoPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class VerifyMarketSpaceAndPromoBannerDisplay extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	PromoPage pp;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Marketing space: Promo/Banner display")
	@Stories("AS-161 - Verify Marketing space: Promo/Banner display")
	@Test
	public void VerifyMarketSpacePromoBanner() throws Exception {

		driver = getDriver();

		hp = new HomePage();
		hp.QAclickOnDetailsLinkHeader();
		Thread.sleep(2000);
		hp.QAcloseDetailsOverlay();
		driver = getDriver();
		urlLink = PropertiesFile.url;
		//driver.get(urlLink + "promo/nealy");
		driver.get(urlLink + "promo/IPNBPUZZLE");
		
		pp = new PromoPage();
		pp.QAclickOnDetailsLinkAtHeaderPromoPage();
		//Assert.assertEquals("Coupon Code NEALY", pp.getPromoCodeDetails());
		Assert.assertEquals("Coupon Code IPNBPUZZLE", pp.getPromoCodeDetails());
		pp.closePromotionDetailsOverlay();
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
