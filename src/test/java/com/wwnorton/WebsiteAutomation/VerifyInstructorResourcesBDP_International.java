package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.FindMyRepPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.InstructorResourcePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyInstructorResourcesBDP_International extends PropertiesFile {

	public static String urlLink;
	CreateNewAccount CNA;
	String newAccountEmailID;
	LoginPage lp;
	HomePage hp;
	InstructorResourcePage irp;
	FindMyRepPage frp;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Instructor Resources - International On Book Details Page.")
	@Stories("AS-438 - Verify Instructor Resources - International On Book Details Page.")
	@Test
	public void VerifyInstructorResources_International() throws Exception {

		driver = getDriver();
		urlLink = PropertiesFile.url;
		
		CNA = new CreateNewAccount();
		CNA.clickOnLogin();
		
		Thread.sleep(2000);
		newAccountEmailID = ReusableMethods.generateEmailID();
		CNA.enterNewAccountEmail(newAccountEmailID);
		CNA.clickOnCreateAccountButton();
		
		Thread.sleep(2000);
		String firstName = "Test";
		String lastName = "Account";
		CNA.enterAccountName(firstName, lastName);
		
		Thread.sleep(2000);
		CNA.validateAccountEmail(newAccountEmailID);
		
		Thread.sleep(2000);
		CNA.retypeAccountEmail(newAccountEmailID);
		CNA.clickOnShowPassword();
		CNA.enterAccountPassword("Password@123");
		
		Thread.sleep(2000);
		CNA.clickCreateAccountButton();
		
		Thread.sleep(2000);
		Assert.assertEquals(CNA.chkYourEmailMsg.getText(), "Please Check Your Email");
		
		String conformationMsg = "We have sent out a confirmation email to you. "
				+ "Please follow the link in the email to verify your account.";
		String accountConformationMsg = CNA.validatAccountConfirmationMessage();
		Assert.assertEquals(accountConformationMsg, conformationMsg);
		System.out.println("STUDENT_USER_NAME => " + newAccountEmailID);
		
		CNA.clickContinueButton();
		Thread.sleep(2000);
		lp = new LoginPage();
		lp.loginClose.click();
		Thread.sleep(2000);
		hp = new HomePage();
		hp.QAclickOnEducator();
		String educatorLinkURL = urlLink + "educator";
		String instrctorResourcesPageURL = urlLink + "catalog/instructor-resources";
		Assert.assertEquals(educatorLinkURL, driver.getCurrentUrl());
		String strEducator = hp.educatorText.getText();
		Assert.assertEquals(strEducator, "Educator");
		
		Thread.sleep(2000);
		ReusableMethods.scrollIntoView(driver, hp.instructorResourcesSection);
		irp = new InstructorResourcePage();
		irp.clickViewInstructorResourcesButton();
		Thread.sleep(2000);
		Assert.assertEquals(instrctorResourcesPageURL, driver.getCurrentUrl());
		Assert.assertEquals(irp.getInstrctorResourcePageHeader(), "Instructor Resources");
		Thread.sleep(2000);
		irp.clickInstructorResourceButton(2);
		Thread.sleep(2000);
		irp.verifyRequestAccessButton();
		irp.clickRequestAccessButton_BDP();
		Thread.sleep(2000);
		irp.verifyInstructorResourceOverlay_Instructor();
		Thread.sleep(2000);
		Assert.assertEquals(irp.verifySchoolInformationOverlay(), true);
		irp.InternationalTab_SchoolInfoOverlay.click();
		Thread.sleep(2000);
		irp.selectSchoolStateInternational("Canada", "Manitoba", "Brandon University, Brandon", "English: English");
		Thread.sleep(2000);
		irp.continueButton.click();
		Thread.sleep(2000);
		irp.courseName.sendKeys("Test Course");
		irp.perTermEnrollment.sendKeys("3");
		irp.selectTerm("Spring");
		Thread.sleep(1000);
		irp.selectYear("2025");
		irp.commentsTextBox.sendKeys("Please disregard, as this is Test comments");
		irp.radioOption_No.click();
		irp.TCCheckbox.click();
		irp.submitButton.click();
		Assert.assertEquals(irp.verifySubmittedRequestOverlay(), true);

	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
