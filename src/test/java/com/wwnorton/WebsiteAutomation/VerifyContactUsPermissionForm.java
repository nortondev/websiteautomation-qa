package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.APWorkshopPage;
import com.wwnorton.WebsiteAutomation.objectFactory.ContactUsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class VerifyContactUsPermissionForm extends PropertiesFile {

	

	public static String urlLink;
	
	HomePage hp;
	ContactUsPage permission;
	

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Contact Us-Permission Form Submission.")
	@Stories("AS-152 - Verify Contact Us-Permission Form Submission.")
	@Test

	public void ContactUsPermissionForm() throws Exception {

		driver = getDriver();

		ReusableMethods.scrollToBottom(driver);
		hp = new HomePage();
		hp.QAclickOnContactUs();
		
		permission = new ContactUsPage();
		Thread.sleep(2000);
		permission.clickOnPermissionLink();
		ReusableMethods.scrollToBottom(driver);
		permission.clickOnRequestFormButton();
		
		// Fill details in Contact Information overlay
		Thread.sleep(2000);
		String fName = "Test Pls Disregard";
		String companyName = "Test Pls Disregard";
		String addLine1 = "Test Pls Disregard";
		String addLine2 = "Test Pls Disregard";
		String cityName = "Test Pls Disregard";
		String stateName = "Alabama";
		String zipCode = "36005";
		String phnNo = "(111)-111-1111";
		String faxNo = "(111) 111-1111 ext. 11111";
		String emailId = "test@test.com";
		permission.submitPermissionContactInformationForm(fName, companyName, addLine1, addLine2, cityName, stateName, zipCode, phnNo, faxNo, emailId);
	    Thread.sleep(2000);
	    
	    // Fill details in Book Information overlay 
		Thread.sleep(2000);
		String Aname = "Test Pls Disregard";
		String ISBN = "978-0-393-33048-9";
		String TitleName = "Test Pls Disregard";
		String CopyRightLine = "000";
		String PagesNo = "000";
		String TitleOfSelection = "Test Pls Disregard";
		String TotalNoOfPages = "0000";
		String TotleWordCount = "0000";
		String TotleNoOfLines = "00";
		String TotleNoOfIll = "00";
		permission.submitPermissionBookInformationForm(Aname, ISBN, TitleName, CopyRightLine, PagesNo, TitleOfSelection, TotalNoOfPages, TotleWordCount, TotleNoOfLines, TotleNoOfIll, TotleNoOfIll);
		
		// Fill Your Publication overlay
		
		Thread.sleep(2000);
		String YPubTitle = "Test Pls Disregard";
		String YAuthEdit = "Test Pls Disregard";
		String YPublisher = "Test Pls Disregard";
		String currnetDate = ReusableMethods.getCurrentDay();
		String formatType = "Test Pls Disregard";
		String noOfPages = "000";
		String totQuantity = "000";
		String price = "0000";
		String addComments = "This is the test by WWN QA team pls disregard";
		permission.submitPermissionYourPublicationForm(YPubTitle, YAuthEdit, YPublisher, currnetDate , formatType ,noOfPages ,totQuantity ,price ,addComments);
		Thread.sleep(3000);
		Assert.assertEquals(permission.checkPermissionConfirmationMessage(), true);
	}

	@AfterTest
	public void closeTest() throws Exception {
	PropertiesFile.tearDownTest();
	}
}
