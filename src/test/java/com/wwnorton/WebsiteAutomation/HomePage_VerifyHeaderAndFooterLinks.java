package com.wwnorton.WebsiteAutomation;


import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class HomePage_VerifyHeaderAndFooterLinks extends PropertiesFile {
	
	public static String urlLink;
	HomePage hp;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Header and Footer links in Website Home Page.")
	@Stories("AS-135 - Verify Header and Footer links in Website Home Page.")
	@Test
	public void VerifyHeaderAndFooterLinks() throws Exception {

		driver = getDriver();
		urlLink = PropertiesFile.url;
		
		hp = new HomePage();
		hp.QAclickOnReader();
		
		String readerLinkURL = urlLink + "reader";
		Assert.assertEquals(readerLinkURL, driver.getCurrentUrl());
		String strReader = hp.readerText.getText();
		Assert.assertEquals(strReader, "Reader");
		
		Thread.sleep(2000);
		hp.QAclickOnStudent();
		String studentLinkURL = urlLink + "student";
		Assert.assertEquals(studentLinkURL, driver.getCurrentUrl());
		String strStudent = hp.studentText.getText();
		Assert.assertEquals(strStudent, "Student");
		
		Thread.sleep(2000);
		hp.QAclickOnEducator();
		String educatorLinkURL = urlLink + "educator";
		Assert.assertEquals(educatorLinkURL, driver.getCurrentUrl());
		String strEducator = hp.educatorText.getText();
		Assert.assertEquals(strEducator, "Educator");
		
		Thread.sleep(2000);
		ReusableMethods.scrollToBottom(driver);
		hp.QAclickOnWhoWeAre();
		String whoWeAreLinkURL = urlLink + "who-we-are";
		Assert.assertEquals(whoWeAreLinkURL, driver.getCurrentUrl());
		String strWhoWeAre = hp.whoWWeAreText.getText();
		Assert.assertEquals(strWhoWeAre, "Who We Are");
		
		Thread.sleep(2000);
		ReusableMethods.scrollToBottom(driver);
		hp.QAclickOnCareers();
		String careersLinkURL = urlLink + "careers";
		Assert.assertEquals(careersLinkURL, driver.getCurrentUrl());
		String strCareers = hp.careersText.getText();
		Assert.assertEquals(strCareers, "Careers");
		
		Thread.sleep(2000);
		ReusableMethods.scrollToBottom(driver);
		hp.QAclickOnHelp();
		String helpLinkURL = urlLink + "help";
		Assert.assertEquals(helpLinkURL, driver.getCurrentUrl());
		String strHelp = hp.helpText.getText();
		Assert.assertEquals(strHelp, "W. W. Norton Help");
		
		Thread.sleep(2000);
		ReusableMethods.scrollToBottom(driver);
		hp.QAclickOnAccessibility();
		String accessibilityLinkURL = urlLink + "accessibility";
		Assert.assertEquals(accessibilityLinkURL, driver.getCurrentUrl());
		String strAccessibilityText = hp.accessibilityText.getText();
		Assert.assertEquals(strAccessibilityText, "Accessibility");
		
		Thread.sleep(2000);
		ReusableMethods.scrollToBottom(driver);
		hp.QAclickOnPrivacyPolicy();
		String privacyPolicyLinkURL = urlLink + "privacy-policy";
		Assert.assertEquals(privacyPolicyLinkURL, driver.getCurrentUrl());
		String strPrivacyPolicyText = hp.privacyPolicyText.getText();
		Assert.assertEquals(strPrivacyPolicyText, "Privacy Policy");
		
		Thread.sleep(2000);
		ReusableMethods.scrollToBottom(driver);
		hp.QAclickOnTermsOfUse();
		String termsOfUseLinkURL = urlLink + "terms-of-use";
		Assert.assertEquals(termsOfUseLinkURL, driver.getCurrentUrl());
		String strTermsOfUseText = hp.termsOfUseText.getText();
		Assert.assertEquals(strTermsOfUseText, "Terms of Use");
		
		Thread.sleep(2000);
		ReusableMethods.scrollToBottom(driver);
		hp.QAclickOnContactUs();
		String contactUsLinkURL = urlLink + "contact-us";
		Assert.assertEquals(contactUsLinkURL, driver.getCurrentUrl());
		String strContactUsText = hp.contactUsText.getText();
		//Assert.assertEquals(strContactUsText, "Contact");
		
		Thread.sleep(2000);
		hp.clickOnSeagull();
		String seagullIconLinkURL = urlLink;
		Assert.assertEquals(seagullIconLinkURL, driver.getCurrentUrl());
		String strSeagullIcon = hp.seagullIconText.getText();
		Assert.assertEquals(strSeagullIcon, "W • W • NORTON & COMPANY INDEPENDENT & EMPLOYEE-OWNED");

	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
