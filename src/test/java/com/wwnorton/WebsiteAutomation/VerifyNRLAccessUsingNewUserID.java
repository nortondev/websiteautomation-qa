package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.FindMyRepPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.InstructorResourcePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyNRLAccessUsingNewUserID extends PropertiesFile {
	
	public static String urlLink;
	CreateNewAccount CNA;
	LoginPage lp;
	HomePage hp;
	InstructorResourcePage irp;
	FindMyRepPage frp;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("NRL request using new user credentials")
	@Stories("AS-168 - NRL request using new user credentials")
	@Test
	public void VerifyInstructorResources() throws Exception {
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String existingAccountEmailID = (String) gContext.getAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1);
		//String existingAccountEmailID = "qa04apr2021@mailinator.com";
		
		driver = getDriver();
	
		urlLink = PropertiesFile.url;
		driver.get(urlLink + "instructor-resources/9780393421507");
	
		
		CNA = new CreateNewAccount();
		CNA.clickOnLogin();
		
		Thread.sleep(2000);
		lp = new LoginPage();
		String Password = "Password@123";
		
		lp.loginWithExistingAccount(existingAccountEmailID, Password);
		lp.closeAccountLoginImg();
		Thread.sleep(2000);
		
		irp = new InstructorResourcePage();
	
		irp.verifyRequestAccessButton();
		irp.clickRequestAccessButton_BDP();
		Thread.sleep(2000);
		irp.verifyInstructorResourceOverlay_Instructor();
		Thread.sleep(2000);
		Assert.assertEquals(irp.verifySchoolInformationOverlay(), true);
		
		
		
		irp.selectSchoolStateUSAHighSchool("Ohio", "Apollo Career High School, Lima");
		Thread.sleep(2000);
		irp.departmentName.sendKeys("Test Pls Disregard");
		irp.continueButton.click();
		Thread.sleep(2000);
		irp.courseName.sendKeys("Test Pls Disregard");
		irp.perTermEnrollment.sendKeys("0");
		irp.selectTerm("Spring");
		Thread.sleep(1000);
		irp.selectYear("2025");
		irp.commentsTextBox.sendKeys("This is the test by WWWN website team pls disregard");
		irp.radioOption_No.click();
		irp.TCCheckbox.click();
		irp.NotificationCheckbox.click();
		irp.submitButton.click();
		Assert.assertEquals(irp.verifySubmittedRequestOverlay(), true);  

	}

	
	@AfterTest
	public void closeTest() throws Exception {
	PropertiesFile.tearDownTest();
	}
}
