package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.NortonCustomFormPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class ValidateNortonCustomFormSubmission extends PropertiesFile {
	
	public static String urlLink;
	NortonCustomFormPage NCF;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Validate Norton Custom Form Submission.")
	@Stories("AS-162 - Validate Norton Custom Form Submission.")
	@Test
	public void ValidateNortonCustomForm() throws Exception {

		driver = getDriver();
		urlLink = PropertiesFile.url;
		driver.get(urlLink+"norton-custom");
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String existingEmailID = (String) gContext.getAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1);
		//String existingEmailID = "qa07feb2021@mailinator.com";
		String name = "Test Account";
		int[] phoneIntArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
		String role = "Student";
		Thread.sleep(2000);
		NCF = new NortonCustomFormPage();
		Assert.assertEquals(NCF.clickContactNortonCustomButton(), true);
		Thread.sleep(2000);
		Assert.assertEquals(NCF.verifyContactInformationOverlay(), true);
		NCF.submitContactInformationForm(name, existingEmailID, phoneIntArray, role);
		Thread.sleep(2000);
		NCF.selectSchoolStateUSACollege("Alabama", "Air War College, Birmingham");
		Thread.sleep(2000);
		NCF.continueButton.click();
		String subject = "Chemistry";
		String courseName = "Fall";
		String courseEnrolllment = "2";
		String requestComments = "Please disregard, It is test comments";
		NCF.submitRequestDetailsForm(subject, courseName, courseEnrolllment, requestComments);
		Thread.sleep(2000);
		Assert.assertEquals(NCF.verifyConnfirmationOverlay(), true);
		Thread.sleep(2000);
		Assert.assertEquals(NCF.contactNortonCustomButton.isDisplayed(), true);

	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}