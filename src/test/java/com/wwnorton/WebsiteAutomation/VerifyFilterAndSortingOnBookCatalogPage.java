package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;

import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyFilterAndSortingOnBookCatalogPage extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	BookSearchResultsPage bookResultsPage;

	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Filter And Sorting On Book Catalog Page.")
	@Stories("AS-149 - Verify Filter And Sorting On Book Catalog Page.")
	@Test
	
	public void VerifyFilterAndSorting() throws Exception {

		driver = getDriver();
		urlLink = PropertiesFile.url;
		
		Thread.sleep(2000);
		hp = new HomePage();
		hp.clickOnShopBooksButton();
		
		Thread.sleep(2000);
		String catalogPageURL = urlLink + "catalog";
		Assert.assertEquals(catalogPageURL, driver.getCurrentUrl());
		
		bookResultsPage = new  BookSearchResultsPage();
		Assert.assertEquals(bookResultsPage.checkAllCategoryLinks(), true);
		bookResultsPage.clickFilterOption("Release", "Coming Soon");
		Assert.assertEquals(bookResultsPage.getFilterName(), "Coming Soon");
		bookResultsPage.clickResetFilterButton("Release (1)");
		Thread.sleep(2000);
		bookResultsPage.clickFilterOption("Editions", "1st Edition");
		Assert.assertEquals(bookResultsPage.getFilterName(), "1st Edition");
		bookResultsPage.clickResetFilterButton("Editions (1)");
		Thread.sleep(2000);
		bookResultsPage.clickFilterOption("Volumes", "1&2");
		Assert.assertEquals(bookResultsPage.getFilterName(), "1&2");
		bookResultsPage.clickResetFilterButton("Volumes (1)");
		Thread.sleep(2000);
		bookResultsPage.clickFilterOption("Series", "A Norton Quick Reference Guide");
		Assert.assertEquals(bookResultsPage.getFilterName(), "A Norton Quick Reference Guide");
		bookResultsPage.clickResetFilterButton("Series (1)");
		
		Thread.sleep(2000);
		bookResultsPage.clickSortDropDown();
		bookResultsPage.selectSortOption("Newest");
		Assert.assertEquals(bookResultsPage.getSelectedSortOption("Newest"), "Newest");
		
		
		Thread.sleep(2000);
		bookResultsPage.clickSortDropDown();
		bookResultsPage.selectSortOption("Oldest");
		Assert.assertEquals(bookResultsPage.getSelectedSortOption("Oldest"), "Oldest");
		
		Thread.sleep(2000);
		bookResultsPage.clickSortDropDown();
		bookResultsPage.selectSortOption("Title A-Z");
		Assert.assertEquals(bookResultsPage.getSelectedSortOption("Title A-Z"), "Title A-Z");
		
	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
