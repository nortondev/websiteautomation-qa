package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.BookDetailsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CartPayment;
import com.wwnorton.WebsiteAutomation.objectFactory.CartShipping;
import com.wwnorton.WebsiteAutomation.objectFactory.CatalogPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.objectFactory.OrderConfirmationPage;
import com.wwnorton.WebsiteAutomation.objectFactory.PromoPage;
import com.wwnorton.WebsiteAutomation.objectFactory.cartLayout;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReadJsonFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class VerifyPromoPageElements extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	PromoPage pp;
	CatalogPage ca;
	BookSearchResultsPage bsr;
	BookDetailsPage bdp;
	cartLayout cLayOut;
	LoginPage lp;
	CartShipping cs;
	CartPayment cp;
	OrderConfirmationPage ocp;
	
	ReadJsonFile readJsonObject = new ReadJsonFile();
	JsonObject jsonobject = readJsonObject.readJason();
	String existingAccountEmailID = jsonobject.getAsJsonObject("ExistingAccountCredentials").get("accountEmail").getAsString();
	String pswd = jsonobject.getAsJsonObject("ExistingAccountCredentials").get("accountpw").getAsString();


	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Promo page elements")
	@Stories("AS-164 - Verify Promo page elements")
	@Test
	public void VerifyPromoPage() throws Exception {

		driver = getDriver();

		urlLink = PropertiesFile.url;
		driver.get(urlLink + "promo/IPNBPUZZLE ");
		pp = new PromoPage();
		pp.QAclickOnDetailsLinkAtHeaderPromoPage();
		Assert.assertEquals("Coupon Code IPNBPUZZLE", pp.getPromoCodeDetails());
		pp.closePromotionDetailsOverlay();
		ca = new CatalogPage();
		ca.clickOnFirstCategory();
		pp.allResultsBreadCrumb();
		Thread.sleep(3000);
		ca.clickOnSecoundCategory();
		if (ca.clickOnSecoundCategory() == true) {
			pp.allResultsBreadCrumb();
		}
		
		Thread.sleep(3000);
		bsr = new BookSearchResultsPage();
		bsr.clickSortDropDown();
		bsr.selectSortOption("Newest");
		bsr.clickSortDropDown();
		bsr.selectSortOption("Oldest");
		bsr.clickSortDropDown();
		bsr.selectSortOption("Title A-Z");
		bsr.clickSortDropDown();
		bsr.selectSortOption("Relevance");
		Thread.sleep(3000); 

		boolean FilterEditionsEnabled = bsr.clickBookListFilterOption("Editions");
		if (FilterEditionsEnabled == true) {
			String totalFilters_Editions = bsr.clickAllFiltersInFilterDropDown();
			Thread.sleep(2000);
			bsr.clickBookListFilterOption("Editions (" + totalFilters_Editions + ")");
			Thread.sleep(2000);
			bsr.clickResetFilterButton("Editions (" + totalFilters_Editions + ")");
			Thread.sleep(3000);
		}

		boolean FilterSeriessEnabled = bsr.clickBookListFilterOption("Series");
		if (FilterSeriessEnabled == true) {
			String totalFilters_Series = bsr.clickAllFiltersInFilterDropDown();
			Thread.sleep(2000);
			bsr.clickBookListFilterOption("Series (" + totalFilters_Series + ")");
			Thread.sleep(2000);
			bsr.clickResetFilterButton("Series (" + totalFilters_Series + ")");
			Thread.sleep(2000);
		}
		pp.addBookInToCart();

		cLayOut = new cartLayout();
		Thread.sleep(3000);
		Assert.assertEquals(cLayOut.webOfferPromoTextExist(), "Promo Applied");
		cLayOut.clickCheckout();
		Thread.sleep(2000);
		lp = new LoginPage();
		lp.loginWithExistingAccount(existingAccountEmailID, pswd);
		Thread.sleep(2000); 

		// Fill the Shipping Information

		cs = new CartShipping();
		Thread.sleep(2000);
		String fName = "Test";
		String lName = "QAAccount";
		String prmAddress = "501 AVENUE E";
		String city = "BIRMINGHAM";
		String state = "Alabama";
		String zip = "35001";
		cs.enterShippingInfo(fName, lName, prmAddress, city, state, zip);
		Thread.sleep(2000);
		cs.submitButton.click();

		Thread.sleep(2000);
		if (cs.checkShippingAddressModalExist() == true) {
			cs.selectShippingAddress_Modal.click();
			cs.UPSShippingSaveAndContinue_Modal.click();
			Thread.sleep(3000);
			cs.submitButton.click();
		}

		// Fill the Payment Information
		Thread.sleep(2000);
		String cHName = "Test Account";
		String expMonth = "December";
		String expYear = "2023";
		String secCode = "123";

		cp = new CartPayment();
		cp.enterPaymentInfo(cHName, expMonth, expYear, secCode);

		Thread.sleep(2000);

		// Check the billing address details

		Thread.sleep(2000);
		cp.checkBillingAddress();
		Thread.sleep(2000);

		// Accept the terms and conditions

		Thread.sleep(2000);
		cp.checkTnC();

		// Place the order

		cp.placeOrder();

		// Order successfully placed

				Thread.sleep(2000);
				ocp = new OrderConfirmationPage();
				Thread.sleep(2000);
				try {
					if(urlLink.equalsIgnoreCase("https://wwnorton.com/")){
						String Prod_orderPlacedURL = urlLink + "order-placed";
						Assert.assertEquals(driver.getCurrentUrl(), Prod_orderPlacedURL);
						
				}else if(urlLink.equalsIgnoreCase("https://qa.wwnorton.net/")) {
					String Lower_orderPlacedURL = urlLink + "checkout";
					Assert.assertEquals(driver.getCurrentUrl(), Lower_orderPlacedURL);
					
				}else if (urlLink.equalsIgnoreCase("https://ecp-qa.wwnorton.net/")) {
					String Lower_orderPlacedURL = urlLink + "checkout";
					Assert.assertEquals(driver.getCurrentUrl(), Lower_orderPlacedURL);
					
				}else if (urlLink.equalsIgnoreCase("https://auth-qa.wwnorton.net/")) {
					String Lower_orderPlacedURL = urlLink + "order-placed";
					Assert.assertEquals(driver.getCurrentUrl(), Lower_orderPlacedURL);
					
				}else if (urlLink.equalsIgnoreCase("https://ecp-stg.wwnorton.net/")) {
					String Lower_orderPlacedURL = urlLink + "order-placed";
					Assert.assertEquals(driver.getCurrentUrl(), Lower_orderPlacedURL);
					
				}else if (urlLink.equalsIgnoreCase("https://auth-stg.wwnorton.net/")) {
					String Lower_orderPlacedURL = urlLink + "checkout";
					Assert.assertEquals(driver.getCurrentUrl(), Lower_orderPlacedURL);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		ocp.clickOnLogOut();

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
