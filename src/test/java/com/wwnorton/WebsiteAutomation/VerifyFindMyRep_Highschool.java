package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.AccountDashBoardPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookDetailsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CartPayment;
import com.wwnorton.WebsiteAutomation.objectFactory.CartShipping;
import com.wwnorton.WebsiteAutomation.objectFactory.CatalogPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.FindMyRepPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.InstructorResourcePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.objectFactory.OrderConfirmationPage;
import com.wwnorton.WebsiteAutomation.objectFactory.SocialMediaSites;
import com.wwnorton.WebsiteAutomation.objectFactory.cartLayout;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class VerifyFindMyRep_Highschool extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	BookSearchResultsPage bookResultsPage;
	BookDetailsPage bdp;
	CatalogPage ca;
	cartLayout cLayOut;
	LoginPage lp;
	CartShipping cs;
	CartPayment cp;
	OrderConfirmationPage ocp;
	AccountDashBoardPage adp;
	InstructorResourcePage ins;
	FindMyRepPage fyr;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Find My Rep for High School")
	@Stories("AS-155 - Find My Rep for High School")
	@Test

	public void VerifyFindMyRepHighschool() throws Exception {

		driver = getDriver();
		String stateNameUSA_Highschool = "Nevada";
		String schNameUSA_Highschool = "Austin School, Austin";
		String subNameUSA_Highschool = "Technical Question";
		String yFName_Highschool = "Test Pls Disregard";
		String yLName_Highschool = "Test Pls Disreagrd";
		String yEmailAdd_Highschool = "test@test.com";
		String yMessage_Highschool = "This is the test by WWN QA Team Please Disregard";

		hp = new HomePage();
		hp.QAclickOnEducator();

		// Send Find My Rep requests for USA High School

		fyr = new FindMyRepPage();
		fyr.clickOnFindMyRepButton();
		fyr.clickOnHighSchoolSection();
		fyr.selectSchoolStateUSAHighschool(stateNameUSA_Highschool, schNameUSA_Highschool);
		fyr.clickOnSearchButton();
		Thread.sleep(3000);
		Assert.assertEquals("Anna Collier", fyr.getRepNameAtHeader());
		Thread.sleep(3000);
		Assert.assertEquals("Anna Collier", fyr.getRepName());
		Thread.sleep(3000);
		Assert.assertEquals("303 912-6436", fyr.getRepPhoneNum());
		Thread.sleep(3000);
		Assert.assertEquals("acollier@wwnorton.com", fyr.getRepEmailId());
		fyr.submitContactDetails(yFName_Highschool, yLName_Highschool, yEmailAdd_Highschool, yMessage_Highschool);
		Thread.sleep(3000);
		fyr.selectSubject(subNameUSA_Highschool);
		fyr.clickOnSendButton();
		Thread.sleep(3000);
		Assert.assertEquals(fyr.checkRequestConformationPopUpForFindMyRep(), true);

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
