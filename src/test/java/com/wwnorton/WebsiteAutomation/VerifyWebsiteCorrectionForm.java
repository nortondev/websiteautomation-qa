package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.APWorkshopPage;
import com.wwnorton.WebsiteAutomation.objectFactory.ContactUsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class VerifyWebsiteCorrectionForm extends PropertiesFile {

	public static String urlLink;

	HomePage hp;
	ContactUsPage correction;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify website correction form")
	@Stories("AS-180 - Verify Contact Us-Correction Form Submission.")
	@Test

	public void ContactUsCorrectionForm() throws Exception {

		driver = getDriver();

		ReusableMethods.scrollToBottom(driver);
		hp = new HomePage();
		hp.QAclickOnContactUs();

		correction = new ContactUsPage();
		Thread.sleep(2000);
		correction.clickOnSubmitACorrectionLink();
		ReusableMethods.scrollToBottom(driver);
		correction.clickOnCorrectionFormButton();

		// Fill details in website feedback overlay

		Thread.sleep(2000);
		String fName = "Test Pls Disregard";
		String emailId = "test@test.com";
		String prodTitle = "Test Pls Disregard";
		String isbn = "9781631496530";
		String pageNoURL = "https://wwnorton.com/books/9781631496530";
		String quoteTheError = "Test Please Disregard";
		String suggestACorrection = "This is the test by WWN QA team please disregard";
		correction.submitCorrectionForm(fName, emailId, prodTitle , isbn , pageNoURL , quoteTheError , suggestACorrection);	
		Thread.sleep(3000);
		Assert.assertEquals(correction.checkCorrectionConfirmationMessage(), true);
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
