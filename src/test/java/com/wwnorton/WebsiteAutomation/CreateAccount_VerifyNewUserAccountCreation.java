package com.wwnorton.WebsiteAutomation;


import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class CreateAccount_VerifyNewUserAccountCreation extends PropertiesFile {

	CreateNewAccount CNA;
	String newAccountEmailID;
	
	@Parameters({ "browser" })
	@BeforeSuite
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
		VerifyNewUserAccountCreation();
		PropertiesFile.tearDownTest();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify New User Account Creation in Norton Website.")
	@Stories("AS-138 - Verify New User Account Creation in Norton Website.")
	@Step("Create New User Account,  Method: {method}")
	public void VerifyNewUserAccountCreation() throws Exception {

		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		
		CNA = new CreateNewAccount();
		CNA.clickOnLogin();
		
		Thread.sleep(2000);
		newAccountEmailID = ReusableMethods.generateEmailID();
		CNA.enterNewAccountEmail(newAccountEmailID);
		CNA.clickOnCreateAccountButton();
		
		Thread.sleep(2000);
		String firstName = "Test";
		String lastName = "Account";
		CNA.enterAccountName(firstName, lastName);
		
		Thread.sleep(2000);
		CNA.validateAccountEmail(newAccountEmailID);
		
		Thread.sleep(2000);
		CNA.retypeAccountEmail(newAccountEmailID);
		CNA.clickOnShowPassword();
		CNA.enterAccountPassword("Password@123");
		
		Thread.sleep(2000);
		CNA.clickCreateAccountButton();
		
		Thread.sleep(2000);
		Assert.assertEquals(CNA.chkYourEmailMsg.getText(), "Please Check Your Email");
		
		String conformationMsg = "We have sent out a confirmation email to you. "
				+ "Please follow the link in the email to verify your account.";
		String accountConformationMsg = CNA.validatAccountConfirmationMessage();
		Assert.assertEquals(accountConformationMsg, conformationMsg);
		
		CNA.clickContinueButton();
		Thread.sleep(2000);
		CNA.clickLogOut();
		
		gContext = SeleniumTestsContextManager.getGlobalContext();
		gContext.setAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1, newAccountEmailID);
		SeleniumTestsContextManager.setGlobalContext(gContext);

		System.out.println("STUDENT_USER_NAME => " + newAccountEmailID);

	}

}
