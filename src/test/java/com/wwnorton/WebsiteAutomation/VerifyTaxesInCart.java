package com.wwnorton.WebsiteAutomation;


import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.BookDetailsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CartItemDetails;

import com.wwnorton.WebsiteAutomation.objectFactory.CartShipping;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;

import com.wwnorton.WebsiteAutomation.objectFactory.cartLayout;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReadJsonFile;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class VerifyTaxesInCart extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	CreateNewAccount CNA;
	LoginPage lp;
	BookSearchResultsPage bookResultsPage;
	BookDetailsPage bdp;
	cartLayout cLayOut;
	CartItemDetails cid;
	CartShipping cs;

	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Taxes.")
	@Stories("AS-190 - Verify Taxes and Actual Price of the book in the Cart.")
	@Test
	
	public void VerifyTaxes() throws Exception {

		//SS
		ReadJsonFile readJsonObject = new ReadJsonFile();
		JsonObject jsonobject = readJsonObject.readJason();
		String existingAccountEmailID = jsonobject.getAsJsonObject("ExistingAccountCredentials").get("accountEmail").getAsString();
		String pswd = jsonobject.getAsJsonObject("ExistingAccountCredentials").get("accountpw").getAsString();
		
		driver = getDriver();
		
		//String bookName = "Counterpoint: A Memoir of Bach and Mourning";
		
		CNA = new CreateNewAccount();
		CNA.clickOnLogin();
		Thread.sleep(2000);
		lp = new LoginPage();
		lp.loginWithExistingAccount(existingAccountEmailID, pswd);
		lp.closeAccountLoginImg();
		
		Thread.sleep(2000);
		hp = new HomePage();
		hp.clickOnShopBooksButton();
		Thread.sleep(2000);
		bookResultsPage = new BookSearchResultsPage();
		bookResultsPage.selectBookFromList(1);
		Thread.sleep(2000);
		bdp = new BookDetailsPage();
		/*ReusableMethods.scrollIntoView(driver, bdp.bookFormatLayout); 
		String headerOption_Book = "Paperback";
		Thread.sleep(2000);
		bdp.clickBookBinding(headerOption_Book);
		Thread.sleep(2000);
		bdp.addBookToCart();*/
		bdp.addToCart();
		cLayOut = new cartLayout();
		Thread.sleep(2000);
		String BookPrice_Cart = cLayOut.getBookPrice_CartLayOut();
		cLayOut.clickCheckout();
		Thread.sleep(3000);
		cid = new CartItemDetails();
		System.out.println(cid.getBookPrice_Cart());
		System.out.println(BookPrice_Cart);
		Assert.assertEquals(BookPrice_Cart.equalsIgnoreCase(cid.getBookPrice_Cart()), true);
		String estimatedTax = cid.getEstimatedTax_Cart();
		
		String  strPrice = BookPrice_Cart.substring(1);
		double ActualTax = (double)Math.round(Double.parseDouble(strPrice) * 10.00) / 100.00;
		String strTax = String.format("%.2f", ActualTax);
		String TaxAmount = "$" + strTax;
		Thread.sleep(3000);
		Assert.assertEquals(estimatedTax.equalsIgnoreCase(TaxAmount), true);
		cid.clickLogOut_CheckOut();
		Thread.sleep(3000);
	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}