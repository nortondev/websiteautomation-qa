package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.FindMyRepPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.InstructorResourcePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyInstructorResourcesOnBDP extends PropertiesFile {
	
	public static String urlLink;
	CreateNewAccount CNA;
	LoginPage lp;
	HomePage hp;
	InstructorResourcePage irp;
	FindMyRepPage frp;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Instructor Resources On Book Details Page.")
	@Stories("AS-160 - Verify Instructor Resources On Book Details Page.")
	@Test
	public void VerifyInstructorResources() throws Exception {

		driver = getDriver();
		urlLink = PropertiesFile.url;
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String existingEmailID = (String) gContext.getAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1);
		
		//String existingAccountEmailID = "qa30mar2021@mailinator.com";
		
		CNA = new CreateNewAccount();
		CNA.clickOnLogin();
		
		Thread.sleep(2000);
		lp = new LoginPage();
		String Password = "Password@123";
		
		lp.loginWithExistingAccount(existingEmailID, Password);
		lp.closeAccountLoginImg();
		Thread.sleep(2000);
		
		hp = new HomePage();
		hp.QAclickOnEducator();
		String educatorLinkURL = urlLink + "educator";
		String instrctorResourcesPageURL = urlLink + "catalog/instructor-resources";
		Assert.assertEquals(educatorLinkURL, driver.getCurrentUrl());
		String strEducator = hp.educatorText.getText();
		Assert.assertEquals(strEducator, "Educator");
		
		Thread.sleep(2000);
		ReusableMethods.scrollIntoView(driver, hp.instructorResourcesSection);
		irp = new InstructorResourcePage();
		irp.clickViewInstructorResourcesButton();
		Thread.sleep(2000);
		Assert.assertEquals(instrctorResourcesPageURL, driver.getCurrentUrl());
		Assert.assertEquals(irp.getInstrctorResourcePageHeader(), "Instructor Resources");
		Thread.sleep(2000);
		irp.clickInstructorResourceButton(3);
		Thread.sleep(2000);
		irp.verifyRequestAccessButton();
		irp.clickRequestAccessButton_BDP();
		Thread.sleep(2000);
		irp.verifyInstructorResourceOverlay_Instructor();
		Thread.sleep(2000);
		Assert.assertEquals(irp.verifySchoolInformationOverlay(), true);
		irp.selectSchoolStateUSACollege("Alabama", "Air War College, Birmingham");
		Thread.sleep(2000);
		irp.continueButton.click();
		Thread.sleep(2000);
		irp.courseName.sendKeys("Test Course");
		irp.perTermEnrollment.sendKeys("3");
		irp.selectTerm("Spring");
		Thread.sleep(1000);
		irp.selectYear("2025");
		irp.commentsTextBox.sendKeys("Please disregard, as this is Test comments");
		irp.radioOption_No.click();
		irp.TCCheckbox.click();
		irp.submitButton.click();
		Assert.assertEquals(irp.verifySubmittedRequestOverlay(), true);

	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
