package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.BookDetailsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.objectFactory.SocialMediaSites;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class VerifyBookRetailersBDP extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	BookSearchResultsPage bookResultsPage;
	BookDetailsPage bdp;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Book Retailers in Book Details page.")
	@Stories("AS-148 - Books in retail panel & newsletter sub")
	@Test

	public void VerifyRetailers() throws Exception {

		driver = getDriver();
		String bookName = "978-0-393-33048-9";
		String emailID = "qa22feb2022@maildrop.cc";
		Thread.sleep(2000);
		hp = new HomePage();

		hp.searchBook(bookName);

		
		Thread.sleep(2000);
		bdp = new BookDetailsPage();
		
		
		
		// --> Amazon Retailers Page 
		bdp.connect_Amazon.click();
		String parentWin = driver.getWindowHandle();
		String parentWindow = bdp.openBookRetailerWindow(parentWin);
		Assert.assertEquals(bdp.getPublisherName_AmazonRetailer(), "W. W. Norton & Company");
		Thread.sleep(2000);
		driver.close();
		driver.switchTo().window(parentWindow); 
		
		// --> Apple Books Retailers Page 
		
		bdp.connect_Apple_Books.click();
		String parentWin1 = driver.getWindowHandle();
		String parentWindow1 = bdp.openBookRetailerWindow(parentWin1);
		Assert.assertEquals(bdp.getPublisherName_AppleBookRetailer(), "W. W. Norton & Company");
		Thread.sleep(2000);
		driver.close();
		driver.switchTo().window(parentWindow); 
		
		
		// --> Barnes & Noble Retailers Page
		
		bdp.connect_Barnes_Noble.click();
		String parentWin2 = driver.getWindowHandle();
		String parentWindow2 = bdp.openBookRetailerWindow(parentWin2);
		Assert.assertEquals(bdp.getPublisherName_BarnesBookRetailer(), "Norton, W. W. & Company, Inc.");
		driver.close();
		driver.switchTo().window(parentWindow2); 
		
		
		bdp.clickOnBooksAMillion();
		bdp.clickOnBookShop();
		bdp.clickOnHudson();
		bdp.clickOnndieBound();
		bdp.clickOnTarget();
		bdp.clickOnWalmart();
		
		// News letter subscription from BDP
		hp.QAsendEmailForNewsLetter(emailID);
		String successMessage = hp.QAsendEmailForNewsLetter(emailID);
		Assert.assertEquals(successMessage, "Thanks for signing up!");
		
		// --> Books A Million Retailers Page
		
		/*	bdp.connect_Books_A_Million.click();
		String parentWin3 = driver.getWindowHandle();
		String parentWindow3 = bdp.openBookRetailerWindow(parentWin3);
		Assert.assertEquals(bdp.getPublisherName_BooksAMillionRetailer(), ": W. W. Norton & Company");
		driver.close();
		driver.switchTo().window(parentWindow3); */
		
		
		// --> Books Shop Retailers Page
		
	/*	bdp.connect_Book_Shop.click();
		String parentWin4 = driver.getWindowHandle();
		String parentWindow4 = bdp.openBookRetailerWindow(parentWin4);
		Assert.assertEquals(bdp.getPublisherName_BookShopRetailer(), "W. W. Norton & Company");
		driver.close();
		driver.switchTo().window(parentWindow4); */
		
	/*	bdp.connect_Hudson.click();
		String parentWin5 = driver.getWindowHandle();
		String parentWindow5 = bdp.openBookRetailerWindow(parentWin5);
		Assert.assertEquals(bdp.getPublisherName_HudsonRetailer(), " W. W. Norton & Company");
		driver.close();
		driver.switchTo().window(parentWindow5); */
		
		
	/*	bdp.connect_IndieBound.click();
		String parentWin6 = driver.getWindowHandle();
		String parentWindow6 = bdp.openBookRetailerWindow(parentWin6);
		Assert.assertEquals(bdp.getPublisherName_IndieBoundRetailer(), "W. W. Norton & Company, 9780393330489, 192pp.");
		driver.close();
		driver.switchTo().window(parentWindow6); */
		

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
