package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyISBN_EditionsAndVolume extends PropertiesFile {

	HomePage hp;
	BookSearchResultsPage bsr;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify ISBN - Editions and Volume.")
	@Stories("AS-159 - Verify ISBN - Editions and Volume.")
	@Test
	public void VerifyISBN_EditionsVolume() throws Exception {

		driver = getDriver();
		
		hp = new HomePage();
		Assert.assertEquals(hp.checkShopTextBookButton(), true);
		Thread.sleep(2000);
		hp.clickOnShopTextBooksButton();
		bsr = new BookSearchResultsPage();
		Thread.sleep(2000);
		Assert.assertEquals(bsr.getBookListFilterOption("Editions"), "Editions");
		Thread.sleep(2000);
		Assert.assertEquals(bsr.getBookListFilterOption("Volumes"), "Volumes");
		Thread.sleep(2000);
		bsr.clickBookListFilterOption("Editions");
		String totalFilters_Editions = bsr.clickAllFiltersInFilterDropDown();
		Thread.sleep(2000);
		ReusableMethods.scrollToTop(driver);
		bsr.clickBookListFilterOption("Editions (" + totalFilters_Editions + ")");
		Thread.sleep(2000);
		bsr.clickResetFilterButton("Editions (" + totalFilters_Editions + ")");
		Thread.sleep(2000);
		bsr.clickBookListFilterOption("Volumes");
		String totalFilters_Volumes = bsr.clickAllFiltersInFilterDropDown();
		Thread.sleep(2000);
		ReusableMethods.scrollToTop(driver);
		bsr.clickBookListFilterOption("Volumes (" + totalFilters_Volumes + ")");
		Thread.sleep(2000);
		bsr.clickResetFilterButton("Volumes (" + totalFilters_Volumes + ")");
		Thread.sleep(2000);

	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
