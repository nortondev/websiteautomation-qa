package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.CartPayment;
import com.wwnorton.WebsiteAutomation.objectFactory.CartShipping;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.FindMyRepPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.InstructorResourcePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.objectFactory.OrderConfirmationPage;
import com.wwnorton.WebsiteAutomation.objectFactory.cartLayout;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class VerifyExamCopyRequest_Highschool extends PropertiesFile {

	public static String urlLink;
	CreateNewAccount CNA;
	LoginPage lp;
	HomePage hp;
	InstructorResourcePage irp;
	FindMyRepPage frp;
	cartLayout cLayOut;
	CartShipping cs;
	CartPayment cp;
	OrderConfirmationPage ocp;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Exam Copy request for high school")
	@Stories("AS-455 - Verify exam copy request for High School")
	@Test
	public void VerifyExamCopyRequestHighschool() throws Exception {

		driver = getDriver();
		urlLink = PropertiesFile.url;

		 SeleniumTestsContext gContext =
		 SeleniumTestsContextManager.getGlobalContext();
		 String existingEmailID = (String)
		 gContext.getAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1);

		 //String existingEmailID = "Test06-Dec@maildrop.cc";

		CNA = new CreateNewAccount();
		CNA.clickOnLogin();

		Thread.sleep(2000);
		lp = new LoginPage();
		String Password = "Password@123";

		lp.loginWithExistingAccount(existingEmailID, Password);
		lp.closeAccountLoginImg();
		Thread.sleep(2000);

		hp = new HomePage();
		hp.QAclickOnEducator();
		String educatorLinkURL = urlLink + "educator";
		String instrctorResourcesPageURL = urlLink + "catalog/instructor-resources";
		Assert.assertEquals(educatorLinkURL, driver.getCurrentUrl());
		String strEducator = hp.educatorText.getText();
		Assert.assertEquals(strEducator, "Educator");

		Thread.sleep(2000);
		ReusableMethods.scrollIntoView(driver, hp.instructorResourcesSection);
		irp = new InstructorResourcePage();
		irp.clickViewInstructorResourcesButton();
		Thread.sleep(2000);
		Assert.assertEquals(instrctorResourcesPageURL, driver.getCurrentUrl());
		Assert.assertEquals(irp.getInstrctorResourcePageHeader(), "Instructor Resources");
		Thread.sleep(2000);
		irp.clickInstructorResourceButton(1);
		Thread.sleep(2000);
		irp.verifyRequestExamCopyButton();
		irp.clickRequestExamCopyButton();
		irp.HighschoolSection.click();

		Thread.sleep(2000);

		irp.selectSchoolStateExamCopyHighSchool("New Jersey", "Academy High School North, Haskell");
		Thread.sleep(3000);
		irp.selectDepartmentNameExamCopy_Highschool("OTHER: OTHER");
		irp.continueButton.click();
		Thread.sleep(2000);
		if(irp.checkSelectEditionError() == true) {
			irp.selectEdition.click();
		}
		irp.continueButton.click();
		irp.courseName.sendKeys("Test Pls Disregard");
		irp.currentBookName.sendKeys("Test Pls Disregard");
		irp.selectTerm("Spring");
		Thread.sleep(1000);
		irp.selectYear("2025");
		irp.perTermEnrollment.sendKeys("3");
		//String currnetDate = ReusableMethods.getCurrentDay();
		irp.selectDecisionDate();
		irp.commentsTextBox.sendKeys("Please disregard, as this is Test comments");
		irp.addToCartButton_Examcopy.click();

		cLayOut = new cartLayout();
		Thread.sleep(2000);

		cLayOut.clickCheckout();

		cs = new CartShipping();
		cs.submitButton.click();
		Thread.sleep(2000);

		String fName = "Test";
		String lName = "Account";
		String prmAddress = "45 COUNTY ROAD 13";
		String city = "CLANTON";
		String state = "Alabama";
		String zip = "35045";
		cs.enterShippingInfo(fName, lName, prmAddress, city, state, zip);
		Thread.sleep(2000);

		cp = new CartPayment();
		cp.checkTnC();

		cs.submitButton.click();
		Thread.sleep(2000);
		if (cs.checkShippingAddressModalExist() == true) {
			cs.selectShippingAddress_Modal.click();
			cs.UPSShippingSaveAndContinue_Modal.click();
			Thread.sleep(3000);
			cs.submitButton.click();
		}

		Thread.sleep(2000);
		Assert.assertEquals("Your Exam Copy Request Has Been Submitted", cs.getSuccessfulExamCopySubmission());
		ocp = new OrderConfirmationPage();
		ocp.clickOnLogOut();

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
