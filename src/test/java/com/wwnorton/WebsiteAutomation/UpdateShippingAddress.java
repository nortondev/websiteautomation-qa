package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.utilities.GetRandomId;
import com.wwnorton.WebsiteAutomation.objectFactory.AccountDashBoardPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class UpdateShippingAddress extends PropertiesFile {
	CreateNewAccount CNA;
	LoginPage lp;
	AccountDashBoardPage adp;

	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Updated Shipping Addresses.")
	@Stories("AS-171 - Verify Updated Shipping Addresses.")
	@Test
	
	public void VerifyUpdatedShippingAddresses() throws Exception {

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String existingAccountEmailID = (String) gContext.getAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1);
		//String existingAccountEmailID = "Test06-Dec@maildrop.cc";
		driver = getDriver();

		CNA = new CreateNewAccount();
		CNA.clickOnLogin();
		
		Thread.sleep(2000);
		String pswd = "Password@123";
		
		lp = new LoginPage();
		lp.loginWithExistingAccount(existingAccountEmailID, pswd);
		Thread.sleep(2000);
		lp.clickProfileLink();
		adp = new AccountDashBoardPage();
		Thread.sleep(2000);
		int suffixCharCount = 2;
		while (adp.checkAddAddressLink() == true) {
			Thread.sleep(2000);
			String fName = "Test";
			String lName = "Account " + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();
			String prmAddress = "COUNTY ROAD " + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();
			String cityName = "CLANTON";
			String state = "Alabama";
			String  zip = "35045";
			int[] phoneIntArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
			
			String accountName = fName +" " + lName;   
			String updatedLName = "Account " + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase(); 
			String updatedAddress = "COUNTY ROAD " + GetRandomId.randomAlphaNumeric(suffixCharCount).toLowerCase();
			adp.addShiipingAddress(fName, lName, prmAddress, cityName, state, zip, phoneIntArray);
			adp.editShiipingAddress(accountName, updatedLName, updatedAddress);
		}
		Thread.sleep(2000);
		Assert.assertEquals(adp.checkAddAddressLink(), false);
		Thread.sleep(2000);
		adp.removeShiipingAddress();
		Thread.sleep(2000);
		lp.clickAccountLink();
		Thread.sleep(2000);
		lp.checkLogOutExist();
	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
