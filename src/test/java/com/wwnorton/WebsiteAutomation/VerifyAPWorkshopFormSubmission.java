package com.wwnorton.WebsiteAutomation;


import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.APWorkshopPage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyAPWorkshopFormSubmission extends PropertiesFile {

	public static String urlLink;
	LoginPage lp;
	APWorkshopPage apworkshop;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify AP Workshop Form Submission.")
	@Stories("AS-144 - Verify AP Workshop Form Submission.")
	@Test
	
	public void VerifyAPWorkshopForm() throws Exception {

		driver = getDriver();
		urlLink = PropertiesFile.url;
		
		try {
				if(urlLink.equalsIgnoreCase("https://wwnorton.com/")){
					driver.get(urlLink+"ap-workshop-leader-request?preview");
					
			}else if(urlLink.equalsIgnoreCase("https://qa.wwnorton.net/")) {
					driver.get(urlLink+"ap-workshop");
				
			}else if (urlLink.equalsIgnoreCase("https://ecp-qa.wwnorton.net/")) {
					driver.get(urlLink+"ap-workshop");
				
			}else if (urlLink.equalsIgnoreCase("https://ecp-stg.wwnorton.net/")) {
				driver.get(urlLink+"ap-workshop");
			
			}else if (urlLink.equalsIgnoreCase("https://auth-qa.wwnorton.net/")) {
					driver.get(urlLink+"ap-workshop");
			}
		}

		catch (Exception e)

		{
			e.printStackTrace();
		}
		
		ReusableMethods.scrollToBottom(driver);
		apworkshop = new APWorkshopPage();
		apworkshop.clickAPWorkShopButton();
		
		Thread.sleep(2000);
		String fName = "Test Account";
		String sName = "Test School";
		String sCityName = "Montgomery";
		String stateName = "Alabama";
		String titlleName = "Instructor";
		String emailID = "test@test.com";
		apworkshop.submitAPContactInformationForm(fName, sName, sCityName, stateName, titlleName, emailID);
		
		Thread.sleep(2000);
		String subjectName = "AP Chemistry";
		String instituteName = "Brighamton";
		String instituteCityName = "Montgomery";
		String instStateName = "Alabama";
		String currnetDate = ReusableMethods.getCurrentDay();
		String enrollmentValue = "1";
		String coordinatorName = "Test Coordinator";
		apworkshop.submitWorkshopInformationForm(subjectName, instituteName, instituteCityName, instStateName, 
				currnetDate, enrollmentValue, coordinatorName, emailID);
		
		Thread.sleep(3000);
		Assert.assertEquals(apworkshop.checkRequestConformationPopUp(), true);
		
	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
