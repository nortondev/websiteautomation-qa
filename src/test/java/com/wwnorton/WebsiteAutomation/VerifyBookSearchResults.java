package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyBookSearchResults extends PropertiesFile {
	
	public static String urlLink;
	HomePage hp;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Header and Footer links in Website Home Page.")
	@Stories("AS-187 - Verify Book Search Results on Home Page.")
	@Test
	public void VerifyBookSearchResults_HomePage() throws Exception {

		driver = getDriver();
		urlLink = PropertiesFile.url;
		
		String book1Str = "Earth Science";
		String book2Str = "Psychological Science";
		String book3Str = "Essentials of Geology";
		
		hp = new HomePage();
		hp.searchBook(book1Str);
		Thread.sleep(2000);
		Assert.assertTrue(hp.getBookHeaderText(book1Str).contains(book1Str));
		Thread.sleep(2000);
		Assert.assertEquals(hp.getBookName(book1Str), book1Str);
		Thread.sleep(2000);
		hp.clearResultsButton.click();
		Thread.sleep(2000);
		hp.searchBook(book2Str);
		Thread.sleep(2000);
		Assert.assertTrue(hp.getBookHeaderText(book2Str).contains(book2Str));
		Thread.sleep(2000);
		Assert.assertEquals(hp.getBookName(book2Str), book2Str);
		Thread.sleep(2000);
		hp.clearResultsButton.click();
		Thread.sleep(2000);
		hp.searchBook(book3Str);
		Thread.sleep(2000);
		Assert.assertTrue(hp.getBookHeaderText(book3Str).contains(book3Str));
		Thread.sleep(2000);
		Assert.assertEquals(hp.getBookName(book3Str), book3Str);
		Thread.sleep(2000);


	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}