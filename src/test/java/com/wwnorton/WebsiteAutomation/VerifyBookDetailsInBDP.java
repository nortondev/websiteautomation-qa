package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.BookDetailsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;


import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyBookDetailsInBDP extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	CreateNewAccount CNA;
	LoginPage lp;
	BookSearchResultsPage bookResultsPage;
	BookDetailsPage bdp;

	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Book Details in Book Details page.")
	@Stories("AS-147 - Verify Book Details in Book Details page.")
	@Test
	
	public void VerifyBookDetails() throws Exception {

		driver = getDriver();
		String bookName = "978-0-393-33048-9";
		
		Thread.sleep(2000);
		hp = new HomePage();
		hp.searchBook(bookName);
		
		Thread.sleep(2000);
		bdp = new BookDetailsPage();
		String bookAuthorName = bdp.getAuthorName_BookDetaillsOverview();
		
		Thread.sleep(2000);
		Assert.assertTrue(bdp.checkBookDetails());
		Thread.sleep(2000);
		ReusableMethods.scrollIntoView(driver, bdp.bookBinderTitle);
		Assert.assertTrue(bdp.verifyBookInformation("Paperback"));

		Thread.sleep(2000);
		bdp.clickAboutTheBook();
		Thread.sleep(2000);
		Assert.assertTrue(bdp.checkAboutTheBook_Tabs());
		
		
		Thread.sleep(2000);
		bdp.tabReview.click();
		if(bdp.reviewQuoteList.size() != 0 ) {
			bdp.reviewQuoteList.get(0).isDisplayed();
		}
		
		Thread.sleep(2000);
		bdp.tabProductDetails.click();
		String isbn = bdp.getISBN();
		Assert.assertTrue(bookName.equalsIgnoreCase(isbn));
		
		Thread.sleep(2000);
		bdp.clickConnectLink();
		Assert.assertTrue(bdp.checkSocialMediaLinks());
		
		Thread.sleep(2000);
		bdp.clickAboutTheAuthor();
		Assert.assertTrue(bookAuthorName.contains("Edward"));
		Thread.sleep(2000);
		bdp.button_AboutTheAuthor.click();
		Thread.sleep(2000);
		String authorTitle = bdp.getAuthorTitle();
		Assert.assertTrue(authorTitle.contains("Edward"));
		
	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
