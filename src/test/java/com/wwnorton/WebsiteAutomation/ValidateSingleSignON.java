package com.wwnorton.WebsiteAutomation;

import static org.junit.Assert.assertArrayEquals;

import java.util.ArrayList;
import java.util.Iterator;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.InstructorResourcePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class ValidateSingleSignON extends PropertiesFile {
	
	CreateNewAccount CNA;
	LoginPage lp;
	InstructorResourcePage irp;
	String urlLink;
	String NRLAccessEmailID;
	String Password;
	
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
		@Severity(SeverityLevel.NORMAL)
		@Description("Validate Single Sign ON.")
		@Stories("AS-142 - Validate Single Sign ON.")
		@Test 
		public void SingleSignON() throws Exception {

			driver = getDriver();
			urlLink = PropertiesFile.url;
			driver.get(urlLink + "instructor-resources/9780393428315");
			if(urlLink.equalsIgnoreCase("https://wwnorton.com/")) {
				NRLAccessEmailID = "vmane@wwnorton.com";
				Password = "Tester7521";
			} else if (urlLink.equalsIgnoreCase("https://auth-qa.wwnorton.net/")) {
				NRLAccessEmailID = "vmane@wwnorton.com";
				Password = "Tester@7521";
			} else if(urlLink.equalsIgnoreCase("https://auth-stg.wwnorton.net/")) {
				NRLAccessEmailID = "vinod.mane@thedigitalgroup.com";
				Password = "Tester@4528";
			} else if(urlLink.equalsIgnoreCase("https://ecp-stg.wwnorton.net/")) {
				NRLAccessEmailID = "vinod.mane@thedigitalgroup.com";
				Password = "Tester@4528";
			} else if(urlLink.equalsIgnoreCase("https://ecp-qa.wwnorton.net/")) {
				NRLAccessEmailID = "vinod.mane@thedigitalgroup.com";
				Password = "Tester123";
			}
			
			CNA = new CreateNewAccount();
			CNA.clickOnLogin();
			Thread.sleep(2000);
			lp = new LoginPage();
			
			
			lp.loginWithExistingAccount(NRLAccessEmailID, Password);
			lp.closeAccountLoginImg();
			irp = new InstructorResourcePage();
			irp.clickInteractiveGuide() ;
			Assert.assertEquals(irp.iigs_title.getAttribute("innerText"), "Interactive Instructor's Guide");
			irp.clickTestBank1();
			Assert.assertEquals(irp.testbank1_title.getAttribute("innerText"), "Test Bank");
			irp.clickhighImpactPractices();
			Assert.assertEquals(irp.HIP_title.getAttribute("innerText"), "High-Impact Practices: A Teaching Guide for Psychology (HIP Guide)");
			irp.clickimageFiles();
			Assert.assertEquals(irp.imageFiles_title.getAttribute("innerText"), "Image Files");
			irp.clickinquisitives();
			Assert.assertEquals(irp.inquisitive_title.getAttribute("innerText"), "InQuizitive");
			irp.clickpptActiveLearning();
			Assert.assertEquals(irp.pptAL_title.getAttribute("innerText"), "PowerPoints: Active Learning");
			irp.clickpptInteractiveLecture();
			Assert.assertEquals(irp.pptIL_title.getAttribute("innerText"), "PowerPoints: Interactive Lecture");
			irp.clickpptLectureOutlines();
			Assert.assertEquals(irp.pptLO_title.getAttribute("innerText"), "PowerPoints: Lecture Outlines");
			irp.clicktestBank2();
			Assert.assertEquals(irp.testbank2_title.getAttribute("innerText"), "Test Bank");
			irp.clickthreeDbrain();
			Assert.assertEquals(irp.threeDbrain_title.getAttribute("innerText"), "3D Brain");
			JavascriptExecutor js = ((JavascriptExecutor) driver);
			js.executeScript("window.scrollTo(0, -document.body.scrollHeight);");
			irp.clickAllAssets();
			Thread.sleep(2000);
			Assert.assertTrue(irp.checkIIGButtons());
			Thread.sleep(2000);
			String parentWindow = driver.getWindowHandle();
			Assert.assertEquals(irp.checkIIGDemoSite_NoSignIn(parentWindow).equalsIgnoreCase("Sign in or Register"), true);
			Thread.sleep(2000);
			Assert.assertEquals(irp.checkIIGFullSite_SignedIn(parentWindow).trim()
					.equalsIgnoreCase("vmane@wwnorton.com"), true);
			Thread.sleep(2000);
			Assert.assertEquals(irp.signOut_IIGSite(parentWindow).equalsIgnoreCase("LOG IN"), true);
			Thread.sleep(2000);
		}
		
	@AfterTest
		public void closeTest() throws Exception {
			PropertiesFile.tearDownTest();
	}

}
