package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.ContactUsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.NRLToolPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyNRLRequestSubmission_Admin extends PropertiesFile {

	public static String urlLink;

	NRLToolPage NLT;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify submission of NRL request as an Admin")
	@Stories("AS-189 - Verify submission of NRL request as an Admin")
	@Test

	public void NRLRequestSubmission_Admin() throws Exception {

		driver = getDriver();
		urlLink = PropertiesFile.url;
		if(urlLink.equalsIgnoreCase("https://auth-qa.wwnorton.net/")) {
			driver.get("https://nrltool-auth-qa.wwnorton.net/");
		} else if (urlLink.equalsIgnoreCase("https://wwnorton.com/")) {
			driver.get("https://nrltool.wwnorton.com/");
		} else if(urlLink.equalsIgnoreCase("https://auth-stg.wwnorton.net/")) {
			driver.get("https://nrltool-auth-stg.wwnorton.net/");
		} else if(urlLink.equalsIgnoreCase("https://ecp-stg.wwnorton.net/")) {
			driver.get("https://nrltool-ecp-stg.wwnorton.net/");
		}
		
		NLT = new NRLToolPage();
		//String Email = "aulhe@wwnorton.com";
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String AccountEmailID = (String) gContext.getAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1);

		String isbnumber = "9780393533019";
		String StateName = "Alabama";
		String SchoolName = "Air University, Maxwell AFB";
		String Dept = "History";
		
		NLT.SubmitNRLRequestForm(AccountEmailID, isbnumber, StateName, SchoolName, Dept);
		ReusableMethods.scrollToTop(driver);
		Thread.sleep(2000);
		Assert.assertTrue(NLT.checkSuccessMsg());
		Thread.sleep(2000);
	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}