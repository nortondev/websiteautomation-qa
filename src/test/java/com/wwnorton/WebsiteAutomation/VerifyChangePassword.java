package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.AccountDashBoardPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyChangePassword extends PropertiesFile {

	CreateNewAccount CNA;
	LoginPage lp;
	AccountDashBoardPage adp;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Change Password functionality.")
	@Stories("AS-444 - Verify Change Password functionality.")
	@Test
	
	public void VerifyChangePasswordWithExistingEmailID() throws Exception {

		driver = getDriver();
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String existingEmailID = (String) gContext.getAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1);
		//String existingEmailID = "qa07feb2021@mailinator.com";
		
		CNA = new CreateNewAccount();
		CNA.clickOnLogin();
		Thread.sleep(2000);
		lp = new LoginPage();
		Thread.sleep(2000);
		String pswd = "Password@123";
		String newpswd = "Digital@123";
		lp.loginWithExistingAccount(existingEmailID, pswd);
		Thread.sleep(2000);
		Assert.assertEquals(true, lp.logoutElement.isDisplayed());
		Thread.sleep(2000);
		lp.clickProfileLink();
		adp = new AccountDashBoardPage();
		adp.updateAccountPassword(pswd, newpswd);
		Thread.sleep(2000);
		CNA.clickOnLogin();
		Thread.sleep(2000);
		lp.checkLogOutExist();
		Thread.sleep(2000);
		CNA.clickOnLogin();
		Thread.sleep(2000);
		lp.loginWithExistingAccount(existingEmailID, newpswd);
		Thread.sleep(2000);
		Assert.assertEquals(true, lp.checkLogOutExist());

	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
