package com.wwnorton.WebsiteAutomation;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.NortonHelpPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class ValidateRequestBookSellersFormSubmission extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	NortonHelpPage nhp;

	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Validate Request Book Sellers Form Submission.")
	@Stories("AS-166 Validate Request Book Sellers Form Submission.")
	@Test
	
	public void validateRequestBookSellersFormSubmission() throws Exception {
		
		driver = getDriver();
		urlLink = PropertiesFile.url;
		String helpURL = urlLink + "help";
		driver.get(helpURL);
		
		nhp = new NortonHelpPage();
		Thread.sleep(2000);
		nhp.learnMoreButton_BookSellers.click();
		Thread.sleep(2000);
		ReusableMethods.scrollToBottom(driver);
		nhp.retailerReqButton_BookSellers.click();
		Thread.sleep(2000);
		String firstName = "Test"; 
		String lastName = "Account";
		String addressLine = "100W Newline Street";
		String cityName = "New York City";
		String stateName = "New York";
		String zip = "11010";
		int[] phoneIntArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
		String email = "abc@gmail.com"; 
		String typeOfStore = "Retail Shop";
		nhp.submitRequestRetailForm(firstName, lastName, addressLine, cityName, stateName, phoneIntArray, zip, email, typeOfStore);
		String pNotice = "We will send you an email confirming your request from " +
		 "seagull@wwnorton.com." + " Please make sure your email client can accept messages from this address; " + 
				"you may need to check your spam folder.";
		Thread.sleep(2000);
		Assert.assertTrue(nhp.verifyConnfirmationOverlay());
		Thread.sleep(2000);
		Assert.assertTrue(nhp.confirmationNotice().contains(pNotice));
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", nhp.closeButton);
		
	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}