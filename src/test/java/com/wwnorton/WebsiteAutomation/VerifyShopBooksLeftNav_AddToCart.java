package com.wwnorton.WebsiteAutomation;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.AccountDashBoardPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookDetailsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CartItemDetails;
import com.wwnorton.WebsiteAutomation.objectFactory.CartPayment;
import com.wwnorton.WebsiteAutomation.objectFactory.CartShipping;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.objectFactory.OrderConfirmationPage;
import com.wwnorton.WebsiteAutomation.objectFactory.cartLayout;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyShopBooksLeftNav_AddToCart extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	CreateNewAccount CNA;
	LoginPage lp;
	BookSearchResultsPage bookResultsPage;
	BookDetailsPage bdp;
	cartLayout cLayOut;
	CartItemDetails cid;
	CartShipping cs;
	CartPayment cp;
	OrderConfirmationPage ocp;
	AccountDashBoardPage adp;

	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify left navigation, browse the books and add into the cart till check-out page.")
	@Stories("AS-184 - Verify left navigation, browse the books and add into the cart till check-out page.")
	@Test
	
	public void validateShopTextBooksLeftNav_AddToCart() throws Exception {

		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String existingAccountEmailID = (String) gContext.getAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1);

		//String existingAccountEmailID = "smaheshwari@wwnorton.com";
		driver = getDriver();
		urlLink = PropertiesFile.url;

		CNA = new CreateNewAccount();
		CNA.clickOnLogin();
		
		Thread.sleep(2000);
		String pswd = "Password@123";
		
		lp = new LoginPage();
		lp.loginWithExistingAccount(existingAccountEmailID, pswd);
		lp.closeAccountLoginImg();
		
		Thread.sleep(2000);
		hp = new HomePage();
		hp.clickOnShopBooksButton();
		Thread.sleep(2000);
		bookResultsPage = new BookSearchResultsPage();
		bookResultsPage.navigateToYoungReaderCategory();
		Thread.sleep(2000);
		bookResultsPage.navigateToAgeCategory();
		Thread.sleep(2000);
		bookResultsPage.navigateToFictionCategory();
		Thread.sleep(2000);
		bookResultsPage.navigateToGeneralCategory();
		Thread.sleep(2000);
		bookResultsPage.selectBookFromList(0);
		Thread.sleep(2000);
		bdp = new BookDetailsPage();
		ReusableMethods.scrollIntoView(driver, bdp.bookFormatLayout); 
		String headerOption_Book1 = "Paperback";
		Thread.sleep(2000);
		bdp.clickBookBinding(headerOption_Book1);
		Thread.sleep(3000);
		bdp.addBookToCart();
		Thread.sleep(2000);
		bookResultsPage.buttonClose.click();
		Thread.sleep(3000);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", hp.seagullicon);
		Thread.sleep(2000);
		hp.clickOnShopBooksButton();
		Thread.sleep(2000);
		bookResultsPage = new BookSearchResultsPage();
		bookResultsPage.navigateToYoungReaderCategory();
		Thread.sleep(2000);
		bookResultsPage.navigateToAgeCategory();
		Thread.sleep(2000);
		bookResultsPage.navigateToFictionCategory();
		Thread.sleep(2000);
		bookResultsPage.navigateToGeneralCategory();
		Thread.sleep(2000);
		bookResultsPage.selectBookFromList(0);
		Thread.sleep(2000);
		bdp = new BookDetailsPage();
		ReusableMethods.scrollIntoView(driver, bdp.bookFormatLayout); 
		String headerOption_Book2 = "Paperback";
		Thread.sleep(2000);
		bdp.clickBookBinding(headerOption_Book2);
		Thread.sleep(3000);
		bdp.addBookToCart();
		Thread.sleep(2000);
		cLayOut = new cartLayout();
		Thread.sleep(2000);
		cLayOut.clickCheckout();
		Thread.sleep(2000);
		cs = new CartShipping();
		Thread.sleep(2000);
		cs.selectNotStudentOption();
		String  fName = "Test";
		String  lName = "Account";
		String  prmAddress = "45 COUNTY ROAD 13";
		String  city = "CLANTON";
		String state = "Alabama";
		String  zip = "35045";
		cs.enterShippingInfo(fName, lName, prmAddress, city, state, zip);
		Thread.sleep(2000);
		cs.submitButton.click();
		Thread.sleep(2000);
		if(cs.checkShippingAddressModalExist() == true) {
			cs.selectShippingAddress_Modal.click();
			cs.UPSShippingSaveAndContinue_Modal.click();
			Thread.sleep(3000);
			cs.submitButton.click();
		}
		
		Thread.sleep(2000);
		String cHName = "Test Account"; 
		String expMonth = "December";
		String expYear = "2023"; 
		String secCode = "123";
		
		cp = new CartPayment();
		cp.enterPaymentInfo(cHName, expMonth, expYear, secCode);
		Thread.sleep(2000);
		cp.checkBillingAddress();
		Thread.sleep(2000);
		cp.checkTnC();
		Thread.sleep(2000);
		cp.placeOrder();
		Thread.sleep(2000);
		ocp = new OrderConfirmationPage();
		ocp.orderDetails();
		//Thread.sleep(2000);
		//lp.clickAccountLink();
		Thread.sleep(2000);
		ocp.logOutLink.click();
		Thread.sleep(2000);
	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}