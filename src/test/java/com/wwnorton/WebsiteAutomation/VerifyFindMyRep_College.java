package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.AccountDashBoardPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookDetailsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CartPayment;
import com.wwnorton.WebsiteAutomation.objectFactory.CartShipping;
import com.wwnorton.WebsiteAutomation.objectFactory.CatalogPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.FindMyRepPage;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.InstructorResourcePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.objectFactory.OrderConfirmationPage;
import com.wwnorton.WebsiteAutomation.objectFactory.SocialMediaSites;
import com.wwnorton.WebsiteAutomation.objectFactory.cartLayout;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;

//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })

public class VerifyFindMyRep_College extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	BookSearchResultsPage bookResultsPage;
	BookDetailsPage bdp;
	CatalogPage ca;
	cartLayout cLayOut;
	LoginPage lp;
	CartShipping cs;
	CartPayment cp;
	OrderConfirmationPage ocp;
	AccountDashBoardPage adp;
	InstructorResourcePage ins;
	FindMyRepPage fyr;

	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}

	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Find My Rep for College")
	@Stories("AS-154 - Find My Rep for College")
	@Test

	public void VerifyFindMyRepCollege() throws Exception {

		driver = getDriver();
		String stateNameUSA = "Iowa";
		String schNameUSA = "Briar Cliff University, Sioux City";
		String subNameUSA = "General Inquiry";
		String stateNameCA = "Ontario";
		String schNameCA = "CTS Canadian Career College, North Bay";
		String subNameCA = "Exam Copy Request";
		String yFName = "Test Pls Disregard";
		String yLName = "Test Pls Disreagrd";
		String yEmailAdd = "test@test.com";
		String yMessage = "This is the test by WWN QA Team Please Disregard";

		hp = new HomePage();
		hp.QAclickOnEducator();

		// Send Find My Rep requests for USA

		fyr = new FindMyRepPage();
		fyr.clickOnFindMyRepButton();
		fyr.selectSchoolStateUSACollege(stateNameUSA, schNameUSA);
		fyr.clickOnSearchButton();
		Thread.sleep(3000);
		Assert.assertEquals("David Robson", fyr.getRepNameAtHeader());
		Thread.sleep(3000);
		Assert.assertEquals("David Robson", fyr.getRepName());
		Thread.sleep(3000);
		Assert.assertEquals("917 699-5182", fyr.getRepPhoneNum());
		Thread.sleep(3000);
		Assert.assertEquals("drobson@wwnorton.com", fyr.getRepEmailId());
		fyr.submitContactDetails(yFName, yLName, yEmailAdd, yMessage);
		Thread.sleep(3000);
		fyr.selectSubject(subNameUSA);
		fyr.clickOnSendButton();
		Thread.sleep(3000);
		Assert.assertEquals(fyr.checkRequestConformationPopUpForFindMyRep(), true);

	}

	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
