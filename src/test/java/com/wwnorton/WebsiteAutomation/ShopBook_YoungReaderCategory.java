package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.AccountDashBoardPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookDetailsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.BookSearchResultsPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CartItemDetails;
import com.wwnorton.WebsiteAutomation.objectFactory.CartPayment;
import com.wwnorton.WebsiteAutomation.objectFactory.CartShipping;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.HomePage;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.objectFactory.OrderConfirmationPage;
import com.wwnorton.WebsiteAutomation.objectFactory.cartLayout;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.ReadJsonFile;
import com.wwnorton.WebsiteAutomation.utilities.ReusableMethods;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class ShopBook_YoungReaderCategory extends PropertiesFile {

	public static String urlLink;
	HomePage hp;
	CreateNewAccount CNA;
	LoginPage lp;
	BookSearchResultsPage bookResultsPage;
	BookDetailsPage bdp;
	cartLayout cLayOut;
	CartItemDetails cid;
	CartShipping cs;
	CartPayment cp;
	OrderConfirmationPage ocp;
	AccountDashBoardPage adp;

	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify ShopBook - Young Reader Category")
	@Stories("AS-176 - Verify ShopBook - Young Reader Category")
	@Test
	
	public void validateShopBook_YoungReaderCategory() throws Exception {

		driver = getDriver();
		urlLink = PropertiesFile.url;
		//String bookName = "Bearmouth";
		//SS
		ReadJsonFile readJsonObject = new ReadJsonFile();
		JsonObject jsonobject = readJsonObject.readJason();
		String existingAccountEmailID = jsonobject.getAsJsonObject("ExistingAccountCredentials").get("accountEmail").getAsString();
		String pswd = jsonobject.getAsJsonObject("ExistingAccountCredentials").get("accountpw").getAsString();

		CNA = new CreateNewAccount();
		CNA.clickOnLogin();
		Thread.sleep(2000);
		lp = new LoginPage();
		lp.loginWithExistingAccount(existingAccountEmailID, pswd);
		lp.closeAccountLoginImg();
		
		Thread.sleep(2000);
		hp = new HomePage();
		hp.QAclickOnReader();
		Thread.sleep(2000);
		hp.clickOnYoungReader_BrowseButton();
		//String collegeBookLinkURL = urlLink + "catalog/young-readers";
		//Assert.assertEquals(collegeBookLinkURL, driver.getCurrentUrl());
		//String strEducator = hp.shopBooks_YoungReaderHeaderText.getText();
		//Assert.assertTrue(strEducator.contains("Young Readers"));
		Thread.sleep(2000);
		bookResultsPage = new BookSearchResultsPage();
		ReusableMethods.scrollIntoView(driver, bookResultsPage.firstBook.get(1));
		bookResultsPage.selectBookFromList(1);
		Thread.sleep(2000);
		bdp = new BookDetailsPage();
		/*ReusableMethods.scrollIntoView(driver, bdp.bookFormatLayout);
		String headerOption_Book = "Hardcover";
		Thread.sleep(2000);
		bdp.clickBookBinding(headerOption_Book);
		Thread.sleep(2000);*/
		bdp.addToCart();
		Thread.sleep(2000);
		cLayOut = new cartLayout();
		Thread.sleep(2000);
		cLayOut.clickCheckout();
		
		Thread.sleep(2000);
		cs = new CartShipping();
		Thread.sleep(2000);
	//	lp.loginWithExistingAccount(existingAccountEmailID, pswd);
		
		cs.selectNotStudentOption();

		//Add login
		String  fName = "Test";
		String  lName = "Account";
		String  prmAddress = "45 COUNTY ROAD 13";
		String  city = "CLANTON";
		String state = "Alabama";
		String  zip = "35045";
		cs.enterShippingInfo(fName, lName, prmAddress, city, state, zip);
		Thread.sleep(2000);
		cs.submitButton.click();
		Thread.sleep(2000);
		if(cs.checkShippingAddressModalExist() == true) {
			cs.selectShippingAddress_Modal.click();
			cs.UPSShippingSaveAndContinue_Modal.click();
			Thread.sleep(3000);
			cs.submitButton.click();
		}
		
		Thread.sleep(2000);
		String cHName = "Test Account"; 
		String expMonth = "December";
		String expYear = "2023"; 
		String secCode = "123";
		
		cp = new CartPayment();
		cp.enterPaymentInfo(cHName, expMonth, expYear, secCode);
		
		Thread.sleep(2000);
		cp.checkBillingAddress();
		Thread.sleep(2000);
		cp.checkTnC();
		Thread.sleep(2000);
		cp.placeOrder();
		
		Thread.sleep(2000);
		ocp = new OrderConfirmationPage();
		String OrderNumber_OrderConfirmation = ocp.orderDetails();
		Thread.sleep(2000);
		hp = new HomePage();
		hp.clickOnSeagull_CheckOut();
		Thread.sleep(2000);
		lp = new LoginPage();
		lp.clickAccountLink();
		Thread.sleep(2000);
		lp.clickOrdersLink();
		Thread.sleep(2000);
/*		adp = new AccountDashBoardPage();
		adp.clickAccountDetailLink();
		Thread.sleep(2000);
		String OrderNumber_OrderDetails = adp.getOrderNumber();
		String[] arrSplit = OrderNumber_OrderDetails.split("#");
		Assert.assertTrue(OrderNumber_OrderConfirmation.equals(arrSplit[1]));
*/
		Thread.sleep(2000);
		lp.clickAccountLink();
		Thread.sleep(2000);
		lp.checkLogOutExist();
	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
