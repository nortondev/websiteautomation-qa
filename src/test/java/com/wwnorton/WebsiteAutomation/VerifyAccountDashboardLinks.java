package com.wwnorton.WebsiteAutomation;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.seleniumtests.core.SeleniumTestsContext;
import com.seleniumtests.core.SeleniumTestsContextManager;
import com.wwnorton.WebsiteAutomation.objectFactory.AccountDashBoardPage;
import com.wwnorton.WebsiteAutomation.objectFactory.CreateNewAccount;
import com.wwnorton.WebsiteAutomation.objectFactory.LoginPage;
import com.wwnorton.WebsiteAutomation.utilities.PropertiesFile;
import com.wwnorton.WebsiteAutomation.utilities.TestListener;
import com.wwnorton.WebsiteAutomation.utilities.WebsiteConstants;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots.
@Listeners({ TestListener.class })


public class VerifyAccountDashboardLinks extends PropertiesFile {

	public static String urlLink;
	CreateNewAccount CNA;
	LoginPage lp;
	AccountDashBoardPage adPage;
	
	@Parameters({ "browser" })
	@BeforeTest
	public void setUp(@Optional("Chrome") String browser) throws Exception {
		PropertiesFile.readPropertiesFile();
		PropertiesFile.setBrowserConfig(browser);
		PropertiesFile.setURL();
	}
	
	@Severity(SeverityLevel.NORMAL)
	@Description("Verify Links in Account Dashboard page.")
	@Stories("AS-143 - Verify Links in Account Dashboard page.")
	@Test
	
	public void VerifyLinksInAccountDashboard() throws Exception {

		driver = getDriver();
		urlLink = PropertiesFile.url;
		
		SeleniumTestsContext gContext = SeleniumTestsContextManager.getGlobalContext();
		String existingEmailID = (String) gContext.getAttribute(WebsiteConstants.WEBSITE_NEW_USER_NAME_1);
		
		CNA = new CreateNewAccount();
		CNA.clickOnLogin();
		
		Thread.sleep(2000);
		lp = new LoginPage();
		String Password = "Password@123";
		
		lp.loginWithExistingAccount(existingEmailID, Password);
		
		Thread.sleep(2000);
		Assert.assertEquals(true, lp.checkProfileLinkExist());
		Assert.assertEquals(true, lp.checkDigitalProductsLinkExist());
		Assert.assertEquals(true, lp.checkOrdersLinkExist());
		
		Thread.sleep(2000);
		lp.clickProfileLink();
		
		Thread.sleep(2000);
		adPage = new AccountDashBoardPage();
		String profileLinkURL = urlLink + "dashboard/profile";
		Assert.assertEquals(profileLinkURL, driver.getCurrentUrl());
		Assert.assertEquals("Test Account", adPage.getProfileName());
		Assert.assertTrue(adPage.getProfileEmailID().equalsIgnoreCase(existingEmailID));
		Assert.assertEquals(true, adPage.checkChangePasswordLink());
		
		Thread.sleep(2000);
		lp.clickAccountLink();
		
		Thread.sleep(2000);
		lp.clickDigitalProductsLink();
		String productsLinkURL = urlLink + "dashboard/digital-products";
		Thread.sleep(2000);
		Assert.assertEquals(productsLinkURL, driver.getCurrentUrl());
		
		Thread.sleep(2000);
		lp.clickAccountLink();
		
		Thread.sleep(2000);
		lp.clickOrdersLink();
		String ordersLinkURL = urlLink + "dashboard/orders";
		Thread.sleep(2000);
		Assert.assertEquals(ordersLinkURL, driver.getCurrentUrl());
		
		Thread.sleep(2000);
		lp.clickAccountLink();
		Assert.assertEquals(true, lp.checkLogOutExist());
		
	}

	
	@AfterTest
	public void closeTest() throws Exception {
		PropertiesFile.tearDownTest();
	}
}
